import 'dart:convert';
import 'package:http/http.dart' as http;


Future<Map<String,dynamic>> getAddress({double? lat, double? long}) async {
  var response;
  String api =
      'api';
  try {
    response = await http.get(
      Uri.parse("https://map.ir/fast-reverse?lat=$lat&lon=$long"),
      headers: {
        "x-api-Key": "$api",
      },
    ).timeout(Duration(seconds: 5));
  } catch (e) {
    print(e);
    return {};
  }
  var x = json.decode(response.body);
  return x;
}
