import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';

class PlusAndMinusButton extends StatelessWidget {
  PlusAndMinusButton(
      {required this.name, required this.index, required this.changeIndex});

  final int index;
  final String name;
  final Function changeIndex;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          IconButton(
            icon: Icon(
              EvaIcons.minusCircleOutline,
              color: Colors.grey,
              size: 32,
            ),
            onPressed: () {
              if (index > 0) return changeIndex.call(context, name, index - 1);
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(index.toString()),
          ),
          IconButton(
            onPressed: () => changeIndex.call(context, name, index + 1),
            icon: Icon(EvaIcons.plusCircle, color: Colors.deepPurple, size: 32),
          )
        ],
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(50),
          ),
          border: Border.all(color: Colors.grey.shade600)),
    );
  }
}
