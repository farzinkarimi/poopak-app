//import 'dart:ui';
//
//import 'package:eva_icons_flutter/eva_icons_flutter.dart';
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter_typeahead/flutter_typeahead.dart';
//import 'package:provider/provider.dart';
//
//Widget searchBarTextField(BuildContext context) {
//  return TypeAheadField(
//    loadingBuilder: (context) {
//      return Center(child: CircularProgressIndicator());
//    },
//    hideSuggestionsOnKeyboardHide: false,
//    transitionBuilder: (context, suggestionsBox, animationController) =>
//        FadeTransition(
//      child: suggestionsBox,
//      opacity: CurvedAnimation(
//          parent: animationController!.view, curve: Curves.fastOutSlowIn),
//    ),
//    suggestionsBoxDecoration: SuggestionsBoxDecoration(
//      borderRadius: BorderRadius.circular(15),
//      color: Colors.white,
//    ),
//    textFieldConfiguration: TextFieldConfiguration(
//        style: DefaultTextStyle.of(context).style.copyWith(
//              color: Colors.black,
//              fontSize: 15,
//            ),
//        decoration: InputDecoration(
//          contentPadding: EdgeInsets.symmetric(vertical: 20),
//          hintText: 'جستجو خدمات',
//          hintStyle: TextStyle(color: Colors.grey),
//          prefixIcon: Icon(
//            EvaIcons.search,
//            color: Colors.grey,
//            size: 30,
//          ),
//          fillColor: Colors.blueGrey[100],
//          filled: true,
//          border: OutlineInputBorder(
//              borderSide: BorderSide(width: 0, style: BorderStyle.none),
//              borderRadius: BorderRadius.all(Radius.circular(10))),
//        )),
//    suggestionsCallback: (pattern) {},
//    itemBuilder: (context, suggestion) {
//      return ListTile(
//        title: Text(suggestion['name']),
//        subtitle: Text('\$${suggestion['price']}'),
//      );
//    },
//    onSuggestionSelected: (suggestion) {
//      Navigator.of(context).push(MaterialPageRoute(
//        builder: (context) => PayScreen(),
//      ));
//    },
//  );
//}
