import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:poopak_app/view/auth/AuthPopUp.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import 'package:provider/provider.dart';

Future<void> customAlertDialog(
    BuildContext context) async {
  await showGeneralDialog(
    context: context,
    barrierDismissible: false,
    barrierLabel: '',
    barrierColor: Colors.black38,
    transitionDuration: Duration(milliseconds: 100),
    transitionBuilder: (ctx, anim1, anim2, child) => BackdropFilter(
      filter:
          ImageFilter.blur(sigmaX: 6 * anim1.value, sigmaY: 8 * anim1.value),
      child: FadeTransition(
        child: child,
        opacity: anim1,
      ),
    ),
    pageBuilder: (_, animation, secondaryAnimation) =>
        ChangeNotifierProvider.value(
      child: AuthBuild(),
      value: Provider.of<UserVModel>(context, listen: false),
    ),
  );

  return;
}
