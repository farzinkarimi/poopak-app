import 'package:flutter/material.dart';
import 'package:poopak_app/view/date_and_time/date/date_vmodel.dart';
import '../main.dart';
import 'package:provider/provider.dart';
import 'package:shamsi_date/shamsi_date.dart';

class DateLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<DateVModel>(context, listen: false);
    return Selector<DateVModel, int>(
      selector: (_, v) => v.date.length,
      builder: (context, value, child) =>Container(

        height: 90,
        width: MediaQuery.of(context).size.width,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: List.generate(30, (index) {
            var date = Jalali.now().addDays(index);
            bool isSelected=valuable.date.any((element) => element!=date.toString());
            return Container(

              margin: EdgeInsets.symmetric(horizontal: 32),
              height: 64,
              width: 72,
              child: Card(

                color: isSelected
                    ? Colors.white
                    : Theme.of(context).colorScheme.secondary,
                child: FittedBox(
                  child: isSelected
                      ? Column(
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.circle,
                                  size: 10,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 5,
                                  height: 16,
                                ),
                                Icon(
                                  Icons.circle,
                                  size: 10,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            InkWell(
                              onTap: () => valuable.date.add(date.toString()),
                              child: date == Jalali.now()
                                  ? Text(
                                      "امروز",
                                      style: TextStyle(fontSize: 22),
                                    )
                                  : date == Jalali.now().addDays(1)
                                      ? Text(
                                          "فردا",
                                          style: TextStyle(fontSize: 22),
                                        )
                                      : Column(
                                        children: [
                                          Text(
                                              Jalali.now().addDays(index).day.toString(),
                                              style: TextStyle(
                                                  fontSize: 25,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          Text(_format(date.month)),
                                        ],
                                      ),
                            ),
                            Icon(
                              Icons.circle,
                              color: Colors.white,
                            )
                          ],
                        )
                      : Column(
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.circle,
                                  size: 10,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 5,
                                  height: 16,
                                ),
                                Icon(
                                  Icons.circle,
                                  size: 10,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            Container(
                              child: date == Jalali.now()
                                  ? Text(
                                      "امروز",
                                      style: TextStyle(
                                          fontSize: 22, color: Colors.white),
                                    )
                                  : date == Jalali.now().addDays(1)
                                      ? Text(
                                          "فردا",
                                          style: TextStyle(
                                              fontSize: 22,
                                              color: Colors.white),
                                        )
                                      : Text(
                                          Jalali.now()
                                              .addDays(index)
                                              .day
                                              .toString(),
                                          style: TextStyle(
                                              fontSize: 36,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                            ),
                            Text(
                              _getWeekDay(date.toDateTime().weekday),
                              style: TextStyle(color: Colors.white),
                            )
                          ],
                        ),
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}

String _format(int d) {
  switch (d) {
    case 1:
      return 'فروردین';
    case 2:
      return 'اردیبهشت';
    case 3:
      return 'خرداد';
    case 4:
      return 'تیر';
    case 5:
      return 'مرداد';
    case 6:
      return 'شهریور';
    case 7:
      return 'مهر';
    case 8:
      return 'آبان';
    case 9:
      return 'آذر';
    case 10:
      return 'دی';
    case 11:
      return 'بهمن';
    case 12:
      return 'اسفند';

  }
  return 'خطا';}

String _getWeekDay(int d) {
  switch (d) {
    case 1:
      return 'دوشنبه';
    case 2:
      return 'سه شنبه';
    case 3:
      return 'چهارشنبه';
    case 4:
      return 'پنج شنبه';
    case 5:
      return 'جمعه';
    case 6:
      return 'شنبه';
    case 7:
      return 'یک شنبه';
  }
  return 'خطا';
}
