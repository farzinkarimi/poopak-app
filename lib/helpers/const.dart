import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

enum auth { signIn, signUp }
enum LoginType { SignIn, SignUp }

final spinkit = SpinKitFadingCube(
  color: Colors.white,
  size: 50.0,
);
final selectedGenderBorder = Border.all(color: Colors.deepPurple, width: 2);

final unselectedGenderBorder =
Border.all(color: Colors.grey.shade300, width: 2);

class ListOfNumberGenerator extends StatelessWidget {
  ListOfNumberGenerator(
      {required this.value, required this.index, required this.set});

  final int? value;
  final int index;
  final Function set;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      width: 48,
      height: 48,

      margin: const EdgeInsets.symmetric(horizontal: 5,vertical: 8),
      decoration: BoxDecoration(
          border:
          value == index ? selectedGenderBorder : unselectedGenderBorder,
          color: value == index ? Theme
              .of(context)
              .colorScheme
              .secondaryVariant : Colors.transparent,
          borderRadius: BorderRadius.circular(15),
          boxShadow: []),
      duration: Duration(milliseconds: 200),
      child: TextButton(
        child: Text(
          (index).toString(),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: value == index ? Theme
                  .of(context)
                  .colorScheme
                  .surface : Theme
                  .of(context)
                  .colorScheme
                  .primaryVariant,
              ),
        ),
        onPressed: () {
          if (value == index)
            return null;
          else
            set(context, index);
        },
      ),
    );
  }
}

class CustomRaisedButton extends StatelessWidget {
  CustomRaisedButton({required this.onPress,
    required this.color,
    required this.textColor,
    required this.text});

  final Function onPress;
  final Color color;
  final Color textColor;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
          width: double.maxFinite,
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              border: Border.all(color: textColor)),
          child: TextButton(
            onPressed: () => onPress.call(),
            child: Text(
              text,
              style: TextStyle(color: textColor),
            ),
          )),
    );
  }
}


class ExtraWidgetCard extends StatelessWidget {

  ExtraWidgetCard({required this.delete,
    required this.set,
    required this.color,
    required this.value,
    required this.widget});

  final Function delete;
  final Function set;
  final Color color;
  final List<String> value;
  final List<dynamic> widget;

  @override
  Widget build(BuildContext context) {
    bool selected = value.any((element) => element == widget[0]);

    return InkWell(
      onTap: () => selected ? delete() : set(),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          color: selected ? Theme
              .of(context)
              .colorScheme
              .surface : Theme
              .of(context)
              .colorScheme
              .primary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            side: BorderSide(color: selected ? color : Colors.grey, width: 4),
          ),
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 32),
            child: Column(
              children: [
                Expanded(
                  child: Image.network(
                    widget[1].toString(),
                    loadingBuilder: (context, child, loadingProgress) =>
                    loadingProgress != null
                        ? Center(child: CircularProgressIndicator())
                        : child,
                    color: selected ? color : Colors.grey,
                    errorBuilder: (context, error, stackTrace) =>
                        Center(child: Icon(Icons.close)),
                  ),
                  flex: 3,
                ),
                Expanded(
                  child: FittedBox(
                    child: Text(
                      widget[0].toString(),
                      style: TextStyle(color: selected ? color : Colors.grey),
                    ),
                    fit: BoxFit.cover,
                  ),
                )
              ],
            ),
            width: 50,
          ),
        ),
      ),
    );
  }
}

Widget dateSelectButton({required String text,
  required bool isSelected,
  required Color color,
  required Function onsSelect,
  IconData? icon}) {
  return RaisedButton(
    onPressed: () => onsSelect.call(),
    child: Padding(
      padding: EdgeInsets.only(
          right: 8.0, top: 8, bottom: 8, left: icon != null ? 0 : 8),
      child: Row(
        children: [
          Text(
            text,
            style: TextStyle(
                color: isSelected ? Colors.white : Colors.grey,
                fontWeight: FontWeight.bold),
          ),
          icon != null
              ? Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(
              icon,
              color: isSelected ? Colors.white : Colors.grey,
            ),
          )
              : SizedBox.shrink()
        ],
      ),
    ),
    color: isSelected ? color : Colors.transparent,
    elevation: 0,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: BorderSide(color: isSelected ? Colors.transparent : Colors.grey)),
  );
}
