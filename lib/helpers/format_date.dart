import 'package:persian_datetime_picker/persian_datetime_picker.dart';

String format(Date d) {
  final f = d.formatter;

  return '${f.yyyy}/${f.mm}/${f.dd}';
}
