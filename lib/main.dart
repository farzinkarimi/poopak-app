import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:poopak_app/view/home/home_vmodel.dart';
import 'package:poopak_app/view_model/city_vmodel.dart';
import 'package:poopak_app/view_model/product_vmodel.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import 'view/drawer/drawer_vmodel.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:provider/provider.dart';


void main() {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final _appRouter = AppRouter();

  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => UserVModel()),
        ChangeNotifierProvider(
          create: (_) => ProductsVModel(),
        ),
        ChangeNotifierProvider(
          create: (_) => CityVModel(),
        ),
        ChangeNotifierProvider(
          create: (_) => HomeVModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => DrawerVModel(),
        )
      ],
      child: MaterialApp.router(

        routerDelegate: AutoRouterDelegate(_appRouter),
        routeInformationParser: _appRouter.defaultRouteParser(),
        theme: ThemeData(
          textTheme: TextTheme(headline1: TextStyle(fontSize: 12)),
          colorScheme: ColorScheme(
              primary: Color(0xFF5f51ba),
              primaryVariant: Colors.grey,
              secondary: Color(0xFF5f51ba),
              secondaryVariant: Colors.deepPurple,
              surface: Colors.white,
              background: Colors.blueGrey.shade50,
              error: Colors.red,
              onPrimary: Colors.black,
              onSecondary: Colors.black87,
              onSurface: Colors.black54,
              onBackground: Colors.grey.shade200,
              onError: Colors.red,
              brightness: Brightness.light),
          backgroundColor: Colors.blueGrey[50],
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'NotoArabic',
        ),
      ),
    );
  }
}


