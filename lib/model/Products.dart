
class ProductItem {
  late String title;
  late String image;
  late String fullDetails;
  late int index = 2;
  late int key;
  late List<dynamic> option;
  late List<dynamic> category;
  late String subtitle;

  ProductItem(
      {required this.title,
        required this.subtitle,
      required this.option,
      required this.index,
      required this.fullDetails,
      required this.image,
      required this.category,
      required this.key});

  ProductItem.fromJson(Map<String, dynamic> parsedJson) {
    category = parsedJson['catagory'];
    title = parsedJson['title'];
    subtitle = parsedJson['subtitle'];
    image = parsedJson['image'];
    key = parsedJson["key"] as int;
    fullDetails = parsedJson['details'];
    index = int.parse(parsedJson['index']);
    option = parsedJson['options'];
  }
}
