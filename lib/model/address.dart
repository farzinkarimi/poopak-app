import 'package:latlong2/latlong.dart';

class UserAddress {
 late String addressEntered;
 late String provinceCity;
 late String name;
 late LatLng latLng;

  UserAddress({required this.addressEntered,
    required this.latLng,
    required this.name,
    required this.provinceCity});


 factory UserAddress.fromJson(Map<String, dynamic> data){
     return UserAddress(addressEntered: data["addressEntered"],
        latLng: LatLng.fromJson(data["latLng"]),
        name: data["name"],
        provinceCity: data["provinceCity"]);
  }

  Map<String, dynamic> toJson() =>
      {
        'addressEntered': addressEntered,
        'provinceCity': provinceCity,
        'name': name,
        'latLng': latLng.toJson(),
      };

@override
  String toString()=> "name: $name addressEntered: $addressEntered provinceCity:$provinceCity latlng:${latLng.toString()}";
}
