class City {
  int data;
  String name;

  City({required this.name, required this.data});

  factory City.fromJson(Map<String, dynamic> json) {
    return City(name: json["name"], data: json["data"]);
  }

  @override
  toString() => "name: $name data: $data";
}
