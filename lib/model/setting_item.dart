import 'package:flutter/material.dart';

class SettingsItemModel {
  late IconData icon;
  late Color color;
  late String title;
  late String description;
  late Function onTab;

  SettingsItemModel(
      {required this.color,
      required this.description,
      required this.icon,
      required this.title,
      required this.onTab});

  SettingsItemModel.fromJson(Map<String, dynamic> parsedJson) {
    icon = parsedJson['icon'];
    color = parsedJson['color'];
    title = parsedJson['title'];
    description = parsedJson['description'];
    onTab=(){};
  }

  @override
  toString() {
    return this.title;
  }
}
