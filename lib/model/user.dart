import 'package:flutter/material.dart';
import 'package:poopak_app/model/address.dart';
import 'package:poopak_app/helpers/const.dart';

class User {
  String name;
  String? email;
  String? birthDay;
  String? gender;
  String mobile;
  String idToken;

  User(
      {required this.name,
      this.email,
      this.birthDay,
      this.gender,
      required this.mobile,
      required this.idToken});

  factory User.fromJson(Map<String, dynamic> data) {
    return User(
        mobile: data["mobile"],
        name: data["name"],
        email: data["email"],
        gender: data["gender"],
        birthDay: data["birthDay"],
        idToken: data["idToken"]);
  }

  Map<String, dynamic> toJson() => {
        'mobile': mobile,
        'email': email,
        'name': name,
        'gender': gender,
        'birthDay': birthDay,
        'idToken': idToken
      };

  @override
  String toString() => "name: $name mobile: $mobile idToken: $idToken birthday: $birthDay gender: $gender email: $email";
}
