import 'package:flutter/material.dart';


enum DrawerIndex {
  HOME,
  Pay,
  Share,
  ContactUs,
  About,
}

class DrawerItemData {
  DrawerItemData({
     this.isAssetsImage = false,
    this.labelName = '',
    required this.icon,
    required this.index,
    this.imageName = '',
  });

  String labelName;
  Icon icon;
  bool isAssetsImage;
  String imageName;
  DrawerIndex index;
}