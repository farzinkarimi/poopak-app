

class CategoryItem{
  late String title;
  late String image;
  late List<dynamic> products;

  CategoryItem({required this.title,required this.image,required this.products});


  CategoryItem.fromJson(Map<String, dynamic> parsedJson){
    title = parsedJson['title'];
    image = parsedJson['image'];
    products = parsedJson['keys'];
  }
}