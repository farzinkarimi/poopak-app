


class Clock {
  late int hour;
  late int minute;

  Clock({required this.hour,required this.minute});
  Clock.fromString({required String s}){
    final end=s.indexOf(":");
    hour=int.parse(s.substring(0,end));
    minute=int.parse(s.substring(end+1));

  }
  Clock addHour(int hour){
    this.hour=(hour+this.hour)%24;
    return this;
  }
  Clock addMinute(int minute){
    this.minute=(minute+this.minute)%60;
    return this;

  }
  String showClock({int hour=0,int min=0}){
    hour=(hour+this.hour)%24;
    min=(min+this.minute)%60;

    return"${hour.toString()}:${min.toString()}";

  }
//  bool hasData(){
//    if(hour!=null)return true;
//    else return false;
//  }

}