import 'package:flutter/material.dart';
import 'package:location/location.dart';

class Order {
  late String typeOfOrder;
  late String date;
  late int price;
  late String status;
  late String key;
  late String payType;
  late LocationData location;

  Order({required this.typeOfOrder,
    required this.date,
    required this.price,
    required this.status,
    required this.key,
    required this.payType,
    required dynamic location}) {
    this.location = LocationData.fromMap(location);
  }


  Order.fromJson(Map <String,dynamic> json){
    typeOfOrder=json["typeOfOrder"];
    date=json["date"];
    price=json["price"];
    status=json["status"];
    key=json["key"];
    payType=json["payType"];
    location=LocationData.fromMap(json["location"]);

  }
}
