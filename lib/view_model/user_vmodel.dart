import 'package:flutter/foundation.dart';
import 'package:poopak_app/model/user.dart';
import 'package:poopak_app/services/auth_service/API/get_auth_apis.dart';
import 'package:poopak_app/services/auth_service/API/get_auth_locally.dart';
import 'package:poopak_app/services/auth_service/auth_service.dart';

class UserVModel extends ChangeNotifier {
  User? _user;
  late AuthService _authService;

  UserVModel() {
    _authService = AuthService(GetAddressAPIs(), GetAuthLocally());
  }

  bool _isAuthenticated = false;

  bool get isAuthenticated => _isAuthenticated;

  User? get user => _user;

  set user(User? u) {
    if (u != null) {
      _user = u;
      _authService.authLocal.saveInStorage(key: "mobile", value: u.mobile);
      _authService.authLocal.saveInStorage(key: "idToken", value: u.idToken);
      changeAuth(true);
    }
    else{
      _user=u;
      changeAuth(false);
    }
  }

  void changeAuth(bool auth) {
    _isAuthenticated = auth;
    notifyListeners();
  }

  Future<bool> _requestSignInByIdToken(String mobile, String idToken) async {
    Map<String, dynamic>? response;
    try {
      response = await _authService.addressAPI.checkEnteredMobile(mobile);
    } catch (e) {
      print(e);

      return false;
    }
    if (response != null) {
      user = User.fromJson(response);
      return true;
    } else {
      return false;
    }
  }

  Future<bool> requestAutoAuth() async {
    final List<String>? response =
        await _authService.authLocal.autoAuthenticate();
    if (response != null) {
      try {
        return await _requestSignInByIdToken(response[1], response[0]);
      } catch (e) {
        print(e);

        return false;
      }
    } else {
      return false;
    }
  }

  Future<void> logout() async {
    user = null;
    _authService.authLocal.removeAuthData();
  }
}
