import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:poopak_app/model/CategoryItem.dart';
import 'package:poopak_app/model/Products.dart';
import 'package:poopak_app/model/city.dart';
import 'package:poopak_app/services/product_service/api/get_product_apis.dart';
import 'package:poopak_app/services/product_service/product_service.dart';

class ProductsVModel extends ChangeNotifier {
  List<ProductItem> products = [];

  List<CategoryItem> categoryItems = [];
  late ProductService _productService;




  ProductsVModel() {
    _productService = ProductService(GetProductsAPIs());
  }



  Future<void> deleteProducts() async {
    products = [];
    categoryItems = [];

  }

  Future<bool> getProducts(City city) async {
    final response = await _productService.productAPI.getProducts(city);

    if (response != null) {
      for (int i = 0; i < response.length; i++) {
        if (response[i]["key"] != null)
          products.add(ProductItem.fromJson(response[i]));
        else
          categoryItems.add(CategoryItem.fromJson(response[i]));
      }
      return true;
    } else {
      return false;
    }
  }
}
