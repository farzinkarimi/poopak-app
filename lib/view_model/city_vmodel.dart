import 'package:flutter/material.dart';
import 'package:poopak_app/model/city.dart';
import 'package:poopak_app/services/auth_service/API/get_auth_locally.dart';
import 'package:poopak_app/services/city_service/api/get_city_apis.dart';
import 'package:poopak_app/services/city_service/api/get_city_locally.dart';
import 'package:poopak_app/services/city_service/city_service.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class CityVModel extends ChangeNotifier {
  List<City>? city;

  City? _selectedCity;
  late CityService _cityService;

  //getter
  City? get selectedCity => _selectedCity;

  //setter
  set selectedCity(City? newCity) {
    _selectedCity = newCity;
    notifyListeners();
  }



  CityVModel() {
    _cityService = CityService(GetCityAPIs(), GetCityLocally());
  }

  Future<bool> changeCityLocally(City c) async {
    final b = await _cityService.cityLocal.saveCity(c.name);
    if (b) {
      selectedCity = c;
      return true;
    } else {
      return false;
    }
  }

  Future<bool> requestCities() async {
    final cities = await _cityService.cityAPI.getCities();
    if (cities != null) {
      city = cities;
      await getSelectedCity();
      return true;
    } else {
      return false;
    }
  }
  Future<bool> deleteDataLocally() async {
    final b = await _cityService.cityLocal.deleteCity();
    if (b) {
      selectedCity = null;
      city=null;
      return true;
    } else {
      return false;
    }
  }
  void deleteData()  {
      selectedCity = null;
      city=null;
    }

  Future<void> getSelectedCity() async {
    final tempCity = await _cityService.cityLocal.getCity();
    if (tempCity != null) {
      selectedCity = city!.firstWhere((element) => element.name == tempCity);
      if(selectedCity==null){
        selectedCity = City(name: "مریوان", data: 8623);

      }
    } else {
      selectedCity = City(name: "مریوان", data: 8623);
    }
  }
}
