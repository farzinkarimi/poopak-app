import 'package:auto_route/auto_route.dart';
import 'package:poopak_app/view/buy/BuySetTime.dart';
import 'package:poopak_app/view/buy/LocationAndTime.dart';
import 'package:poopak_app/view/buy/PaymentTab.dart';
import 'package:poopak_app/view/buy/ordering_page.dart';
import 'package:poopak_app/view/category/category_page.dart';
import '../view/home/home_parent.dart';

import '../view/buy/product_peek.dart';
import '../view/setting/setting_page.dart';
import '../view/home/homePage.dart';
import 'package:poopak_app/view/order/order_page.dart';
import '../view/setting/page/inbox_page.dart';
import '../view/setting/page/rules_page.dart';
import '../view/setting/page/support_page.dart';
import '../view/setting/page/takhfif_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    CustomRoute(
      path: "product",
      name: 'productRouter',
      page: ProductPeek,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    AutoRoute(
      path: "/buy",
      name: "BuyRouter",
      page: BuyPage,
      children: [
        CustomRoute(
            path: '',
            page: OrderingPage,
            transitionsBuilder: TransitionsBuilders.fadeIn),
        CustomRoute(
            path: 'locationRoute',
            page: LocationAndTime,
            name: "locationRouter",
            transitionsBuilder: TransitionsBuilders.fadeIn),
        CustomRoute(
            path: 'paymentRoute',
            page: PaymentTab,
            name: "paymentRouter",
            transitionsBuilder: TransitionsBuilders.fadeIn),
        RedirectRoute(path: '*', redirectTo: ''),
      ],
    ),
    AutoRoute(page: CategoryPage, path: "category"),
    AutoRoute(
      page: HomeParent,
      name: "firstRouter",
      initial: true,
      path: "/",
      children: [
        AutoRoute(
          path: "home",
          page: HomePage,
          name: "homeRouter",
        ),
        AutoRoute(page: OrderPage, path: "order", name: "orderRouter"),
        AutoRoute(
            path: "setting",
            name: "settingRouter",
            page: EmptyRouterPage,
            children: [
              CustomRoute(
                path: "",
                page: SettingPage,
              ),
              CustomRoute(path: "inbox", page: InboxPage),
              CustomRoute(path: "rule", page: RulesPage),
              CustomRoute(path: "support", page: SupportPage),
              CustomRoute(path: "takhfif", page: TakhfifPage)
            ]),
      ],
    ),
  ],
)
class $AppRouter {}
