// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i10;
import 'package:flutter/material.dart' as _i16;

import '../model/CategoryItem.dart' as _i18;
import '../model/Products.dart' as _i17;
import '../view/buy/BuySetTime.dart' as _i2;
import '../view/buy/LocationAndTime.dart' as _i6;
import '../view/buy/ordering_page.dart' as _i5;
import '../view/buy/PaymentTab.dart' as _i7;
import '../view/buy/product_peek.dart' as _i1;
import '../view/category/category_page.dart' as _i3;
import '../view/home/home_parent.dart' as _i4;
import '../view/home/homePage.dart' as _i8;
import '../view/order/order_page.dart' as _i9;
import '../view/setting/page/inbox_page.dart' as _i12;
import '../view/setting/page/rules_page.dart' as _i13;
import '../view/setting/page/support_page.dart' as _i14;
import '../view/setting/page/takhfif_page.dart' as _i15;
import '../view/setting/setting_page.dart' as _i11;

class AppRouter extends _i10.RootStackRouter {
  AppRouter([_i16.GlobalKey<_i16.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i10.PageFactory> pagesMap = {
    ProductRouter.name: (routeData) {
      final args = routeData.argsAs<ProductRouterArgs>();
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i1.ProductPeek(args.product),
          transitionsBuilder: _i10.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    BuyRouter.name: (routeData) {
      final args = routeData.argsAs<BuyRouterArgs>();
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: _i2.BuyPage(args.product));
    },
    CategoryRoute.name: (routeData) {
      final args = routeData.argsAs<CategoryRouteArgs>();
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: _i3.CategoryPage(args.category));
    },
    FirstRouter.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: _i4.HomeParent());
    },
    OrderingRoute.name: (routeData) {
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i5.OrderingPage(),
          transitionsBuilder: _i10.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    LocationRouter.name: (routeData) {
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i6.LocationAndTime(),
          transitionsBuilder: _i10.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    PaymentRouter.name: (routeData) {
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i7.PaymentTab(),
          transitionsBuilder: _i10.TransitionsBuilders.fadeIn,
          opaque: true,
          barrierDismissible: false);
    },
    HomeRouter.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: _i8.HomePage());
    },
    OrderRouter.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: _i9.OrderPage());
    },
    SettingRouter.name: (routeData) {
      return _i10.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i10.EmptyRouterPage());
    },
    SettingRoute.name: (routeData) {
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i11.SettingPage(),
          opaque: true,
          barrierDismissible: false);
    },
    InboxRoute.name: (routeData) {
      final args = routeData.argsAs<InboxRouteArgs>();
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i12.InboxPage(key: args.key, massages: args.massages),
          opaque: true,
          barrierDismissible: false);
    },
    RulesRoute.name: (routeData) {
      final args = routeData.argsAs<RulesRouteArgs>();
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i13.RulesPage(args.rules),
          opaque: true,
          barrierDismissible: false);
    },
    SupportRoute.name: (routeData) {
      final args = routeData.argsAs<SupportRouteArgs>();
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i14.SupportPage(args.options, args.title),
          opaque: true,
          barrierDismissible: false);
    },
    TakhfifRoute.name: (routeData) {
      final args = routeData.argsAs<TakhfifRouteArgs>();
      return _i10.CustomPage<dynamic>(
          routeData: routeData,
          child: _i15.TakhfifPage(gifts: args.gifts, offs: args.offs),
          opaque: true,
          barrierDismissible: false);
    }
  };

  @override
  List<_i10.RouteConfig> get routes => [
        _i10.RouteConfig(ProductRouter.name, path: 'product'),
        _i10.RouteConfig(BuyRouter.name, path: '/buy', children: [
          _i10.RouteConfig(OrderingRoute.name,
              path: '', parent: BuyRouter.name),
          _i10.RouteConfig(LocationRouter.name,
              path: 'locationRoute', parent: BuyRouter.name),
          _i10.RouteConfig(PaymentRouter.name,
              path: 'paymentRoute', parent: BuyRouter.name),
          _i10.RouteConfig('*#redirect',
              path: '*',
              parent: BuyRouter.name,
              redirectTo: '',
              fullMatch: true)
        ]),
        _i10.RouteConfig(CategoryRoute.name, path: 'category'),
        _i10.RouteConfig(FirstRouter.name, path: '/', children: [
          _i10.RouteConfig(HomeRouter.name,
              path: 'home', parent: FirstRouter.name),
          _i10.RouteConfig(OrderRouter.name,
              path: 'order', parent: FirstRouter.name),
          _i10.RouteConfig(SettingRouter.name,
              path: 'setting',
              parent: FirstRouter.name,
              children: [
                _i10.RouteConfig(SettingRoute.name,
                    path: '', parent: SettingRouter.name),
                _i10.RouteConfig(InboxRoute.name,
                    path: 'inbox', parent: SettingRouter.name),
                _i10.RouteConfig(RulesRoute.name,
                    path: 'rule', parent: SettingRouter.name),
                _i10.RouteConfig(SupportRoute.name,
                    path: 'support', parent: SettingRouter.name),
                _i10.RouteConfig(TakhfifRoute.name,
                    path: 'takhfif', parent: SettingRouter.name)
              ])
        ])
      ];
}

/// generated route for [_i1.ProductPeek]
class ProductRouter extends _i10.PageRouteInfo<ProductRouterArgs> {
  ProductRouter({required _i17.ProductItem product})
      : super(name, path: 'product', args: ProductRouterArgs(product: product));

  static const String name = 'ProductRouter';
}

class ProductRouterArgs {
  const ProductRouterArgs({required this.product});

  final _i17.ProductItem product;
}

/// generated route for [_i2.BuyPage]
class BuyRouter extends _i10.PageRouteInfo<BuyRouterArgs> {
  BuyRouter(
      {required _i17.ProductItem product, List<_i10.PageRouteInfo>? children})
      : super(name,
            path: '/buy',
            args: BuyRouterArgs(product: product),
            initialChildren: children);

  static const String name = 'BuyRouter';
}

class BuyRouterArgs {
  const BuyRouterArgs({required this.product});

  final _i17.ProductItem product;
}

/// generated route for [_i3.CategoryPage]
class CategoryRoute extends _i10.PageRouteInfo<CategoryRouteArgs> {
  CategoryRoute({required _i18.CategoryItem category})
      : super(name,
            path: 'category', args: CategoryRouteArgs(category: category));

  static const String name = 'CategoryRoute';
}

class CategoryRouteArgs {
  const CategoryRouteArgs({required this.category});

  final _i18.CategoryItem category;
}

/// generated route for [_i4.HomeParent]
class FirstRouter extends _i10.PageRouteInfo<void> {
  const FirstRouter({List<_i10.PageRouteInfo>? children})
      : super(name, path: '/', initialChildren: children);

  static const String name = 'FirstRouter';
}

/// generated route for [_i5.OrderingPage]
class OrderingRoute extends _i10.PageRouteInfo<void> {
  const OrderingRoute() : super(name, path: '');

  static const String name = 'OrderingRoute';
}

/// generated route for [_i6.LocationAndTime]
class LocationRouter extends _i10.PageRouteInfo<void> {
  const LocationRouter() : super(name, path: 'locationRoute');

  static const String name = 'LocationRouter';
}

/// generated route for [_i7.PaymentTab]
class PaymentRouter extends _i10.PageRouteInfo<void> {
  const PaymentRouter() : super(name, path: 'paymentRoute');

  static const String name = 'PaymentRouter';
}

/// generated route for [_i8.HomePage]
class HomeRouter extends _i10.PageRouteInfo<void> {
  const HomeRouter() : super(name, path: 'home');

  static const String name = 'HomeRouter';
}

/// generated route for [_i9.OrderPage]
class OrderRouter extends _i10.PageRouteInfo<void> {
  const OrderRouter() : super(name, path: 'order');

  static const String name = 'OrderRouter';
}

/// generated route for [_i10.EmptyRouterPage]
class SettingRouter extends _i10.PageRouteInfo<void> {
  const SettingRouter({List<_i10.PageRouteInfo>? children})
      : super(name, path: 'setting', initialChildren: children);

  static const String name = 'SettingRouter';
}

/// generated route for [_i11.SettingPage]
class SettingRoute extends _i10.PageRouteInfo<void> {
  const SettingRoute() : super(name, path: '');

  static const String name = 'SettingRoute';
}

/// generated route for [_i12.InboxPage]
class InboxRoute extends _i10.PageRouteInfo<InboxRouteArgs> {
  InboxRoute({_i16.Key? key, required List<String> massages})
      : super(name,
            path: 'inbox', args: InboxRouteArgs(key: key, massages: massages));

  static const String name = 'InboxRoute';
}

class InboxRouteArgs {
  const InboxRouteArgs({this.key, required this.massages});

  final _i16.Key? key;

  final List<String> massages;
}

/// generated route for [_i13.RulesPage]
class RulesRoute extends _i10.PageRouteInfo<RulesRouteArgs> {
  RulesRoute({required String rules})
      : super(name, path: 'rule', args: RulesRouteArgs(rules: rules));

  static const String name = 'RulesRoute';
}

class RulesRouteArgs {
  const RulesRouteArgs({required this.rules});

  final String rules;
}

/// generated route for [_i14.SupportPage]
class SupportRoute extends _i10.PageRouteInfo<SupportRouteArgs> {
  SupportRoute({required List<dynamic> options, required String title})
      : super(name,
            path: 'support',
            args: SupportRouteArgs(options: options, title: title));

  static const String name = 'SupportRoute';
}

class SupportRouteArgs {
  const SupportRouteArgs({required this.options, required this.title});

  final List<dynamic> options;

  final String title;
}

/// generated route for [_i15.TakhfifPage]
class TakhfifRoute extends _i10.PageRouteInfo<TakhfifRouteArgs> {
  TakhfifRoute({required List<String> gifts, required List<String> offs})
      : super(name,
            path: 'takhfif', args: TakhfifRouteArgs(gifts: gifts, offs: offs));

  static const String name = 'TakhfifRoute';
}

class TakhfifRouteArgs {
  const TakhfifRouteArgs({required this.gifts, required this.offs});

  final List<String> gifts;

  final List<String> offs;
}
