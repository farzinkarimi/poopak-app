import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

class PayScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: false,
          title: Padding(
            padding: const EdgeInsets.only(top: 16, right: 16),
            child: Text("صفحه پرداخت"),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          actions: [
            Padding(
                padding: const EdgeInsets.only(left: 24.0, top: 16),
                child: Icon(Icons.account_balance_wallet)
                    .iconColor(Colors.black54))
          ],
        ),
        body: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 48,
                  ),
                  Text('موجودی').textColor(Colors.grey),
                  Text("57000".replaceAllMapped(_reg, _mathFunc) + " تومان ")
                      .fontSize(32),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 32.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () {},
                              child: Text("افزایش موجودی").textColor(Colors.white),
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all(
                                  StadiumBorder(),
                                ),
                              ),
                            ).padding(right: 8).height(52),
                          ),
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () {},
                              child: Text("برداشت").textColor(Colors.white),
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all(
                                  StadiumBorder(),
                                ),
                              ),
                            ).padding(right: 8).height(52),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Divider(),
                ],
              ),
            ),
            SizedBox(
              height: 12,
            ),
            ListTile(
              horizontalTitleGap: 4,
              leading: Icon(
                Icons.arrow_downward,
                color: Colors.red,
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text("برداشت"), Text("3000 تومان")],
              ),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("15/11/1378 11:30"),
                  Text("کد پیگیری : 4484354675")
                ],
              ),
            ),
            ListTile(
              horizontalTitleGap: 4,
              leading: Icon(
                Icons.arrow_downward,
                color: Colors.red,
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text("برداشت"), Text("3000 تومان")],
              ),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("15/11/1378 11:30"),
                  Text("کد پیگیری : 4484354675")
                ],
              ),
            ),
            ListTile(
              horizontalTitleGap: 4,
              leading: Icon(
                Icons.arrow_upward,
                color: Colors.green,
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text("واریزی به شرکت"), Text("3000 تومان")],
              ),
              subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("15/11/1378 11:30"),
                  Text("کد پیگیری : 4484354675")
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

RegExp _reg = RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
String Function(Match) _mathFunc = (Match match) => '${match[1]},';
