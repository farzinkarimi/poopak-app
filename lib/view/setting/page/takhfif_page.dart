import 'package:flutter/material.dart';

class TakhfifPage extends StatelessWidget {
  TakhfifPage({required this.gifts, required this.offs});

  final List<String> offs;
  final List<String> gifts;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: Text("تخفیف ها و جایزه ها",style: TextStyle(color: Colors.white),),
            bottom: PreferredSize(
                child: TabBar(
                  
                  labelColor: Colors.white,
                  tabs: [
                    Tab(
                      text: 'تخفیف ها',
                    ),
                    Tab(
                      text: 'جایزه ها',
                    )
                  ],
                  indicatorColor: Colors.white,
                  automaticIndicatorColorAdjustment: true,
                ),
                preferredSize: Size.fromHeight(50)),
          ),
          body: TabBarView(
              physics: BouncingScrollPhysics(), children: [tab1(), tab2()]),
        ));
  }

  Widget tab1() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: offs.length,
      itemBuilder: (context, index) => ListTile(
        title: ListTile(title: Text(offs[index])),
      ),
    );
  }

  Widget tab2() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: gifts.length,
      itemBuilder: (context, index) => ListTile(
        title: Text(gifts[index]),
      ),
    );
  }
}
