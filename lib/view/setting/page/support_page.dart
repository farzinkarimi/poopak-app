import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';

class SupportPage extends StatelessWidget {
  SupportPage(this.options, this.title);

  final List<dynamic> options;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(title,style: TextStyle(fontWeight: FontWeight.bold),),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 24),
        child: ListView.builder(
          itemCount: options.length,
          shrinkWrap: true,
          itemBuilder: (context, index) => Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Card(
              child: ListTile(
                contentPadding: EdgeInsets.symmetric(horizontal: 32,vertical: 4),
                onTap: () {
                  if(options[index]["options"]!=null)
                  AutoRouter.of(context).push(SupportRoute(
                      options: options[index]["options"],
                      title: options[index]["name"]));
                },
                title: Text(options[index]["name"]),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
