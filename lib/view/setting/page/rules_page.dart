import 'package:flutter/material.dart';

class RulesPage extends StatelessWidget {
  RulesPage( this.rules);

  final String rules;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text("قوانین و مقررات"),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 16,horizontal: 24),
        child: SingleChildScrollView(
          child: Align(
              alignment: Alignment.topRight,
              child: Text(rules)),
        ),
      ),
    );
  }
}
