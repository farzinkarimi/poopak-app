import 'package:flutter/material.dart';
import 'package:styled_widget/styled_widget.dart';

class InboxPage extends StatelessWidget {
  const InboxPage({Key? key, required this.massages}) : super(key: key);
  final List<String> massages;

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text("پیام ها").fontWeight(FontWeight.bold),
        ),
        body: ListView.builder(
          physics:
              AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
          itemCount: massages.length,
          itemBuilder: (context, index) {
            return Card(
              child: ExpansionTile(
                  title: Text(massages[index]).bold().fontSize(16),
                  children: List.generate(
                      30, (_) => Text(massages[index]).bold().fontSize(16))),
            ).padding(horizontal: 12);
          },
        ),
      ),
    );
  }
}
