


import 'package:flutter/foundation.dart';

class SettingVModel extends ChangeNotifier{
  String? _pressedItem;

  String? get pressedItem=>_pressedItem;

  set pressedItem(String? s){
    _pressedItem=s;
    notifyListeners();
  }
}