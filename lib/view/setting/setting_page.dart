import 'dart:convert';
import 'dart:math';
import 'package:poopak_app/helpers/CustomShowDialog.dart';
import 'package:poopak_app/model/setting_item.dart';
import 'package:poopak_app/view/setting/setting_vmodel.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import 'package:rive/rive.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class SettingPage extends StatefulWidget {
  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    var userService = Provider.of<UserVModel>(context, listen: false);

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
      child: FutureBuilder<List<SettingsItemModel>>(
        future: getSettingList(context),
        builder: (_, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              {
                return Center(
                    child: RiveAnimation.asset(
                  'assets/rive/loading4.riv',
                  fit: BoxFit.fill,
                ));
              }
            default:
              if (snapshot.hasError) {
                print(snapshot.error.toString());
                return Text("error");
              } else {
                return <Widget>[
                  UserCard(),
                  ActionsRow(),
                  Settings(snapshot.data!),
                ]
                    .toColumn()
                    .padding(vertical: 30, horizontal: 20)
                    .scrollable(physics: BouncingScrollPhysics());
              }
          }
        },
      ),
    );
  }

  Future<List<SettingsItemModel>> getSettingList(BuildContext context) async {
    final _random = Random();
    http.Response response;
    try {
      response = await http.get(
          Uri.parse(
            "https://api.json-generator.com/templates/10hUrKSEls9S/data",
          ),
          headers: {
            "Authorization": "Bearer 71f3ii0o09eshcs49425vr8jpiz4wdlupqj9yls8"
          }).timeout(Duration(seconds: 10));
    } catch (e) {
      print(e);
      return [];
    }
    final data = jsonDecode(response.body);
    List<SettingsItemModel> widgets;
    widgets = data.map<SettingsItemModel>((e) {
      final int icon=e["icon"];
      return SettingsItemModel(
          color: Colors.primaries[_random.nextInt(Colors.primaries.length)]
                  [_random.nextInt(9) * 100] ??
              Theme.of(context).colorScheme.secondary,
          description: e["title"].toString(),
          icon: IconData(icon, fontFamily: 'MaterialIcons'),
          title: e["title"].toString(),
          onTab: () {
            switch (e["title"]) {
              case "پیام ها":
                context.router.push(InboxRoute(
                    massages: List<String>.from(e["options"]["پیام ها"])));
                break;
              case "قوانین و مقررات":
                context.router
                    .push(RulesRoute(rules: e["options"]["قوانین"].toString()));
                break;
              case "پشتیبانی":
                context.router.push(
                  SupportRoute(
                    title: e["title"].toString(),
                    options: [e["options"]],
                  ),
                );
                break;
              case "تخفیف ها و جایزه ها":
                context.router.push(TakhfifRoute(
                    gifts: List<String>.from(e["options"]["تخفیف ها"]),
                    offs: List<String>.from(e["options"]["جایزه ها"])));
            }
          });
    }).toList();
    return widgets;
  }
}

class UserCard extends StatelessWidget {
  Widget _buildUserRow({required BuildContext context, required String title}) {
    return <Widget>[
      const Icon(Icons.account_circle)
          .decorated(
            color: Theme.of(context).colorScheme.surface,
            borderRadius: BorderRadius.circular(30),
          )
          .constrained(height: 50, width: 50)
          .padding(left: 16),
      <Widget>[
        Text(
          title,
          style: TextStyle(
            color: Theme.of(context).colorScheme.surface,
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ).padding(bottom: 5),
        Text(
          'نمایش اطلاعات کاربری',
          style: TextStyle(
            color: Theme.of(context).colorScheme.surface.withOpacity(0.6),
            fontSize: 12,
          ),
        ),
      ].toColumn(crossAxisAlignment: CrossAxisAlignment.start),
    ].toRow();
  }

  Widget _buildUserStatsItem(String value, String text) => <Widget>[
        Text(value).fontSize(20).textColor(Colors.white).padding(bottom: 5),
        Text(text).textColor(Colors.white.withOpacity(0.6)).fontSize(12),
      ].toColumn();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => customAlertDialog(context),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.secondary,
          borderRadius: BorderRadius.circular(20),
        ),
        alignment: Alignment.center,
        child: Consumer<UserVModel>(builder: (_, value, child) => _buildUserRow(
            context: context,
            title: value.user != null
                ? value.user!.name
                : "لطفا وارد شوید"),),
      ),
    );
  }
}

class ActionsRow extends StatelessWidget {
  Widget _buildActionItem(String name, IconData icon) {
    final Widget actionIcon = Icon(icon, size: 20, color: Color(0xFF42526F))
        .alignment(Alignment.center)
        .ripple()
        .constrained(width: 50, height: 50)
        .backgroundColor(Color(0xfff6f5f8))
        .clipOval()
        .padding(bottom: 5);

    final Widget actionText = Text(
      name,
      style: TextStyle(
        color: Colors.black.withOpacity(0.8),
        fontSize: 12,
      ),
    );

    return <Widget>[
      actionIcon,
      actionText,
    ].toColumn().padding(vertical: 20);
  }

  @override
  Widget build(BuildContext context) => <Widget>[
        _buildActionItem('پرداختی ها', Icons.attach_money),
        _buildActionItem('تغییرات ظاهری', Icons.settings),
        _buildActionItem('اطلاعیه ها', Icons.room_service),
      ].toRow(mainAxisAlignment: MainAxisAlignment.spaceAround);
}

class Settings extends StatelessWidget {
  final List<SettingsItemModel> settingItems;

  Settings(this.settingItems);

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => SettingVModel(),
        child: settingItems
            .map(
              (settingsItem) => SettingsItem(
                icon: settingsItem.icon,
                iconBgColor: settingsItem.color,
                title: settingsItem.title,
                description: settingsItem.description,
                onTab: settingsItem.onTab,
              ),
            )
            .toList()
            .toColumn(),
      );
}

class SettingsItem extends StatefulWidget {
  SettingsItem(
      {required this.icon,
      required this.iconBgColor,
      required this.title,
      required this.description,
      required this.onTab});

  final IconData icon;
  final Color iconBgColor;
  final String title;
  final String description;
  final Function onTab;

  @override
  _SettingsItemState createState() => _SettingsItemState();
}

class _SettingsItemState extends State<SettingsItem> {
  @override
  Widget build(BuildContext context) {
    final Widget icon = Icon(widget.icon,
            size: 20, color: Theme.of(context).colorScheme.surface)
        .padding(all: 12)
        .decorated(
          color: widget.iconBgColor,
          borderRadius: BorderRadius.circular(30),
        )
        .padding(left: 15, right: 10);

    final Widget title = Text(
      widget.title,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        color: Theme.of(context).colorScheme.onSecondary,
        fontSize: 16,
      ),
    ).padding(bottom: 5);

    final Widget description = Text(
      widget.description,
    ).textStyle(
      TextStyle(
        color: Colors.grey.shade400,
        fontWeight: FontWeight.bold,
        fontSize: 12,
      ),
    );

    return Consumer<SettingVModel>(
      child: <Widget>[
        icon,
        <Widget>[
          title,
          description,
        ]
            .toColumn(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
            )
            .fittedBox(),
      ]
          .toRow()
          .alignment(Alignment.center)
          .borderRadius(all: 15)
          .ripple()
          .backgroundColor(Theme.of(context).colorScheme.surface, animate: true)
          .clipRRect(all: 25) // clip ripple
          .borderRadius(all: 25, animate: true),
      builder: (_, sProvider, child) {
        final bool = sProvider.pressedItem == widget.title;

        return child!
            .elevation(
              bool ? 0 : 20,
              borderRadius: BorderRadius.circular(25),
              shadowColor: Color(0x30000000),
            ) // shadow borderRadius
            .constrained(height: 80)
            .padding(vertical: 12) // margin
            .gestures(
              onTap: () async {
                sProvider.pressedItem = widget.title;
                await Future.delayed(Duration(milliseconds: 200));
                sProvider.pressedItem = null;
                widget.onTab.call();
              },
            )
            .scale(all: bool ? 0.95 : 1.0, animate: true)
            .animate(Duration(milliseconds: 100), Curves.easeOut);
      },
    );
  }
}
