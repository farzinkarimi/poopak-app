import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/view/buy/design/list_tile_design.dart';
import '../buy_vmodel.dart';
import 'package:provider/provider.dart';

class CustomerGender extends StatelessWidget {

  void set(BuildContext context, String genderTitle) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    valuable.validator.removeWhere((element) => element=="جنسیت بیمار");
    valuable.deleteSession("جنسیت بیمار");

    return valuable.addSession("جنسیت بیمار", genderTitle);
  }

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["جنسیت بیمار"] == null) {
      valuable.validator.add("جنسیت بیمار");
    }
    return BuyListTile(
      trailing: Selector<BuyVModel, String?>(
        builder: (context, value, child) => value != null
            ? Icon(Icons.check_circle, color: Colors.green)
            : Icon(
          Icons.circle_outlined,
          color: Colors.grey.shade300,
        ),
        selector: (p0, p1) => p1.session["جنسیت بیمار"],
      ),
      leading: Icon(Icons.image),
      title:  'جنسیت بیمار',
      children: [
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 32, left: 16),
                child: Selector<BuyVModel, String?>(
                    selector: (_, changeNotifier) =>
                    changeNotifier.session["جنسیت بیمار"],
                    builder: (context, value, child) {
                      return _CustomerGenderWidget(
                          value: value,
                          title: 'آقا',
                          icon: FontAwesomeIcons.mars,
                          set: set);
                    }),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 16, right: 16),
                child: Selector<BuyVModel, String?>(
                    selector: (_, changeNotifier) =>
                    changeNotifier.session["جنسیت بیمار"],
                    builder: (context, value, child) {
                      return _CustomerGenderWidget(
                          value: value,
                          title: 'خانم',
                          icon: FontAwesomeIcons.venus,
                          set: set);
                    }),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class _CustomerGenderWidget extends StatelessWidget {
  _CustomerGenderWidget(
      {required this.value,
      required this.title,
      required this.icon,
      required this.set});

  final String? value;
  final String title;
  final IconData icon;
  final Function set;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      decoration: BoxDecoration(
          border:
              value == title ? selectedGenderBorder : unselectedGenderBorder,
          color: value == title ? Colors.deepPurple : Colors.transparent,
          borderRadius: BorderRadius.circular(15),
          boxShadow: []),
      duration: Duration(milliseconds: 200),
      child: TextButton(
        child: Column(
          children: [
            Icon(
              icon,
              color: value == title ? Colors.white : Colors.grey,
              size: 45,
            ),
            FittedBox(
              child: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: value == title ? Colors.white : Colors.grey,
                    fontSize: 18),
              ),
            ),
          ],
        ),
        onPressed: () {
          if (value == title)
            return null;
          else {
            set(context,title);
          }
        },
      ),
    );
  }
}
