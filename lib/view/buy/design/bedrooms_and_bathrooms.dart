import 'package:flutter/material.dart';
import 'package:poopak_app/helpers/plus_and_minus_button.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import 'package:poopak_app/view/buy/design/list_tile_design.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class KitchensAndBathrooms extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["آشپزخانه"] == null) {
      valuable.session["آشپزخانه"] = 1;
      valuable.session["سرویس بهداشتی"] = 1;
    }
    return BuyListTile(
      trailing: Icon(Icons.check_circle,color: Colors.green,),
      leading: Icon(Icons.image),
      title: "تعداد آشپزخانه و سرویس بهداشتی",
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text("آشپزخانه", style: TextStyle(fontWeight: FontWeight.bold)),
            Text(
              "سرویس بهداشتی",
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Selector2<BuyVModel, BuyVModel, Tuple2<int, int>>(
          selector: (p0, p1, p2) =>
              Tuple2(p1.session["آشپزخانه"], p2.session["سرویس بهداشتی"]),
          builder: (context, value, child) => Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              PlusAndMinusButton(
                  name: "آشپزخانه",
                  index: value.item1,
                  changeIndex: _changeIndex),
              PlusAndMinusButton(
                  name: "سرویس بهداشتی",
                  index: value.item2,
                  changeIndex: _changeIndex)
            ],
          ),
        ),
      ],
    );
  }
}

void _changeIndex(BuildContext context, String key, int index) {
  final buyVModel = Provider.of<BuyVModel>(context, listen: false);
  buyVModel.deleteSession(key);
  buyVModel.addSession(key, index);
}
