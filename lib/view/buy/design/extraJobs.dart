import 'package:flutter/material.dart';
import 'package:poopak_app/helpers/const.dart';
import '../buy_vmodel.dart';
import 'package:provider/provider.dart';

class ExtraWorks extends StatelessWidget {
  ExtraWorks(this.work);

  final Map<String, dynamic> work;

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["کارهای اضافی"] == null) {
      valuable.session["کارهای اضافی"] = [].cast<String>();
    }
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Text(
            'سرویس های اضافی',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
        ),
        Container(
            height: 400,
            child: GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              itemCount: work.length,
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              itemBuilder: (context, index) => Selector<BuyVModel, List<String>>(
                selector: (_, changeNotifier) =>
                    changeNotifier.session["کارهای اضافی"].cast<String>(),
                builder: (context, value, child) {
                  return ExtraWidgetCard(
                      color: Theme.of(context).colorScheme.secondary,
                      set: () {
                        return valuable.addSession("کارهای اضافی",
                          [...value, work["choose"][index][0]]
                        );
                      },
                      delete: () {
                        valuable.deleteSessionExtraWidget("کارهای اضافی", work["choose"][index][0]);
                      },
                      value: value,
                      widget: work["choose"][index]);
                },
              ),
            )),
      ],
    );
  }
}

//FadeInImage.assetNetwork(
//placeholder: 'assets/images/grey.jpg',
//fit: BoxFit.cover,
//placeholderCacheHeight: 230,
//fadeInCurve: Curves.easeOutSine,
//placeholderCacheWidth:
//(MediaQuery.of(context).size.width).toInt(),
//image: works["choose"][index][1].toString(),
//fadeInDuration: Duration(milliseconds: 100),
//),
