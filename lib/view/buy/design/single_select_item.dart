import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:poopak_app/helpers/const.dart';
import '../buy_vmodel.dart';
import 'package:provider/provider.dart';

class SingleSelectItem extends StatelessWidget {
  SingleSelectItem(this.option);

  final Map<String, dynamic> option;

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["تک گزینه ای${option["title"]}"] == null) {
      valuable.validator.add("تک گزینه ای${option["title"]}");    }


    List<dynamic> choose = option['choose'];
    return Consumer<BuyVModel>(
      builder: (_, __, ___) {
        return Card(
          elevation: 0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)),
          child: ListTile(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 14, vertical: 6),
            leading: Image.asset(
              "assets/images/houseCleaning.png",
              height: 42,
            ),
            title: FittedBox(
              alignment: Alignment.centerRight,
              fit: BoxFit.scaleDown,
              child: Text(
                "${option["title"].toString()}",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            trailing: valuable.session["تک گزینه ای${option["title"]}"]!=null
                ? Icon(
              Icons.check_circle,
              color: Colors.green,
            )
                : Icon(
              Icons.circle_outlined,
              color: Colors.grey.shade300,
            ),
            onTap: () {
              showCupertinoModalBottomSheet(
                enableDrag: false,
                expand: false,
                context: context,
                builder: (_) => Container(
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Scaffold(
                      appBar: AppBar(
                        title: FittedBox(
                          child: Text(
                            option["title"].toString(),
                          ),
                        ),
                        elevation: 0,
                        backgroundColor: Colors.transparent,
                        foregroundColor: Colors.black,
                        centerTitle: true,
                      ),
                      bottomNavigationBar: CustomRaisedButton(
                          onPress: () => Navigator.pop(context),
                          color: Theme.of(context).colorScheme.secondary,
                          textColor: Colors.white,
                          text: "تایید"),
                      body: ChangeNotifierProvider.value(
                        value: valuable,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Directionality(
                            textDirection: TextDirection.rtl,
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                              itemBuilder: (context, index) {
                                return Selector<BuyVModel, String?>(
                                  selector: (_, changeNotifier) =>
                                  changeNotifier.session["تک گزینه ای${option["title"]}"],
                                  builder: (context, value, child) {
                                    return _SingleSelectItemListTile(
                                        title: choose[index],
                                        value: value,
                                        set: () {
                                          if (valuable.validator.any((element) =>
                                          element == "تک گزینه ای${option["title"]}"))
                                            valuable.validator
                                                .remove("تک گزینه ای${option["title"]}");
                                           valuable.addSession(
                                               "تک گزینه ای${option["title"]}",  choose[index]);
                                        },
                                        delete: () {
                                          if (!valuable.validator.any((element) =>
                                          element == "تک گزینه ای${option["title"]}"))
                                            valuable.validator.add("تک گزینه ای${option["title"]}");
                                          return valuable.deleteSessionSingleSelectListTile(
                                              "تک گزینه ای${option["title"]}");
                                        });
                                  },
                                );
                              },
                              itemCount: choose.length,
                            ),
                          )

                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}

class _SingleSelectItemListTile extends StatelessWidget {
  _SingleSelectItemListTile(
      {required this.value,
      required this.title,
      required this.delete,
      required this.set});

  final String? value;
  final String title;
  final Function set;
  final Function delete;

  @override
  Widget build(BuildContext context) {
    final String buffer = value ?? "";
    final bool isEqual = buffer == title;
    return AnimatedContainer(
      margin: isEqual
          ? EdgeInsets.only(bottom: 5, top: 5, right: 20, left: 20)
          : EdgeInsets.only(bottom: 6, top: 6, right: 36, left: 36),
      decoration: BoxDecoration(
        border: isEqual ? selectedGenderBorder : unselectedGenderBorder,
        color: isEqual ? Colors.deepPurple : Colors.transparent,
        borderRadius: BorderRadius.circular(8),
      ),
      duration: Duration(milliseconds: 300),
      child: TextButton(
        child: FittedBox(
          child: Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: isEqual ? Colors.white : Colors.grey,
                fontSize: 12),
          ),
        ),
        onPressed: () {
          if (isEqual)
            delete();
          else
            set();
        },
      ),
    );
  }
}
