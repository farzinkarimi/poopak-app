import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:poopak_app/helpers/const.dart';
import '../buy_vmodel.dart';
import 'package:provider/provider.dart';

class MultiSelectItem extends StatelessWidget {
  MultiSelectItem(this.option);

  final Map<String, dynamic> option;

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["چند گزینه ای${option["title"]}"] == null) {
      valuable.validator.add("چند گزینه ای${option["title"]}");
    }

    List<dynamic> choose = option['choose'];
    return Consumer<BuyVModel>(
      builder: (_, __, ___) {
        return Card(
          elevation: 0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 14, vertical: 6),
            leading: Image.asset(
              "assets/images/houseCleaning.png",
              height: 42,
            ),
            title: FittedBox(
              alignment: Alignment.centerRight,
              fit: BoxFit.scaleDown,
              child: Text(
                "${option["title"].toString()}",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            trailing: valuable.session["چند گزینه ای${option["title"]}"]!=null
                ? Icon(
                    Icons.check_circle,
                    color: Colors.green,
                  )
                : Icon(
                    Icons.circle_outlined,
                    color: Colors.grey.shade300,
                  ),
            onTap: () {
              showCupertinoModalBottomSheet(
                enableDrag: false,
                expand: false,
                context: context,
                builder: (_) => Container(
                  height: MediaQuery.of(context).size.height * 0.5,
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Scaffold(
                      appBar: AppBar(
                        title: FittedBox(
                          child: Text(
                            option["title"].toString(),
                          ),
                        ),
                        elevation: 0,
                        backgroundColor: Colors.transparent,
                        foregroundColor: Colors.black,
                        centerTitle: true,
                      ),
                      bottomNavigationBar: CustomRaisedButton(
                          onPress: () => Navigator.pop(context),
                          color: Theme.of(context).colorScheme.secondary,
                          textColor: Colors.white,
                          text: "تایید"),
                      body: ChangeNotifierProvider.value(
                        value: valuable,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Directionality(
                            textDirection: TextDirection.rtl,
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: BouncingScrollPhysics(
                                  parent: AlwaysScrollableScrollPhysics()),
                              itemBuilder: (context, index) {
                                return Selector<BuyVModel, List<String>?>(
                                  selector: (_, changeNotifier) =>
                                      changeNotifier.session[
                                          "چند گزینه ای${option["title"]}"],
                                  builder: (context, value, child) {
                                    return _MultiSelectItemListTile(
                                        title: choose[index],
                                        value: value,
                                        set: () {
                                          if (valuable.validator.any((element) =>
                                              element ==
                                              "چند گزینه ای${option["title"]}"))
                                            valuable.validator.remove(
                                                "چند گزینه ای${option["title"]}");
                                          final List<String> initialValue =
                                              value ?? [];
                                          return valuable.addSession(
                                              "چند گزینه ای${option["title"]}",
                                              [...initialValue, choose[index]]
                                                  .cast<String>());
                                        },
                                        delete: () {
                                          if (!valuable.validator.any((element) =>
                                              element ==
                                              "چند گزینه ای${option["title"]}"))
                                            valuable.validator.add(
                                                "چند گزینه ای${option["title"]}");
                                          return valuable
                                              .deleteSessionMultiSelectListTile(
                                                  "چند گزینه ای${option["title"]}",
                                                  choose[index]);
                                        });
                                  },
                                );
                              },
                              itemCount: choose.length,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}

class _MultiSelectItemListTile extends StatelessWidget {
  _MultiSelectItemListTile(
      {required this.title,
      required this.set,
      required this.delete,
      required this.value});

  final String title;
  final Function set;
  final Function delete;
  final List<String>? value;

  @override
  Widget build(BuildContext context) {
    final List<String> buffer = value ?? [];
    final bool selected = buffer.contains(title);
    return AnimatedContainer(
      margin: selected
          ? EdgeInsets.only(bottom: 5, top: 5, right: 30, left: 30)
          : EdgeInsets.only(bottom: 6, top: 6, right: 48, left: 48),
      decoration: BoxDecoration(
        border: selected ? selectedGenderBorder : unselectedGenderBorder,
        color: selected ? Colors.deepPurple : Colors.transparent,
        borderRadius: BorderRadius.circular(8),
      ),
      duration: Duration(milliseconds: 300),
      child: TextButton(
        child: FittedBox(
          child: Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: selected ? Colors.white : Colors.grey,
                fontSize: 12),
          ),
        ),
        onPressed: () {
          if (selected)
            delete();
          else
            set();
        },
      ),
    );
  }

  Widget nextPrevButton(
      {required Function()? onPress,
      required Color color,
      required Color textColor,
      required String text}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
          width: double.maxFinite,
          decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              border: Border.all(color: textColor)),
          child: TextButton(
            onPressed: onPress,
            child: Text(
              text,
              style: TextStyle(color: textColor),
            ),
          )),
    );
  }
}
