import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:poopak_app/helpers/const.dart';
import '../buy_vmodel.dart';
import 'package:provider/provider.dart';

class TypeOfBuilding extends StatelessWidget {
  void set(BuildContext context, String genderTitle) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    valuable.validator.removeWhere((element) => element=="نوع ساختمان");

    valuable.deleteSession("نوع ساختمان");
    return valuable.addSession("نوع ساختمان", genderTitle);
  }

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["نوع ساختمان"] == null) {
      valuable.validator.add("نوع ساختمان");
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,

      children: [
        Text(
          'نوع محل کدامیک می باشد؟',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 32, left: 16),
                child: Selector<BuyVModel, String?>(
                    selector: (_, changeNotifier) =>
                    changeNotifier.session["نوع ساختمان"],
                    builder: (context, value, child) =>
                        _TypeOfBuildingWidget(
                            value: value,
                            title: 'مسکونی',
                            outlineIcon: EvaIcons.homeOutline,
                            icon: EvaIcons.home,
                            set: set)),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 32, right: 16),
                child: Selector<BuyVModel, String?>(
                    selector: (_, changeNotifier) =>
                    changeNotifier.session["نوع ساختمان"],
                    builder: (context, value, child) =>
                        _TypeOfBuildingWidget(
                            value: value,
                            title: 'اداری',
                            outlineIcon: FontAwesomeIcons.building,
                            icon: FontAwesomeIcons.solidBuilding,
                            set: set)),
              ),
            )
          ],
        ),
      ],
    );
  }
}


class _TypeOfBuildingWidget extends StatelessWidget {
  _TypeOfBuildingWidget({
    required this.value,
    required this.title,
    required this.icon,
    required this.outlineIcon,
    required this.set });


  final String? value;
  final String title;
  final IconData icon;
  final IconData outlineIcon;
  final Function set;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      decoration: BoxDecoration(
        border: value == title
            ? selectedGenderBorder
            : unselectedGenderBorder,
        color: value == title ? Colors.deepPurple : Colors.transparent,
        borderRadius: BorderRadius.circular(15),
      ),
      duration: Duration(milliseconds: 200),
      child: TextButton(
        child: Column(
          children: [
            Icon(
              value == title ? icon : outlineIcon,
              color: value == title ? Colors.white : Colors.grey,
              size: 45,
            ),
            Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: value == title ? Colors.white : Colors.grey,
                  fontSize: 18),
            ),
          ],
        ),
        onPressed: () {
          if (value == title)
            return null;
          else
            set(context,title);
        },
      ),
    );
  }
}
