import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/view/buy/design/list_tile_design.dart';
import '../buy_vmodel.dart';
import 'package:provider/provider.dart';


class IsEmptyRoom extends StatelessWidget {

  void set (BuildContext context,String value) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    valuable.validator.removeWhere((element) => element=="خالی از سکونت");
    valuable.validator.remove("خالی از سکونت");

    return valuable.addSession("خالی از سکونت", value);
  }

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["خالی از سکونت"] == null) {
      valuable.validator.add("خالی از سکونت");
    }

    return BuyListTile(
      trailing: Selector<BuyVModel, String?>(
        builder: (context, value, child) => value != null
            ? Icon(Icons.check_circle, color: Colors.green)
            : Icon(
          Icons.circle_outlined,
          color: Colors.grey.shade300,
        ),
        selector: (p0, p1) => p1.session["خالی از سکونت"],
      ),
      title:"آیا محل شما خالی از سکونت است؟",
        leading: Icon(Icons.image),
        children: [
      Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Selector<BuyVModel, String?>(
                  selector: (_, changeNotifier) =>
                  changeNotifier.session["خالی از سکونت"],
                  builder: (context, value, child) => _IsEmptyRoomWidget(
                      value: value,
                      title: 'بله',
                      outlineIcon: Icons.check_box_outline_blank,
                      icon: Icons.check_box,
                      set: set)),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Selector<BuyVModel, String?>(
                  selector: (_, changeNotifier) =>
                  changeNotifier.session["خالی از سکونت"],
                  builder: (context, value, child) {
                    return _IsEmptyRoomWidget(
                        outlineIcon: Icons.check_box_outline_blank,
                        icon: Icons.check_box,
                        value: value,
                        title: 'خیر',
                        set: set);
                  }),
            ),
          ),
        ],
      ),
    ],
    );
  }
}




class _IsEmptyRoomWidget extends StatelessWidget {
  _IsEmptyRoomWidget({
    required this.value,
    required this.title,
    required this.icon,
    required this.outlineIcon,
    required this.set });


  final String? value;
  final String title;
  final IconData icon;
  final IconData outlineIcon;
  final Function set;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      decoration: BoxDecoration(
        border: value == title
            ? selectedGenderBorder
            : unselectedGenderBorder,
        color: value == title ? Colors.deepPurple : Colors.transparent,
        borderRadius: BorderRadius.circular(15),
      ),
      duration: Duration(milliseconds: 200),
      child: TextButton(
        child: Column(
          children: [
            Icon(
              value == title ? icon : outlineIcon,
              color: value == title ? Colors.white : Colors.grey,
              size: 45,
            ),
            Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: value == title ? Colors.white : Colors.grey,
                  fontSize: 18),
            ),
          ],
        ),
        onPressed: () {
          if (value == title)
            return null;
          else
            set(context , title);
        },
      ),
    );
  }
}
