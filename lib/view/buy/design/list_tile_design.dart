import 'package:flutter/material.dart';

class BuyListTile extends StatelessWidget {
  BuyListTile(
      {required this.title, required this.children, required this.leading, required this.trailing});

  final String title;
  final List<Widget> children;
  final Widget leading;
  final Widget trailing;

  @override
  Widget build(BuildContext context) {

    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      elevation: 0,

      child: ExpansionTile(
        tilePadding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
        childrenPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        title: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        children: children,
        leading: leading,
        trailing:trailing,
      ),
    );
  }
}
