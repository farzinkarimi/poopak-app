import 'package:flutter/material.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/view/buy/design/list_tile_design.dart';
import '../buy_vmodel.dart';
import 'package:provider/provider.dart';

class CountOfWorkers extends StatelessWidget {
  void set(BuildContext context, int index) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    valuable.validator.removeWhere((element) => element == "تعداد خدمات دهنده");

    valuable.deleteSession("تعداد خدمات دهنده");

    return valuable.addSession("تعداد خدمات دهنده", index);
  }

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["تعداد خدمات دهنده"] == null) {
      valuable.validator.add("تعداد خدمات دهنده");
    }
    return BuyListTile(
      trailing: Selector<BuyVModel, int?>(
        builder: (context, value, child) => value != null
            ? Icon(Icons.check_circle, color: Colors.green)
            : Icon(
                Icons.circle_outlined,
                color: Colors.grey.shade300,
              ),
        selector: (p0, p1) => p1.session["تعداد خدمات دهنده"],
      ),
      leading: Icon(Icons.image),
      title: 'چه تعداد خدمات دهنده نیاز دارید؟',
      children: [
        Container(
          height: 64,
          child: ListView(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            children: List.generate(
                3,
                (index) => Selector<BuyVModel, int?>(
                      selector: (_, changeNotifier) =>
                          changeNotifier.session["تعداد خدمات دهنده"],
                      builder: (context, value, child) {
                        return ListOfNumberGenerator(
                          value: value,
                          index: index + 1,
                          set: set,
                        );
                      },
                    )),
          ),
        ),
      ],
    );
  }
}
