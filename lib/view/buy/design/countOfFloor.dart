import 'package:flutter/material.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/view/buy/design/list_tile_design.dart';
import '../buy_vmodel.dart';
import 'package:provider/provider.dart';

class CountOfFloors extends StatelessWidget {


  void set(BuildContext context, int index) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);

    valuable.validator.removeWhere((element) => element=="تعداد طبقات");
    valuable.deleteSession("تعداد طبقات");

    return valuable.addSession("تعداد طبقات", index);
  }

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["تعداد طبقات"] == null) {
      valuable.validator.add("تعداد طبقات");
    }

    return BuyListTile(
      trailing: Selector<BuyVModel, int?>(
        builder: (context, value, child) => value != null
            ? Icon(Icons.check_circle, color: Colors.green)
            : Icon(
          Icons.circle_outlined,
          color: Colors.grey.shade300,
        ),
        selector: (p0, p1) => p1.session["تعداد طبقات"],
      ),
      leading: Icon(Icons.image),
      title: 'ساختمان شما چند طبقه است؟' ,
      children: [
        Container(
          height: 64,
          child: ListView(
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            children: List.generate(
                7,
                (index) => Selector<BuyVModel, int?>(
                      selector: (_, changeNotifier) =>
                          changeNotifier.session["تعداد طبقات"],
                      builder: (context, value, child) {
                        return ListOfNumberGenerator(
                            value: value,
                            index: index + 1,
                            set: set);
                      },
                    )),
          ),
        ),
      ],
    );
  }
}
