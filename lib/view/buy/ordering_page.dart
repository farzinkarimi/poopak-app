import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart';
import 'package:page_transition/page_transition.dart';
import 'package:poopak_app/helpers/CustomShowDialog.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/model/Products.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:poopak_app/view/buy/design/multi_select_item.dart';
import 'package:poopak_app/view/buy/design/single_select_item.dart';
import '../../view_model/city_vmodel.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import '../auth/auth_vmodel.dart';
import 'package:poopak_app/view/buy/LocationAndTime.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:poopak_app/view/buy/design/count_of_workers.dart';
import 'package:poopak_app/view/date_and_time/date/date_vmodel.dart';
import 'package:poopak_app/view/date_and_time/time/time_vmodel.dart';
import 'package:poopak_app/view/map_location/address_vmodel.dart';
import 'package:poopak_app/view/map_location/view/address/address_bottom_sheet_vmodel.dart';
import 'package:provider/provider.dart';

import 'design/customer_gender.dart';
import 'design/worker_gender.dart';
import 'design/IsEmptyWidget.dart';
import 'design/bedrooms_and_bathrooms.dart';
import 'design/countOfFloor.dart';
import 'design/extraJobs.dart';

class OrderingPage extends StatelessWidget {
  OrderingPage();

  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    final buyVModel = Provider.of<BuyVModel>(context, listen: false);
    return Scaffold(
      bottomNavigationBar: _NextButton(),
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      resizeToAvoidBottomInset: false,
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: ListView(

            shrinkWrap: true,
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 18),
            physics: BouncingScrollPhysics(),
            controller: scrollController,
            children: [
                          if (buyVModel.productItem.option
                  .any((element) => element.containsKey("workerGender")))
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: WorkerGender(),
                ),
              if (buyVModel.productItem.option
                  .any((element) => element.containsKey("countOfWorkers")))
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: CountOfWorkers(),
                ),
              if (buyVModel.productItem.option
                  .any((element) => element.containsKey("customerGender")))
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: CustomerGender(),
                ),
              if (buyVModel.productItem.option
                  .any((element) => element.containsKey("isEmptyRoom")))
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: IsEmptyRoom(),
                ),
              if (buyVModel.productItem.option
                  .any((element) => element.containsKey("countOfFloors")))
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: CountOfFloors(),
                ),
              if (buyVModel.productItem.option.any(
                      (element) => element.containsKey("kitchensAndBathrooms")))
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: KitchensAndBathrooms(),
                ),
              if (buyVModel.productItem.option.any((element) =>
                  element.containsKey("extra")))
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: ExtraWorks(buyVModel.productItem.option
                      .firstWhere((element) => element['type'] == 4)),

                ),
              ...buyVModel.productItem.option.where((
                  element) => element["type"] == 2).toList().map((e) =>
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: SingleSelectItem(e),
                  )),
              ...buyVModel.productItem.option.where((
                  element) => element["type"] == 3).toList().map((e) =>
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: MultiSelectItem(e),
                  )),
              SizedBox(
                height: 200,
              )
            ]),
      ),
    );
  }
}

class _NextButton extends StatelessWidget {
  _NextButton();

  final GlobalKey<NavigatorState> materialKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return CustomRaisedButton(
        color: Theme
            .of(context)
            .colorScheme
            .secondary,
        textColor: Colors.white,
        text: "تایید",
        onPress: () async {
          final value = Provider.of<BuyVModel>(context, listen: false);
          var user = Provider.of<UserVModel>(context, listen: false);

          if (value.validator.isNotEmpty) {
            Fluttertoast.showToast(msg: "تمامی بخش ها باید وارد شوند");
            return;
          }
        if (user.user == null) {
          Fluttertoast.showToast(msg: "ابتدا وارد شوید");
          await customAlertDialog(context);

          return;
        }
          AutoRouter.of(context).navigate(LocationRouter());
//          Navigator.of(materialKey.currentContext!).push(PageTransition(
//              settings: RouteSettings(arguments: materialKey),
//              child: Container(
//                child: MultiProvider(providers: [
//
//                ], child: LocationAndTime()),
//              ),
//              type: PageTransitionType.fade));
//
          value.pageCounter++;
        });
  }

}
