import 'package:flutter/material.dart';
import 'package:poopak_app/model/Products.dart';

class BuyVModel extends ChangeNotifier {
  Map<String, dynamic> _session = {};
  List<String> validator = [];
  int _pageCounter = 0;
  String? _caption;
  late ProductItem productItem;



  BuyVModel(ProductItem p){
    productItem=p;
  }

  Map<String, dynamic> get session => _session;

  set session(Map<String, dynamic> s) {
    _session = s;
    notifyListeners();
  }

  void addSession(String key, dynamic value) {
    Map<String, dynamic> buffer = {key: value};
    _session.addEntries(buffer.entries);
    notifyListeners();
  }

  void deleteSession(String key) {
    _session.remove(key);
    notifyListeners();
  }

  void deleteSessionExtraWidget(String key, String value) {
    _session[key].remove(value);
    notifyListeners();
  }

  void deleteSessionSingleSelectListTile(String key) {
    _session[key] = null;
    notifyListeners();
  }

  void deleteSessionMultiSelectListTile(String key, String value) {
    _session[key] = [..._session[key]].cast<String>();
    _session[key].remove(value);
    if (_session[key].length == 0) _session[key] = null;
    notifyListeners();
  }

  int get pageCounter => _pageCounter;

  String? get caption => _caption;

//

  set pageCounter(int index) {
    _pageCounter = index;
    notifyListeners();
  }

  set caption(String? c) {
    _caption = c;
    notifyListeners();
  }

//
}
