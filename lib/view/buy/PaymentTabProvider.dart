
import 'package:flutter/cupertino.dart';

class PaymentTabProvider extends ChangeNotifier {
  String _payMethod="پرداخت اینترنتی";
  String? _giftKey;
  String? _value;


  String get payMethod=>_payMethod;
  String? get giftKey=>_giftKey;
  String? get value=>_value;


  set payMethod(String s){
    _payMethod=s;
    notifyListeners();
  }
  set giftKey(String? s){
    _giftKey=s;
    notifyListeners();
  }
  set value(String? s){
    _value=s;
    notifyListeners();
  }




}