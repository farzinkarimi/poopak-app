import 'dart:ui';
import 'package:auto_route/auto_route.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:poopak_app/model/Products.dart';
import 'package:poopak_app/view/date_and_time/date/date_vmodel.dart';
import 'package:poopak_app/view/map_location/address_vmodel.dart';
import 'buy_page_appbar.dart';
import 'buy_vmodel.dart';
import 'package:provider/provider.dart';

class BuyPage extends StatelessWidget {
  BuyPage(this.product);

  ProductItem product;
  GlobalKey<NavigatorState> orderKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => BuyVModel(product),
        ),
      ],
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            bottom: PreferredSize(
                child: BuyPageAppbar(),
                preferredSize:
                    Size.fromHeight(MediaQuery.of(context).size.height * 0.12)),
            shadowColor: Colors.transparent,
            backgroundColor: Colors.transparent,
            centerTitle: true,
            title: Container(
              alignment: Alignment.center,
              child: Text(
                'ثبت سرویس',
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
            actions: [
              IconButton(
                icon: Icon(
                  Icons.add,
                  color: Colors.transparent,
                ),
                onPressed: null,
              )
            ],
            leading: IconButton(
                icon: Icon(
                  EvaIcons.arrowForward,
                  color: Colors.black,
                ),
                onPressed: () => Navigator.of(context).pop()),
          ),
          body: MultiProvider(providers: [
            ChangeNotifierProvider(create: (_) => DateVModel()),
            ChangeNotifierProvider(
              create: (_) => AddressVModel(),
            )
          ], child: AutoRouter()),
        ),
      ),
    );
  }
}

int findInitial(ProductItem product) {
  var type = product.option.where((element) => element['type'] == 1);

  return type.length;
}
