import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import 'PaymentTabProvider.dart';
import 'package:provider/provider.dart';

class PaymentTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => PaymentTabProvider(),
        )
      ],
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          bottomNavigationBar: _NextButton(),
          backgroundColor: Theme.of(context).backgroundColor,
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                Card(
                  color: Colors.grey[50],
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  margin: const EdgeInsets.all(2),
                  child: Consumer<PaymentTabProvider>(
                    builder: (_, valuable, ___) => Column(
                      children: [
                        Container(
                            padding: EdgeInsets.only(
                              right: 8,
                              top: 16,
                            ),
                            alignment: Alignment.topRight,
                            child: Text("شیوه پرداخت",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ))),
                        _button(
                            color: Theme.of(context).colorScheme.secondary,
                            icon: Icons.payment,
                            value: valuable.payMethod,
                            name: "پرداخت اینترنتی",
                            onTap: () =>
                                valuable.payMethod = "پرداخت اینترنتی"),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8),
                          child: _button(
                              color: Theme.of(context).colorScheme.secondary,
                              icon: FontAwesomeIcons.home,
                              value: valuable.payMethod,
                              name: "پرداخت در منزل",
                              onTap: () =>
                                  valuable.payMethod = "پرداخت در منزل"),
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  color: Colors.grey[50],
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  margin: const EdgeInsets.all(2),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: Stack(
                          children: [
                            Positioned(
                                left: 4,
                                top: 8,
                                child: IconButton(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  onPressed: () {},
                                  icon: Icon(Icons.add_circle),
                                )),
                            TextFormField(
                              style: TextStyle(fontSize: 12),
                              textAlignVertical: TextAlignVertical.center,
                              textDirection: TextDirection.rtl,
                              textAlign: TextAlign.right,
                              decoration: InputDecoration(
                                  hintText: "کد تخفیف",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  labelText: "کد تخفیف"),
                              onSaved: (newValue) {},
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 8),
                        child: Stack(
                          children: [
                            Positioned(
                                left: 4,
                                top: 8,
                                child: IconButton(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  onPressed: () {},
                                  icon: Icon(Icons.add_circle),
                                )),
                            TextFormField(
                              style: TextStyle(fontSize: 12),
                              textAlignVertical: TextAlignVertical.center,
                              textDirection: TextDirection.rtl,
                              textAlign: TextAlign.right,
                              decoration: InputDecoration(
                                  hintText: "کارت هدیه",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(8)),
                                  labelText: "کارت هدیه"),
                              onSaved: (newValue) {},
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Card(
                  color: Colors.grey[50],
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  margin: const EdgeInsets.all(2),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                    child: Column(
                      children: [
                        Align(
                            alignment: Alignment.topRight,
                            child: Text(
                              "جزئیات پرداخت",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            )),
                        Row(
                          children: [
                            Text(
                              "هزینه سرویس",
                              style: TextStyle(color: Colors.grey),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            Text("مجموع تخفیف",
                                style: TextStyle(color: Colors.grey))
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              "جمع خرید سرویس",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          ],
                        ),
                        Divider(),
                        Row(
                          children: [
                            Text("هزینه سرویس به ازای هر ساعت کار اضافه",
                                style: TextStyle(color: Colors.grey))
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 64,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _button(
      {required String value,
      required String name,
      required void Function()? onTap,
      required Color color,
      required IconData icon}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 4),
      child: ListTile(
        tileColor: value == name ? color.withOpacity(0.2) : Colors.transparent,
        trailing: value == name
            ? Icon(
                Icons.check_circle,
                color: color,
              )
            : Icon(Icons.circle_outlined),
        onTap: onTap,
        contentPadding: EdgeInsets.only(bottom: 2, right: 12, left: 12, top: 2),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(
                color: value == name ? color : Colors.blueGrey.shade200)),
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Icon(icon, color: value == name ? color : Colors.black54),
            ),
            SizedBox(
              width: 16,
            ),
            Text(
              name,
              style: TextStyle(color: value == name ? color : Colors.black54),
            )
          ],
        ),
      ),
    );
  }
}

class _NextButton extends StatelessWidget {
  _NextButton();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: CustomRaisedButton(
        onPress: () {
          final value = Provider.of<BuyVModel>(context, listen: false);

          AutoRouter.of(context).navigate(FirstRouter());
//        Navigator.push(
//          materialKey.currentContext!,
//          PageTransition(
//            settings: RouteSettings(arguments: materialKey),
//            type: PageTransitionType.fade,
//            child: MultiProvider(
//                providers: [
//                  ChangeNotifierProvider(
//                      create: (_) => CityVModel()),
//
//                ],
//                child: Container(
//                  color: Colors.blue,
//                )),
//          ),
//        );

          value.pageCounter++;
        },
        color: Theme.of(context).colorScheme.secondary,
        text: "ثبت و ادامه",
        textColor: Colors.white,
      ),
    );
  }
}
