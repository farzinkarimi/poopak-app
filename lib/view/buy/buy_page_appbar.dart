import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import '../../main.dart';
import 'buy_vmodel.dart';
import 'package:provider/provider.dart';

class BuyPageAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FittedBox(

      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 32),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Selector<BuyVModel, int>(
                selector: (_, v) => v.pageCounter,
                builder: (_, value, __) => Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.adjust,
                                    color: value == 0
                                        ? Theme.of(context).colorScheme.secondary
                                        : Colors.grey,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                              child: DottedLine(
                            dashGapLength: 5,
                            dashLength: 5,
                          )),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.adjust,
                                    color: value == 1
                                        ? Theme.of(context).colorScheme.secondary
                                        : Colors.grey,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                              child: DottedLine(
                            dashGapLength: 5,
                            dashLength: 5,
                          )),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.adjust,
                                    color: value == 2
                                        ? Theme.of(context).colorScheme.secondary
                                        : Colors.grey,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                              child: DottedLine(
                            dashGapLength: 5,
                            dashLength: 5,
                          )),
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Icon(
                                    Icons.adjust,
                                    color: value == 3
                                        ? Theme.of(context).colorScheme.secondary
                                        : Colors.grey,
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FittedBox(
                              child: Text(
                            "جزئیات سرویس",
                            style: TextStyle(
                                fontSize: 10,

                                color: value == 0
                                    ? Theme.of(context).colorScheme.secondary
                                    : Colors.grey),
                          )),
                          FittedBox(
                            child: Text(
                              "ثبت آدرس",
                              style: TextStyle(
                                fontSize: 10,
                                  color: value == 1
                                      ? Theme.of(context).colorScheme.secondary
                                      : Colors.grey),
                            ),
                          ),
                          FittedBox(
                              child: Text(
                            "پرداخت سفارش",
                            style: TextStyle(
                                fontSize: 10,

                                color: value == 2
                                    ? Theme.of(context).colorScheme.secondary
                                    : Colors.grey),
                          )),
                          FittedBox(
                              child: Text(
                            " بخش نهایی   ",
                            style: TextStyle(
                                fontSize: 10,

                                color: value == 3
                                    ? Theme.of(context).colorScheme.secondary
                                    : Colors.grey),
                          ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Divider(
            height: 0,
            color: Theme.of(context).colorScheme.secondary,
          ),
        ],
      ),
    );
  }
}
