import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:poopak_app/view/buy/PaymentTab.dart';
import 'package:poopak_app/view/buy/PaymentTabProvider.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import 'package:poopak_app/view/date_and_time/time/time_vmodel.dart';
import 'package:poopak_app/view/map_location/view/address/address_bottom_sheet_vmodel.dart';
import 'package:poopak_app/view/map_location/address_list_tile.dart';
import 'package:poopak_app/view/map_location/address_vmodel.dart';
import 'package:provider/provider.dart';
import '../date_and_time/DateAndTimeWidget.dart';

class LocationAndTime extends StatefulWidget {
  LocationAndTime();

  @override
  State<LocationAndTime> createState() => _LocationAndTimeState();
}

class _LocationAndTimeState extends State<LocationAndTime> {
  late Future<bool> future;

  @override
  void didChangeDependencies() {
    var userAddress = Provider.of<AddressVModel>(context, listen: false);
    future = userAddress.getUserAddress();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async{
        Provider.of<BuyVModel>(context,listen: false).validator.clear();
        return true;
      },
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider.value(
            value: Provider.of<AddressVModel>(context, listen: false),
          ),
          ChangeNotifierProvider(create: (_) => TimeVModel()),
          ChangeNotifierProvider(
            create: (_) => AddressBottomSheetVModel(),
          ),
        ],
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Scaffold(
            bottomNavigationBar: _NextButton(),
            backgroundColor: Theme.of(context).backgroundColor,
            resizeToAvoidBottomInset: false,
            body: SingleChildScrollView(
              child: FutureBuilder<bool>(
                future: future,
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SpinKitWave(
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Text(
                            "در حال دریافت آدرس ها",
                            style: TextStyle(
                                color: Theme.of(context)
                                    .colorScheme
                                    .secondary
                                    .withOpacity(0.5)),
                          )
                        ],
                      );
                    default:
                      if (snapshot.data != null) {
                        if (snapshot.data!) {
                          return Column(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              Divider(
                                color: Colors.grey,
                              ),
                              AddressListTile(),
                              Divider(
                                color: Colors.grey,
                              ),
                              DateAndTimeWidget(),
                            ],
                          );
                        } else {
                          return const Text("خطا در دریافت آدرس");
                        }
                      } else {
                        return const Text("خطا در دریافت آدرس");
                      }
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

//class _NextButton extends StatelessWidget {
//  _NextButton(this.materialKey);
//  final GlobalKey<NavigatorState> materialKey;
//  @override
//  Widget build(BuildContext context) {
//    return CustomRaisedButton(onPress: () => , color: Theme.of(context).primaryColor, textColor: Colors.white, text: "ثبت و ادامه");
//  }
//}
class _NextButton extends StatelessWidget {
  _NextButton();


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: CustomRaisedButton(
          text: "ثبت و ادامه",
          color: Theme.of(context).colorScheme.secondary,
          textColor: Colors.white,
          onPress: () {
            final value = Provider.of<BuyVModel>(context, listen: false);

            if (value.validator.isNotEmpty) {
              Fluttertoast.showToast(msg: "تمامی بخش ها باید وارد شوند");
              return;
            }

            AutoRouter.of(context).navigate(PaymentRouter());

            value.pageCounter++;
          }),
    );
  }
}
