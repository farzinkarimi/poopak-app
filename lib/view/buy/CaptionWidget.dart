import 'package:flutter/material.dart';
import 'buy_vmodel.dart';
import 'package:provider/provider.dart';

class CaptionWidget extends StatelessWidget {
  TextEditingController editingController=TextEditingController();
  @override
  Widget build(BuildContext context) {
    var valuable=Provider.of<BuyVModel>(context,listen: false);
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Container(
        height: 200,
        padding: EdgeInsets.symmetric(horizontal: 10,vertical: 30),
        child: TextField(
          controller: editingController,
          onChanged: (value) {
            valuable.caption=value;
          },

          maxLines: 5,
          decoration: InputDecoration(
            labelText: 'توضیحات',
            contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        ),
      ),
    );
  }
}
