import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:poopak_app/model/Products.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:poopak_app/view/map_location/address_vmodel.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import '../../main.dart';
import 'package:poopak_app/view/buy/BuySetTime.dart';
import 'buy_vmodel.dart';
import 'package:provider/provider.dart';

class ProductPeek extends StatelessWidget {
  ProductPeek(this.product);

  final ProductItem product;

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<UserVModel>(context, listen: false);
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
            CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  expandedHeight: 200,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                    background: Container(
                      color: Theme.of(context).colorScheme.secondary,
                      child: Hero(
                        tag: product.title,
                        child: CachedNetworkImage(
                          placeholder: (context, url) => Container(
                            color: Colors.grey.shade300,
                          ),
                          fit: BoxFit.cover,
                          errorWidget: (_, __, ___) => Center(
                            child: Icon(FontAwesomeIcons.images),
                          ),
                          fadeInCurve: Curves.easeOutSine,
                          imageUrl: product.image,
                          fadeInDuration: Duration(milliseconds: 100),
                        ),
                      ),
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                          child: Text(
                        product.title,
                        style: TextStyle(fontSize: 25, fontFamily: 'HeadLine'),
                        textAlign: TextAlign.center,
                      )),
                      SizedBox(
                        height: 20,
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Divider(
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          children: <Widget>[
                            Text(
                              product.fullDetails,
                              style: TextStyle(fontSize: 19),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            Container(
                alignment: Alignment.bottomRight,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.secondary,
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () {
                                AutoRouter.of(context).push(BuyRouter(product: product));
//                                Navigator.of(context).push(MaterialPageRoute(
//                                  builder: (context) =>
//                                      MultiProvider(providers: [
//                                    ChangeNotifierProvider(
//                                      create: (context) => BuyVModel(),
//                                    ),
//                                    ChangeNotifierProvider.value(
//                                      value: user,
//                                    ),
//                                    ChangeNotifierProvider(
//                                      create: (_) => AddressVModel(),
//                                    ),
//                                  ], child: BuyPage(product)),
//                                ));
                              },
                              child: Text(
                                'ثبت و ادامه',
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                'انصراف',
                                style: TextStyle(color: Colors.black),
                              ),
                            )),
                      ),
                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
