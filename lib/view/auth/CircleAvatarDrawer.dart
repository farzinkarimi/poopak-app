import 'package:flutter/material.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import '../../helpers/CustomShowDialog.dart';
import 'package:provider/provider.dart';

class AuthedDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = Provider.of<UserVModel>(context, listen: false);
    return Container(
      width: double.maxFinite,
      height: 80,
      child: ListTile(
        onTap: () {},
        subtitle: Text(user.user!.mobile),
        title: Text(user.user!.name),
        leading: CircleAvatar(
          radius: 20,
          backgroundColor: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }
}

class AuthDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 80,
      child: ListTile(
        onTap: () async {
          customAlertDialog(context);
        },
        subtitle: Text('لطفا وارد شوید'),
        title: Text('خوش امدید!'),
        leading: CircleAvatar(
          radius: 20,
          backgroundColor: Theme.of(context).colorScheme.secondary,
        ),
      ),
    );
  }
}
