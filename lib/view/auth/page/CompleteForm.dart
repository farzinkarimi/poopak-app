import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/helpers/format_date.dart';
import 'package:poopak_app/model/user.dart';
import 'package:poopak_app/view/auth/auth_vmodel.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import 'package:provider/provider.dart';

class CompleteFormVModel extends ChangeNotifier {
  String _date = 'تاریخ تولد';
  String _selectedGender = "man";

  String get date => _date;

  String get selectedGender => _selectedGender;

  void changeDate(String newDate) {
    _date = newDate;
    notifyListeners();
  }

  void changeGender(String newGender) {
    _selectedGender = newGender;
    notifyListeners();
  }
}

class CompleteForm extends StatefulWidget {
  CompleteForm(this.mobileNumber, this.ctx);

  final String mobileNumber;
  final BuildContext ctx;

  @override
  _CompleteFormState createState() => _CompleteFormState();
}

class _CompleteFormState extends State<CompleteForm> {
  FocusNode focusNodeEmail = FocusNode();
  FocusNode focusNodeName = FocusNode();
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _submitForm() async {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    _formKey.currentState!.save();
    String cache = "";
    var valuable = Provider.of<CompleteFormVModel>(context, listen: false);
    var authVModel = Provider.of<AuthVModel>(context, listen: false);

    cache = await authVModel.requestSignUp({
      "email": emailController.text,
      "name": nameController.text,
      "gender": valuable.selectedGender,
      "birthDay": valuable._date == 'تاریخ تولد' ? null : valuable._date,
      "mobile": widget.mobileNumber
    });
    if (cache == "موفق در بارگزاری اطلاعات") {
      Navigator.of(widget.ctx).pop();
      Provider.of<UserVModel>(context, listen: false).user =
          authVModel.tempUser;
    } else {
      Fluttertoast.showToast(msg: cache);
    }
  }

  @override
  void dispose() {
    focusNodeEmail.dispose();
    focusNodeName.dispose();
    emailController.dispose();
    nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<CompleteFormVModel>(context, listen: false);
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Form(
          key: _formKey,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 16),
            child: ListView(
              children: [
                Container(
                  child: ListTile(
                    leading: Icon(Icons.person),
                    title: TextFormField(
                      autovalidateMode: AutovalidateMode.always,
                      textInputAction: TextInputAction.next,
                      textAlignVertical: TextAlignVertical.top,
                      onFieldSubmitted: (term) {
                        _fieldFocusChange(
                            context, focusNodeName, focusNodeEmail);
                      },
                      decoration: InputDecoration(
                          constraints: BoxConstraints(maxHeight: 80),
                          border: OutlineInputBorder(),
                          labelText: 'نام و نام خانوادگی'),
                      focusNode: focusNodeName,
                      validator: (value) {
                        if (value!.length < 5) return 'وارد کردن نام اجباریست';
                      },
                      controller: nameController,
                    ),
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  height: 48,
                  child: ListTile(
                    leading: Container(
                        child: Icon(
                      Icons.email,
                    )),
                    title: TextFormField(
                      textAlign: TextAlign.left,
                      textAlignVertical: TextAlignVertical.top,
                      focusNode: focusNodeEmail,
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      validator: (String? value) {
                        if (value != null) if (RegExp(
                                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                            .hasMatch(value)) {
                          return 'ایمیل نامعتبر';
                        }
                      },
                      decoration: InputDecoration(
                          constraints: BoxConstraints(maxHeight: 80),
                          border: OutlineInputBorder(),
                          labelText: 'ایمیل'),
                    ),
                  ),
                ),
                SizedBox(
                  height: 12,
                ),
                Container(
                  height: 48,
                  child: ListTile(
                    leading: Icon(EvaIcons.calendar),
                    title: GestureDetector(
                      onTap: () async {
                        final Jalali? date = await showPersianDatePicker(
                            context: context,
                            initialDate: Jalali.now(),
                            firstDate: Jalali.now().addYears(-100),
                            lastDate: Jalali.now());
                        if (date != null) valuable.changeDate(format(date));
                      },
                      child: Container(
                        height: double.maxFinite,
                        alignment: Alignment.centerRight,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(5)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Consumer<CompleteFormVModel>(
                            builder: (_, value, __) => Text(
                              valuable._date.toString(),
                              style: TextStyle(
                                  color: value._date == 'تاریخ تولد'
                                      ? Colors.grey[700]
                                      : Colors.black),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                Container(
                  width: double.maxFinite,
                  child: Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Selector<CompleteFormVModel, String>(
                            selector: (_, p1) => p1.selectedGender,
                            builder: (_, value, __) {
                              bool b = value == "man";
                              return AnimatedContainer(
                                decoration: BoxDecoration(
                                    border: b
                                        ? selectedGenderBorder
                                        : unselectedGenderBorder,
                                    color: Theme.of(context).colorScheme.surface,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: []),
                                duration: Duration(milliseconds: 200),
                                child: TextButton(
                                  child: Text(
                                    'مرد',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: b
                                            ? Theme.of(context).colorScheme.secondary
                                            : Colors.grey,
                                        fontSize: 16),
                                  ),
                                  onPressed: () {
                                    if (b)
                                      return null;
                                    else {
                                      valuable.changeGender("man");
                                    }
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Selector<CompleteFormVModel, String>(
                            selector: (_, p1) => p1.selectedGender,
                            builder: (_, value, __) {
                              bool b = value == "woman";

                              return AnimatedContainer(
                                decoration: BoxDecoration(
                                    color: Theme.of(context).colorScheme.surface,
                                    border: b
                                        ? selectedGenderBorder
                                        : unselectedGenderBorder,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: []),
                                duration: Duration(milliseconds: 200),
                                child: TextButton(
                                  child: Text('زن',
                                      style: TextStyle(
                                          color: b
                                              ? Theme.of(context).colorScheme.secondary
                                              : Colors.grey,
                                          fontSize: 16)),
                                  onPressed: () {
                                    if (b)
                                      return null;
                                    else {
                                      valuable.changeGender("woman");
                                    }
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),

                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.surface,
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: () => Navigator.pop(widget.ctx),
                              child: Text(
                                'انصراف',
                                style: TextStyle(color: Colors.black),
                              ),
                            )),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            decoration: BoxDecoration(
                                color: Theme.of(context).colorScheme.secondary,
                                borderRadius: BorderRadius.circular(10)),
                            child: TextButton(
                              onPressed: _submitForm,
                              child: Text(
                                'ثبت نام',
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                      ),
                    ),
                  ],
                ) // birthday
              ],
            ),
          ),
        ),
      ),
    );
  }
}

_fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}
