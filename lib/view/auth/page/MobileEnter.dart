import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:persian_number_utility/persian_number_utility.dart';
import 'package:poopak_app/helpers/EnsureVisible.dart';
import 'SignInOrSignUp.dart';
import '../auth_vmodel.dart';
import 'package:provider/provider.dart';

class MobileEnter extends StatefulWidget {
  MobileEnter(this.keyOne, this.editingController, this.ctx);

  final TextEditingController editingController;
  final GlobalKey<NavigatorState> keyOne;
  final BuildContext ctx;

  @override
  _MobileEnterState createState() => _MobileEnterState();
}

class _MobileEnterState extends State<MobileEnter> {
  final _mobileFocusNode = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> _submitForm() async {
    if (!_formKey.currentState!.validate()) {
      return ;
    }
    _formKey.currentState!.save();
    var authVModel = Provider.of<AuthVModel>(context, listen: false);
    final bool enterMobileSuccess= await authVModel.enterMobile();
    if (enterMobileSuccess) {
      FocusScope.of(context).unfocus();
      widget.keyOne.currentState!.push(
        PageTransition(
            child: SignInOrSignUp(widget.keyOne, widget.ctx),
            type: PageTransitionType.leftToRight),
      );
    } else {
      return ;
    }
  }

  Widget _buildMobileTextField() {
    var authVModel = Provider.of<AuthVModel>(context, listen: false);

    return EnsureVisibleWhenFocused(
      focusNode: _mobileFocusNode,
      child: TextFormField(
        style: TextStyle(fontSize: 12),
        textAlignVertical: TextAlignVertical.center,
        textDirection: TextDirection.ltr,
        inputFormatters: [
          MaskTextInputFormatter(
              mask: '###########', filter: {"#": RegExp(r'[0-9]|[۰-۹]')})
        ],
        controller: widget.editingController,
        textAlign: TextAlign.right,
        decoration: InputDecoration(
            helperText: ' ',
            hintText: '۰۰۰۰ ۳۴۵ ۰۹۱۲',
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            prefixIcon: Icon(Icons.phone_android),
            labelText: 'شماره موبایل'),
        validator: (value) {
          String buffer;
          if (value != null) if (value.isEmpty)
            return 'لطفا یک شماره موبایل وارد نمایید';
          if (widget.editingController.text[0] == '0'||widget.editingController.text[0]=="۰") {
            buffer = widget.editingController.text;

          } else {
            buffer = '0' + widget.editingController.text;

          }
          print(buffer
              .extractNumber()
              .toEnglishDigit()
              );

          if (!buffer
              .extractNumber()
              .toEnglishDigit()
              .isValidIranianMobileNumber())
            return 'لطفا یک شماره موبایل صحیح وارد کنید';
        },
        onSaved: (newValue) {
          authVModel.tempMobile = newValue!.toEnglishDigit();
        },
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    final authVModel=Provider.of<AuthVModel>(context,listen: false);
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            height: MediaQuery.of(context).size.width*0.9,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 25),
                      alignment: Alignment.topCenter,
                      child: Image.asset(
                        'assets/images/AuthPageVector.png',
                        width: MediaQuery.of(context).size.width * 0.23,
                      ),
                    ),
                    Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.only(top: 20),
                      child: Image.asset(
                        'assets/images/AuthPageHand.png',
                        width: MediaQuery.of(context).size.width * 0.19,
                      ),
                    ),
                    Positioned(
                      right: 5,
                      top: 5,
                      child: IconButton(
                        icon: Icon(EvaIcons.closeCircleOutline),
                        onPressed: () {
                          Navigator.pop(widget.ctx);

                        },

                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Text(
                    'لطفا برای ورود یا ثبت نام شماره تلفن خود را وارد نمایید',
                    textAlign: TextAlign.center,
                  ),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 2),
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Stack(
                      children: [
                        Container(child: _buildMobileTextField()),
                        Positioned(
                          left: 8,
                          top: 10,
                          child: Container(
                            height: 48,
                            width: MediaQuery.of(context).size.width * 0.22,
                            child: Selector<AuthVModel,bool>(
                              selector: (_, p1) => p1.showSpinner,
                              builder: (_, notifier, __) => RaisedButton(
                                onPressed: () async {
                                  authVModel.spinChange();
                                   await _submitForm();
                                  authVModel.spinChange();
                                },
                                child: !notifier
                                    ? FittedBox(
                                      child: Text(
                                          'دریافت کد',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                          ),
                                        ),
                                    )
                                    : SpinKitPulse(color: Colors.white),
                                color: Theme.of(context).colorScheme.secondary,
                                shape: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(width: 0)),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 1,
                          child: Selector<AuthVModel,String>(
                            selector: (_, p1) => p1.errorMassage,
                              builder: (_, value, __) => AnimatedSwitcher(
                                  duration: Duration(milliseconds: 500),
                                  child: value !=""
                                      ? Padding(
                                          padding:
                                              const EdgeInsets.only(right: 8.0),
                                          child: Text(
                                            value,
                                            style: TextStyle(
                                                color: Colors.red,
                                                fontSize: 12),
                                          ),
                                        )
                                      : Container())),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
