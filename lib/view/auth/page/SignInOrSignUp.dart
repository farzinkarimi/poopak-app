import 'dart:async';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import '../pinField.dart';
import '../../../main.dart';
import 'CompleteForm.dart';
import '../auth_vmodel.dart';

import 'package:provider/provider.dart';

class SignInOrSignUp extends StatefulWidget {
  SignInOrSignUp(this.keyOne, this.ctx);

  final GlobalKey<NavigatorState> keyOne;
  final BuildContext ctx;

  @override
  _SignInOrSignUpState createState() => _SignInOrSignUpState();
}

class _SignInOrSignUpState extends State<SignInOrSignUp> {
  final formKey = GlobalKey<FormState>();
  final TextEditingController textEditingController = TextEditingController();

  Future<void> _submitForm() async {
    var authVModel = Provider.of<AuthVModel>(context, listen: false);
    if (!formKey.currentState!.validate()) {
      return;
    }
    formKey.currentState!.save();
    if (authVModel.loginType == LoginType.SignIn) {
      if (authVModel.pin.toString() == textEditingController.text) {
        Navigator.of(widget.ctx).pop();
        final userVModel = Provider.of<UserVModel>(context, listen: false);
        userVModel.user = authVModel.tempUser;
      } else {
        authVModel.pinError = true;
        return;
      }
    } else {
      if (authVModel.pin.toString() == textEditingController.text) {
        Navigator.of(context).push(PageTransition(
            child: ChangeNotifierProvider(
                create: (_) => CompleteFormVModel(),
                builder: (__, _) =>
                    CompleteForm(authVModel.tempMobile, widget.ctx)),
            type: PageTransitionType.leftToRight));
      } else {
        authVModel.pinError = true;
        return;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.8,
          height: MediaQuery.of(context).size.width * 0.9,
          decoration: BoxDecoration(
//        boxShadow: [BoxShadow(blurRadius: 3, spreadRadius: 1)],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      icon: Icon(
                        Icons.check,
                        color: Theme.of(context).colorScheme.surface,
                      ),
                      onPressed: null),
                  Consumer<AuthVModel>(
                    builder: (_, notifier, __) => Text(
                      notifier.loginType == LoginType.SignIn
                          ? 'ورود کاربر'
                          : 'ثبت نام کاربر',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 5, top: 5),
                    child: IconButton(
                        icon: Icon(EvaIcons.arrowForward),
                        onPressed: () {
                          Navigator.of(context).pop();
                        }),
                  )
                ],
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 14),
                child: Text(
                  'یک پیامک حاوی یک کد یک کد چهار رقمی به شماره موبایل شما ارسال شد',
                  style: TextStyle(fontSize: 12),
                  textAlign: TextAlign.right,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 48, vertical: 6),
                child: Consumer<AuthVModel>(
                  builder: (_, __, ___) => Form(
                      key: formKey,
                      child: PinTextWidget(
                          textEditingController)),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                      onTap: () {},
                      child: Text(
                        'تلاش دوباره',
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Theme.of(context).colorScheme.secondary),
                      )),
                  Text('پیامکی دریافت نکردید؟'),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 16),
                height: 46,
                width: double.maxFinite,
                child: RaisedButton(
                  onPressed: () async {
                    await _submitForm();
                  },
                  child: Consumer<AuthVModel>(
                    builder: (_, notifier, __) => Text(
                      notifier.loginType == LoginType.SignIn
                          ? 'ورود'
                          : 'تکمیل اطلاعات',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Theme.of(context).colorScheme.surface,
                      ),
                    ),
                  ),
                  color: Theme.of(context).colorScheme.secondary,
                  shape: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(width: 0)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
