import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'auth_vmodel.dart';
import 'package:provider/provider.dart';

class PinTextWidget extends StatelessWidget {
  PinTextWidget(this.textEditingController);
  final TextEditingController textEditingController;
  @override
  Widget build(BuildContext context) {
    var value=Provider.of<AuthVModel>(context,listen:false);

    return PinCodeTextField(
      autoDismissKeyboard: true,
      appContext: context,
//              pastedTextStyle: TextStyle(
//                color: Colors.green.shade600,
//                fontWeight: FontWeight.bold,
//              ),
      errorTextSpace: 30,

      length: 4,
      obscureText: false,
      animationType: AnimationType.fade,
      validator: (v) {
        if (v!.length != 4||value.pinError)
          return 'رمز غلط است';
      },
      pinTheme: PinTheme(
          selectedFillColor: Colors.transparent,
          inactiveFillColor: Colors.transparent,
          shape: PinCodeFieldShape.box,
          borderRadius: BorderRadius.circular(15),
          selectedColor: Colors.deepPurple[700],
          fieldHeight: 50,
          fieldWidth: 42,
          activeFillColor: Colors.white,
          inactiveColor: Colors.blueGrey,
          activeColor:
          value.pinError?Colors.red: Colors.teal[700]
      ),

      cursorColor: Colors.black,
      animationDuration: Duration(milliseconds: 300),
      textStyle: TextStyle(fontSize: 16, height: 1.6),
      enableActiveFill: true,
      controller: textEditingController,
      inputFormatters: [
        MaskTextInputFormatter(
            mask: '####', filter: {"#": RegExp(r'[0-9]|[۰-۹]')})
      ],
      keyboardType: TextInputType.number,

      onCompleted: (v) {
        value.pinError=false;
        if (value.pin.toString() == v) print("Completed");
      },
      // onTap: () {
      //   print("Pressed");
      // },
     onChanged: (value) {

     },

      beforeTextPaste: (text) {
        print("Allowing to paste $text");
        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
        //but you can show anything you want here, like your pop up saying wrong paste format or etc
        return true;
      },
    );
  }
}
