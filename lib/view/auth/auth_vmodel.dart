import 'dart:math';
import 'package:flutter/material.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/model/user.dart';
import 'package:poopak_app/services/auth_service/API/get_auth_apis.dart';
import 'package:poopak_app/services/auth_service/API/get_auth_locally.dart';
import 'package:poopak_app/services/auth_service/auth_service.dart';

class AuthVModel extends ChangeNotifier {
  User? tempUser;
  LoginType _loginType = LoginType.SignIn;
  int? _pin;
  bool _pinError = false;
  late AuthService _authService;
  bool showSpinner = false;
  String errorMassage = '';
  String tempMobile = "";

  void errorMassageShow(String error) {
    errorMassage = error;
    notifyListeners();
  }

  void spinChange() {
    showSpinner = (!showSpinner);
    notifyListeners();
  }

  LoginType get loginType => _loginType;

  bool get pinError => _pinError;

  int? get pin => _pin;

  AuthVModel() {
    _authService = AuthService(GetAddressAPIs(), GetAuthLocally());
  }

  set pinError(bool b) {
    _pinError = b;
    notifyListeners();
  }

  void changePinError() {
    _pinError = !_pinError;
    notifyListeners();
  }

  set pin(int? p) {
    _pin = p;
    notifyListeners();
  }

  void changeLoginType(LoginType loginType) {
    _loginType = loginType;
    notifyListeners();
  }

  Future<bool> enterMobile() async {
    int randomNumber = Random().nextInt(8999) + 1000;
    bool sendPinRequest = false;
    try {
      sendPinRequest =
          await _authService.addressAPI.sendPin(tempMobile, randomNumber);
    } catch (e) {
      print(e);

      errorMassageShow("خطا در ارسال پیام");
      return false;
    }
    if (sendPinRequest) {
      return await checkEnteredMobileRequest(randomNumber);
    } else {
      errorMassageShow("خطا در اتصال");
      return false;
    }
  }

  Future<bool> checkEnteredMobileRequest(int randomNumber) async {
    Map<String, dynamic>? response;

    try {
      response = await _authService.addressAPI.checkEnteredMobile(tempMobile);
    } catch (e) {
      print(e);

      errorMassageShow("خطا در اتصال به سرور");

      return false;
    }
    if (response != null) {
      if (response["mobileExists"] == true) {
        tempUser = User.fromJson(response);
        _loginType = LoginType.SignIn;
        _pin = randomNumber;
      } else {
        _loginType = LoginType.SignUp;
        _pin = randomNumber;
      }
      return true;
    } else {
      errorMassageShow("خطا در دریافت اطلاعات");
      return false;
    }
  }

  Future<String> requestSignUp(Map<String,dynamic> user) async {
    String? response = await _authService.addressAPI.requestSignUp(user);
    if (response!=null) {
      user["idToken"]=response;
      tempUser=User.fromJson(user);
      print(tempUser.toString());
      return "موفق در بارگزاری اطلاعات";
    } else {
      return "خطا در ارتباط با سرور";
    }
  }

//  @override
//  Future<User?> requestSignIn(String? mobile) async {
//    http.Response response;
//
////  todo camyar
//    try {
//      response = await http.get(
//          Uri.parse(
//              "https://api.json-generator.com/templates/Cp9tr29p2gKl/data"),
//          headers: {
//            "Authorization": "Bearer 71f3ii0o09eshcs49425vr8jpiz4wdlupqj9yls8"
//          }).timeout(Duration(seconds: 10));
//    } catch (e) {
//      print("$e from requestSignIn");
//      return null;
//    }
//    final Map<String, dynamic> returnedMassageSignIn =
//    jsonDecode(response.body);
//    if (response.statusCode == 200 || response.statusCode == 201) {
//      this.user = User(
//          mobile: returnedMassageSignIn['mobile'].toString(),
//          gender: returnedMassageSignIn['gender'] == 'man'
//              ? Gender.man
//              : Gender.woman,
//          email: returnedMassageSignIn['email'],
//          birthDay: returnedMassageSignIn['birthday'],
//          name: returnedMassageSignIn['name']);
//      saveInStorage(key: "token", value: returnedMassageSignIn['idToken']);
//      saveInStorage(
//          key: "mobile", value: returnedMassageSignIn['mobile'].toString());
//      this.changeAuth(true);
//      return "success";
//    } else {
//      return "خطا در ارسال اطلاعات";
//    }
//  }
//  saveInStorage(key: "token", value: returnedMassageSignIn['idToken']);
//  saveInStorage(
//  key: "mobile", value: returnedMassageSignIn['mobile'].toString());

}
