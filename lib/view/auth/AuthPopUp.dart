import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../main.dart';
import 'page/MobileEnter.dart';
import 'auth_vmodel.dart';
import 'package:provider/provider.dart';

class AuthBuild extends StatefulWidget {
  AuthBuild();

  @override
  _AuthBuildState createState() => _AuthBuildState();
}

class _AuthBuildState extends State<AuthBuild> {
  TextEditingController editingController = TextEditingController();
  var keyOne = GlobalKey<NavigatorState>();

  @override
  void dispose() {
    editingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create:(context) => AuthVModel()),
      ],
      child: AlertDialog(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
        content: Container(
          width: MediaQuery.of(context).size.width * 0.8,
          height: MediaQuery.of(context).size.width * 0.9,
          child: Scaffold(

            resizeToAvoidBottomInset: false,
            body: Navigator(

              key: keyOne,
              onGenerateRoute: (routeSettings) {
                return MaterialPageRoute(

                  builder: (_) =>
                      MobileEnter(keyOne, editingController,context),
                );
              },
            ),
          ),
        ),
        contentPadding: EdgeInsets.all(1),
        elevation: 5,
      ),
    );
  }
}