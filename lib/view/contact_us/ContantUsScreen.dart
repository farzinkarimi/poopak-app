import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:poopak_app/view/contact_us/invite_vmodel.dart';
import 'package:provider/provider.dart';

class ContantUs extends StatefulWidget {
  @override
  State<ContantUs> createState() => _ContantUsState();
}

class _ContantUsState extends State<ContantUs>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        vsync: this,
        duration: Duration(
          milliseconds: 200,
        ));



    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: ChangeNotifierProvider(
        create: (context) => InviteVModel(),
        builder: (context, child) => Scaffold(
          backgroundColor: Theme.of(context).colorScheme.primary,
          body: Container(
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Image.asset("assets/images/invite.png"),
                Consumer<InviteVModel>(
                  builder: (context, value, child) => Positioned(
                    height: value.offset + 350,
                    left: 0,
                    bottom: 0,
                    child: GestureDetector(
                      onVerticalDragEnd: (details) {
                        if (details.primaryVelocity! > -1000) {
                          final x = Tween<double>(begin: value.offset, end: 0);

                          animation = x.animate(CurvedAnimation(
                              parent: animationController,
                              curve: Curves.easeOutQuart))
                            ..addListener(() {
                              value.offset = animation.value;
                            });

                          animationController
                              .forward()
                              .whenCompleteOrCancel(() {
                            animationController.clearListeners();
                            animationController.reset();
                          });
                        } else {
                          final x = Tween<double>(
                              begin: value.offset,
                              end: MediaQuery.of(context).size.height * 0.2);

                          animation = x.animate(CurvedAnimation(
                              parent: animationController,
                              curve: Curves.easeOutQuart))
                            ..addListener(() {
                              value.offset = animation.value;
                            });

                          animationController
                              .forward()
                              .whenCompleteOrCancel(() {
                            animationController.clearListeners();
                            animationController.reset();
                          });
                        }
                      },
                      onVerticalDragUpdate: (details) {
                        if (details.delta.dy > 0)
                          value.offset =
                              (value.offset - (details.delta.dy) / 10);
                        else {
                          value.offset = value.offset - details.delta.dy * 1.2;
                        }
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15))),
                        width: MediaQuery.of(context).size.width,
                        child: ListView(
                          padding: EdgeInsets.symmetric(
                              vertical: 32, horizontal: 12),
                          physics: NeverScrollableScrollPhysics(),
                          children: [
                            Center(
                                child: Text(
                              "دعوت از دوستان",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.bold),
                            )),
                            Center(
                              child: Text(
                                "با دعوت ما به دوستان خود 5 درصد هدیه از پوپک هدیه بگیرید",
                                style: TextStyle(color: Colors.grey),
                              ),
                            ),
                            Padding(
                                padding: EdgeInsets.only(
                                    right: 16, top: 32, bottom: 16),
                                child: Text(
                                  "مارا با دوستان خود به اشتراک بگذارید",
                                  style: TextStyle(fontSize: 10),
                                )),
                            Card(
                              elevation: 4,
                              child: ListTile(
                                leading: Icon(Icons.sms),
                                title: Text("پیامک متنی"),
                              ),
                            ),
                            Card(
                              elevation: 4,
                              child: ListTile(
                                leading: Icon(FontAwesomeIcons.whatsapp),
                                title: Text("واتس اپ"),
                              ),
                            ),
                            Card(
                              elevation: 4,
                              child: ListTile(
                                leading: Icon(FontAwesomeIcons.instagram),
                                title: Text("اینستاگرام"),
                              ),
                            ),
                            Card(
                              elevation: 4,
                              child: ListTile(
                                leading: Icon(Icons.open_in_browser_outlined),
                                title: Text("صفحه وب"),
                              ),
                            ),
                          ],
                          shrinkWrap: true,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
