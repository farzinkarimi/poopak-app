import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:poopak_app/model/city.dart';

import 'package:poopak_app/view_model/city_vmodel.dart';
import 'package:provider/provider.dart';
import '../home_scroll_controller_and_drawer_key.dart';

class HomePageAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final cityVModel = Provider.of<CityVModel>(context, listen: false);
    return SliverAppBar(

      backgroundColor: Colors.transparent,
      pinned: false,
      leading: IconButton(
        icon: Icon(
          Icons.menu,
          color: Theme.of(context).colorScheme.surface,
        ),
        onPressed: () {
          HomeScrollAndDrawerKey.innerDrawerKey.currentState!
              .open(direction: InnerDrawerDirection.end);
        },
      ),
      actions: [
        IconButton(
          color: Theme.of(context).colorScheme.surface,
          icon: Icon(Icons.search),
          onPressed: () async {},
        )
      ],
      centerTitle: true,
      title: ElevatedButton(
        onPressed: () async {
          showSearch(context: context, delegate: _CustomSearchDelegate());
        },
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(cityVModel.selectedCity!.name),
            SizedBox(
              width: 6,
            ),
            Icon(Icons.keyboard_arrow_down),
          ],
        ),
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.transparent),

            elevation: MaterialStateProperty.all(0),
            foregroundColor: MaterialStateProperty.all(
                Theme.of(context).colorScheme.surface)),
      ),
    );
  }
}

class _CustomSearchDelegate extends SearchDelegate {



  @override
  String? get searchFieldLabel => "جستجو";

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = '';
          },
          icon: Icon(Icons.clear))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, null);
        },
        icon: Icon(Icons.arrow_back));
  }

  @override
  Widget buildResults(BuildContext context) {
    final cityVModel = Provider.of<CityVModel>(context, listen: false);
    List<City> matchQuery = [];
    for (City c in cityVModel.city!) {
      if (c.name.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(c);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) => ListTile(
        onTap: () async{
          await cityVModel.changeCityLocally(matchQuery[index]);
          close(context, null);

        },
        title: Text(matchQuery[index].name),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final cityVModel = Provider.of<CityVModel>(context, listen: false);
    List<City> matchQuery = [];
    for (City c in cityVModel.city!) {
      if (c.name.toLowerCase().contains(query.toLowerCase())) {
        matchQuery.add(c);
      }
    }
    return ListView.builder(
      itemCount: matchQuery.length,
      itemBuilder: (context, index) => ListTile(
        onTap: () async{
          await cityVModel.changeCityLocally(matchQuery[index]);
          close(context, null);

        },
        title: Text(matchQuery[index].name),
      ),
    );
  }
}
