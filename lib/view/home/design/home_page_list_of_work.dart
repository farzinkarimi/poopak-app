import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:poopak_app/model/Products.dart';
import '../../../view_model/product_vmodel.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:provider/provider.dart';

class HomePageListOfWork extends StatelessWidget {
  HomePageListOfWork(this.product, this.controller,this.title);
final String title;
  final List<ProductItem> product;
  final AnimationController controller;

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 12.0, top: 12),
            child: Text(
              title,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            height: MediaQuery.of(context).size.height * 0.3,
            width: MediaQuery.of(context).size.width,
            child: Selector<ProductsVModel, List<ProductItem>>(
              selector: (_, notifier) => notifier.products,
              builder: (context, value, child) => ListView(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                children: List<Widget>.generate(
                  product.length,
                  (index) {
                    final int count = product.length;
                    final Animation<double> animation =
                        Tween<double>(begin: 0.0, end: 1.0).animate(
                      CurvedAnimation(
                        parent: controller,
                        curve: Interval((1 / count) * index, 1.0,
                            curve: Curves.fastOutSlowIn),
                      ),
                    );
                    return Padding(

                      padding:
                          const EdgeInsets.only(right: 16, left: 16, bottom: 16),
                      child: ListShow(product[index], controller, animation),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ListShow extends StatelessWidget {
  ListShow(
    this.product,
    this.animationController,
    this.animation,
  );

  final ProductItem product;
  final AnimationController animationController;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => AutoRouter.of(context).push(ProductRouter(product: product)),
      child: AnimatedBuilder(
        animation: animationController,
        builder: (context, child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(
              transform: Matrix4.translationValues(
                  0.0, 50 * (1 - animation.value), 0.0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.7,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: const BorderRadius.all(Radius.circular(16.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.4),
                      offset: const Offset(-2, 4),
                      blurRadius: 8,
                    ),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(16.0)),
                  child: Stack(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Hero(
                                tag: product.title,
                                child: CachedNetworkImage(
                                  fit: BoxFit.cover,
                                    imageUrl: product.image,
                                    placeholder: (context, url) {
                                      return Container(color: Colors.grey.shade300,);
                                    },
                                    errorWidget: (context, url, error) => Icon(
                                          FontAwesomeIcons.image,
                                          color: Colors.red,
                                        ))),
                          ),
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.only(
                                  bottom: 8, right: 16, left: 16),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  FittedBox(
                                    child: Text(
                                      product.title,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 20),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        product.index.toString(),
                                        style: TextStyle(
                                            fontSize: 14,
                                            color:
                                                Colors.grey.withOpacity(0.8)),
                                      ),
                                      const SizedBox(
                                        width: 4,
                                      ),
                                      Icon(FontAwesomeIcons.mapMarkerAlt,
                                          size: 12,
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Positioned(
                        top: 8,
                        right: 8,
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(32.0),
                            ),
                            onTap: () {},
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Icon(
                                Icons.favorite_border,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
