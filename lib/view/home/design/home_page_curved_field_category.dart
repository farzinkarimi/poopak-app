import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_plus/flutter_swiper_plus.dart';
import 'package:poopak_app/view/home/home_vmodel.dart';
import '../../../main.dart';

import '../../../view_model/product_vmodel.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:provider/provider.dart';

class HomePageCurveField extends StatelessWidget {
  HomePageCurveField();

  @override
  Widget build(BuildContext context) {
    return Consumer<HomeVModel>(
      builder: (context, notifier, _) {
        return Positioned(
          top: notifier.offset > 0 ? -notifier.offset : 0,
          height: notifier.offset > 0 ? 150 : 150 - notifier.offset * 0.9,
          child: Container(
            decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.secondaryVariant,
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(
                        notifier.offset >= 0 ? 20 : 20 + notifier.offset / 4.5),
                    bottomLeft: Radius.circular(notifier.offset >= 0
                        ? 20
                        : 20 + notifier.offset / 4.5))),
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.topCenter,
            child: Container(
              alignment: Alignment.bottomLeft,
              padding: EdgeInsets.only(right: 60, left: 20),
            ),
          ),
        );
      },
    );
  }
}

class CategoryHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var products = Provider.of<ProductsVModel>(context, listen: false);
    int swiperLength=(products.categoryItems.length / 8).ceil();
    return Consumer<HomeVModel>(
      builder: (context, notifier, child) => AnimatedPositioned(
        duration: Duration(milliseconds: 25),
        top: MediaQuery.of(context).size.height * 0.13 - notifier.offset,
        height: MediaQuery.of(context).size.height * 0.3,
        child: child!,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 12),
        child: Card(
          color: Theme.of(context).colorScheme.surface,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          elevation: 6,
          child: Swiper(
            itemBuilder: (context, i) {

              return GridView.count(
              padding: EdgeInsets.all(5),
              physics: NeverScrollableScrollPhysics(),
              crossAxisCount: 4,
              children: List.generate(
                products.categoryItems.length-(8*i)>=8?8:products.categoryItems.length-(8*i),
                (index) {
                  final j=index+(8*i);
                  return ListTile(
                  onTap: () {
                    AutoRouter.of(context).push(
                        CategoryRoute(category: products.categoryItems[j]));
                  },
                  subtitle: FittedBox(
                    fit: BoxFit.scaleDown,
                    child: Text(
                      products.categoryItems[j].title,
                      style: TextStyle(fontSize: 10),
                    ),
                  ),
                  title: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Image.asset(products.categoryItems[j].image),
                    ),
                  ),
                );
                },
              ),
            );
            },
            loop: false,
            pagination: SwiperPagination(
              margin: const EdgeInsets.all(5),
            ),
            itemCount: swiperLength,
          ),
        ),
      ),
    );
  }
}
