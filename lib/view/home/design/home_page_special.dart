import 'package:flutter/material.dart';
import '../../../main.dart';


class HomePageSpecial extends StatelessWidget {
  HomePageSpecial(this.image);
  final String image;
  @override
  Widget build(BuildContext context) {
    return Container(child:
    SliverToBoxAdapter(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(
                right: 12.0, top: 12, bottom: 16),
            child: Text(
              "پیشنهاد ویژه",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            color: Colors.transparent,
            margin: const EdgeInsets.symmetric(
                horizontal: 16.0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                      Radius.circular(25))),
              height:
              MediaQuery.of(context).size.height * 0.25,
              width: MediaQuery.of(context).size.width,
              child: ClipRRect(
                borderRadius:
                BorderRadius.all(Radius.circular(25)),
                child: Image.network(
                  image,
                  width: MediaQuery.of(context).size.width,
                  alignment: Alignment.centerLeft,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
      ),
    ),);
  }
}
