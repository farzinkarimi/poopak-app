
import 'package:flutter/material.dart';


class GridViewWork extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverGrid.count(
      mainAxisSpacing: 16,
      childAspectRatio: 16 / 9,
      crossAxisCount: 3,
      children: [
        Container(
          margin:
          const EdgeInsets.symmetric(horizontal: 8.0),
          decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              Expanded(
                  flex: 3,
                  child: Image.asset(
                      "assets/images/catering.png")),
              Expanded(
                  flex: 2,
                  child: Text(
                    "پذیرایی",
                    style: TextStyle(
                        color: Colors.white, fontSize: 12),
                  ))
            ],
          ),
        ),
        Container(
          margin:
          const EdgeInsets.symmetric(horizontal: 8.0),
          decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              Expanded(
                  flex: 3,
                  child: Image.asset(
                      "assets/images/stairs.png")),
              Expanded(
                  flex: 2,
                  child: Text(
                    "شستشو راه پله",
                    style: TextStyle(
                        color: Colors.white, fontSize: 12),
                  ))
            ],
          ),
        ),
        Container(
          margin:
          const EdgeInsets.symmetric(horizontal: 8.0),
          decoration: BoxDecoration(
              color: Colors.deepOrange,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              Expanded(
                  flex: 3,
                  child: Image.asset(
                      "assets/images/houseCleaning.png")),
              Expanded(
                  flex: 2,
                  child: Text(
                    "نظافت خانه",
                    style: TextStyle(
                        color: Colors.white, fontSize: 12),
                  ))
            ],
          ),
        ),
        Container(
          margin:
          const EdgeInsets.symmetric(horizontal: 8.0),
          decoration: BoxDecoration(
              color: Colors.lightBlue,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              Expanded(
                  flex: 3,
                  child: Image.asset(
                      "assets/images/freight-item.png")),
              Expanded(
                  flex: 2,
                  child: Text(
                    "اسباب کشی",
                    style: TextStyle(
                        color: Colors.white, fontSize: 12),
                  ))
            ],
          ),
        ),
        Container(
          margin:
          const EdgeInsets.symmetric(horizontal: 8.0),
          decoration: BoxDecoration(
              color: Colors.pink,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              Expanded(
                  flex: 3,
                  child: Image.asset(
                      "assets/images/elevator.png")),
              Expanded(
                  flex: 2,
                  child: Text(
                    "تعمیرات آسانسور",
                    style: TextStyle(
                        color: Colors.white, fontSize: 12),
                  ))
            ],
          ),
        ),
        Container(
          margin:
          const EdgeInsets.symmetric(horizontal: 8.0),
          decoration: BoxDecoration(
              color: Colors.purple,
              borderRadius: BorderRadius.circular(15)),
          child: Column(
            children: [
              SizedBox(
                height: 5,
              ),
              Expanded(
                  flex: 3,
                  child: Image.asset(
                      "assets/images/carpet.png")),
              Expanded(
                  flex: 2,
                  child: Text(
                    "قالیشویی",
                    style: TextStyle(
                        color: Colors.white, fontSize: 12),
                  ))
            ],
          ),
        ),
      ],
    );
  }
}
