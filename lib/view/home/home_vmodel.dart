import 'package:flutter/material.dart';
import 'home_scroll_controller_and_drawer_key.dart';

class HomeVModel with ChangeNotifier {
  double _offset = 0;

  HomeVModel() {
   HomeScrollAndDrawerKey.scrollControllerHomePage.addListener(() {
      _offset = HomeScrollAndDrawerKey.scrollControllerHomePage.offset;
      notifyListeners();
    });
  }
  double get offset => _offset;
}
