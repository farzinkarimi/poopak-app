import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import 'design/grid_view_work.dart';
import 'design/home_page_appbar.dart';
import 'design/home_page_list_of_work.dart';
import 'design/home_page_special.dart';
import 'package:styled_widget/styled_widget.dart';
import 'design/home_page_curved_field_category.dart';
import 'home_scroll_controller_and_drawer_key.dart';
import '../../view_model/city_vmodel.dart';
import '../../view_model/product_vmodel.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  late AnimationController animationController;
  late Future<bool> getProductFuture;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    getProductFuture = getProducts();
    super.initState();
  }

  Future<bool> getProducts() async {
    final products = Provider.of<ProductsVModel>(context, listen: false);
    final cityVModel = Provider.of<CityVModel>(context, listen: false);
    final userVModel = Provider.of<UserVModel>(context, listen: false);

    await userVModel.requestAutoAuth();
    cityVModel.deleteData();
    final cityResponse = await cityVModel.requestCities();
    await products.deleteProducts();
    if (cityResponse) {
      if (await products.getProducts(cityVModel.selectedCity!)) {
        animationController.forward();
        return true;
      } else {
         cityVModel.deleteData();
        return false;
      }
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    animationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final value = Provider.of<ProductsVModel>(context, listen: false);
    print('home page build called');
    return FutureBuilder<bool>(
        future: getProductFuture,
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Stack(
                children: [
                  HomePageCurveField(),
                  _WaitingWidget(),
                ],
              );
            default:
              if (value.products.isEmpty) {
                return Center(
                  child: GestureDetector(
                      onTap: () async {
                        setState(() {
                          getProductFuture = getProducts();

                        });
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.refresh,
                            size: 36,
                          ),
                          Text('خطا در دریافت داده'),
                        ],
                      )),
                );
              } else {
                return RefreshIndicator(
                  color: Theme.of(context).colorScheme.secondary,
                  onRefresh: () async {
                    await Future.delayed(Duration(milliseconds: 700));
                    setState(() {
                      getProductFuture = getProducts();

                    });

                  },
                  child: Stack(
                    children: [
                      HomePageCurveField(),
                      CustomScrollView(
                        controller:
                            HomeScrollAndDrawerKey.scrollControllerHomePage,
                        physics: AlwaysScrollableScrollPhysics(
                            parent: BouncingScrollPhysics()),
                        slivers: [
                          HomePageAppBar(),
                          SliverToBoxAdapter(
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.4,
                            ),
                          ),
                          GridViewWork(),
                          if (value.products.any((elements) =>
                              elements.category.contains("نظافت منزل")))
                            HomePageListOfWork(
                                value.products.where((elements) =>
                                    elements.category.contains("نظافت منزل")).toList(), animationController,"نظافت منزل"),
                          if (value.products.any((elements) =>
                              elements.category.contains("حمل و نقل")))
                            HomePageListOfWork(
                                value.products.where((elements) =>
                                    elements.category.contains("حمل و نقل")).toList(), animationController,"حمل و نقل"),
                          SliverToBoxAdapter(
                            child: Container(
                              height: 200,
                            ),
                          ),
                          if (value.products.any((element) =>
                              element.category.contains("پیشنهاد ویژه")))
                            HomePageSpecial(value.products
                                .firstWhere((element) =>
                                    element.category.contains("پیشنهاد ویژه"))
                                .image)
                        ],
                      ),
                      CategoryHomePage()
                    ],
                  ),
                );
              }
          }
        });
  }
}

class _WaitingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Container(
            color: Colors.white30,
            height: 24,
            width: 24,
          ).clipOval(),
          Container(
            width: 120,
            height: 12,
            color: Colors.white30,
          ).clipRRect(all: 15),
          Container(
            color: Colors.white30,
            height: 24,
            width: 24,
          ).clipOval(),
        ]).padding(horizontal: 16, vertical: 12),
        Card(
          elevation: 6,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.3,
            child: GridView.count(
              physics: NeverScrollableScrollPhysics(),
              children: List.generate(
                8,
                (index) => Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      height: 48,
                      width: 48,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            height: 24,
                            width: 24,
                            color: Colors.grey.shade300,
                          ).clipRRect(all: 15).padding(horizontal: 12),
                          Container(
                            height: 4,
                            width: 48,
                            color: Colors.grey.shade300,
                          ).clipRRect(all: 15).padding(horizontal: 12)
                        ],
                      ),
                      color: Colors.grey.shade200,
                    ).clipRRect(all: 10),
                    Container(
                      height: 8,
                      width: 36,
                      color: Colors.grey.shade300,
                    ).clipRRect(all: 15).padding(horizontal: 12)
                  ],
                ),
              ),
              shrinkWrap: true,
              crossAxisCount: 4,
              crossAxisSpacing: 24,
              mainAxisSpacing: 16,
              childAspectRatio: 1 / 1,
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 24),
            ),
          ).decorated(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        ).padding(horizontal: 12, vertical: 4),
        GridView.count(
          physics: NeverScrollableScrollPhysics(),
          children: List.generate(
            6,
            (index) => Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 28,
                    width: 28,
                    color: Colors.grey.shade400,
                  ).clipOval(),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        height: 12,
                        width: 24,
                        color: Colors.grey.shade400,
                      ).clipRRect(all: 15).padding(horizontal: 12),
                      Container(
                        height: 8,
                        width: 48,
                        color: Colors.grey.shade400,
                      ).clipRRect(all: 15).padding(horizontal: 12),
                      Container(
                        height: 8,
                        width: 48,
                        color: Colors.grey.shade400,
                      ).clipRRect(all: 15).padding(horizontal: 12)
                    ],
                  ),
                ],
              ),
              color: Colors.grey.shade300,
            ).clipRRect(all: 20),
          ),
          shrinkWrap: true,
          crossAxisCount: 3,
          mainAxisSpacing: 12,
          crossAxisSpacing: 18,
          childAspectRatio: 16 / 9,
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 48),
        ),
        Row(
          children: [
            Container(
              height: 32,
              width: 32,
              color: Colors.grey.shade400,
            ).clipOval(),
            SizedBox(
              width: 12,
            ),
            Container(
              height: 12,
              width: 128,
              color: Colors.grey.shade400,
            ).clipRRect(all: 15),
          ],
        ).padding(horizontal: 16),
      ],
    );
  }
}
