import 'package:auto_route/auto_route.dart';
import 'package:custom_navigation_bar/custom_navigation_bar.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:poopak_app/view/contact_us/invite_vmodel.dart';
import 'package:poopak_app/view/home/home_vmodel.dart';
import 'package:rive_splash_screen/rive_splash_screen.dart';
import 'package:styled_widget/styled_widget.dart';
import 'package:poopak_app/model/drawer_item_data.dart';
import '../../main.dart';
import 'home_scroll_controller_and_drawer_key.dart';
import '../drawer/drawer.dart';
import '../contact_us/ContantUsScreen.dart';
import '../pay_screen/Payscreen.dart';
import '../../view_model/city_vmodel.dart';
import '../../view_model/product_vmodel.dart';
import '../drawer/drawer_vmodel.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:provider/provider.dart';

class HomeParent extends StatefulWidget {
  @override
  _HomeParentState createState() => _HomeParentState();
}

class _HomeParentState extends State<HomeParent> {
  Widget? drawerScreen;

  @override
  Widget build(BuildContext context) {

    return CustomDrawer(
      changeDrawerCallBack: changeIndex,
      scaffold: Consumer<DrawerVModel>(
        builder: (context, value, child) =>
            value.page == DrawerIndex.HOME ? child! : drawerScreen!,
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: AutoTabsScaffold(

            routes: const [HomeRouter(), OrderRouter(), SettingRouter()],
            bottomNavigationBuilder: (_, tabsRouter) {
              return CustomNavigationBar(
                selectedColor: Theme.of(context).colorScheme.secondary,
                items: [
                  CustomNavigationBarItem(
                    title: Text("خانه").fontSize(12),
                    icon: Icon(EvaIcons.home),
                  ),
                  CustomNavigationBarItem(
                    title: Text("سفارشات").fontSize(12),
                    icon: Icon(EvaIcons.personOutline),
                  ),
                  CustomNavigationBarItem(
                    title: Text("تنظیمات").fontSize(12),
                    icon: Icon(EvaIcons.settings2),
                  ),
                ],
                currentIndex: tabsRouter.activeIndex,
                onTap: (p0) {
                  if (p0 == tabsRouter.activeIndex && p0 == 0) {
                    HomeScrollAndDrawerKey.scrollControllerHomePage.animateTo(
                        0,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeOutExpo);
                    return 0;
                  } else {
                    tabsRouter.setActiveIndex.call(p0);
                  }
                },
              );
            },
          ),
        ),
      ),
    );
  }

  void changeIndex(DrawerIndex drawerIndexData) async {
    var drawerIndex = Provider.of<DrawerVModel>(context, listen: false);
    if (drawerIndex.page != drawerIndexData) {
      if (drawerIndexData == DrawerIndex.HOME) {
        drawerScreen = null;
      } else if (drawerIndexData == DrawerIndex.Pay) {
        drawerScreen = PayScreen();
      } else {
        drawerScreen = ContantUs();
      }
      drawerIndex.updatePage(drawerIndexData);
    }
  }
}
