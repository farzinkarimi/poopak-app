import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';


class HomeScrollAndDrawerKey {
  static  ScrollController scrollControllerHomePage=ScrollController();
  static GlobalKey<InnerDrawerState> innerDrawerKey =
    GlobalKey<InnerDrawerState>();
}
