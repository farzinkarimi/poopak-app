import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:poopak_app/model/address.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import 'package:poopak_app/view/map_location/address_vmodel.dart';
import 'package:poopak_app/view/map_location/view/address/address_botoom_sheet.dart';
import 'package:poopak_app/view/map_location/view/address/address_bottom_sheet_vmodel.dart';
import 'package:provider/provider.dart';

class AddressListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);
    if (valuable.session["address"] == null) {
      valuable.session["address"] = {
        "name": "",
        "addressEntered": "",
        "provinceCity": "",
        "latLng": {}
      };
      valuable.validator.add("address");
    }
    return Selector<BuyVModel, String>(
      selector: (p0, v) => v.session["address"]["provinceCity"],
      builder: (_, value, __) {
        return ListTile(
          onTap: () {
            var selectedAddress =
                Provider.of<AddressBottomSheetVModel>(context, listen: false);
            final buyVModel = Provider.of<BuyVModel>(context, listen: false);
            if ((buyVModel.session["address"]["name"] as String).isNotEmpty)
              selectedAddress.address =
                  UserAddress.fromJson(buyVModel.session["address"]);

            showCupertinoModalBottomSheet(
              useRootNavigator:true ,

              topRadius: Radius.circular(15),
              barrierColor: Colors.black12,
              expand: false,
              context: context,
              builder: (_) {
                return MultiProvider(providers: [
                  ChangeNotifierProvider.value(value: buyVModel),
                  ChangeNotifierProvider.value(
                      value:
                      Provider.of<AddressVModel>(context, listen: false)),
                  ChangeNotifierProvider.value(value: selectedAddress),
                ], child: AddressBottomSheet(userName: "farzinkarimi"));
              },
            );
          },
          title: Wrap(
            spacing: 12,
            children: [
              Icon(
                Icons.location_on,
                color: Colors.black54,
              ),
              if (value.isEmpty)
                Text("انتخاب آدرس")
              else
                Text(valuable.session["address"]["addressEntered"]),
            ],
          ),
          trailing: Icon(Icons.keyboard_arrow_down),
          subtitle: value.isEmpty
              ? Text("آدرس مورد نظر را انتخاب کنید")
              : Text(valuable.session["address"]["name"]),
        );
      },
    );
  }
}
