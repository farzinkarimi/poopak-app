import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:poopak_app/view/map_location/address_vmodel.dart';

import 'package:provider/provider.dart';

class UserNewLocationModal extends StatefulWidget {
  const UserNewLocationModal(this.address, {Key? key}) : super(key: key);

  final Map<String, dynamic> address;

  @override
  _UserNewLocationModalState createState() => _UserNewLocationModalState();
}

class _UserNewLocationModalState extends State<UserNewLocationModal> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  late TextEditingController address;

  late TextEditingController provence;

  late TextEditingController city;

  late TextEditingController postalCode;

  late TextEditingController pelak;

  late TextEditingController userName;

  Future<String?> _submitForm() async {
    if (!_formKey.currentState!.validate()) {
      return null;
    }
    _formKey.currentState!.save();
    Map<String, dynamic> successCache;
    var userService = Provider.of<AddressVModel>(context, listen: false);
//    successCache = await userService.sendPin(cacheUser['mobile']);
//    if (successCache['success'] == true) {
    FocusScope.of(context).unfocus();
    Navigator.of(context).pop();
    Navigator.of(context).pop();
//    } else {
//      return successCache['massage'];
//    }

//    successCache = await x.signup(widget.editingController);
//
//    if (successCache['success'] == true) {
//      widget.pageController.previousPage(duration: null, curve: null);
//    } else {
//      SnackBar m = SnackBar(
//        content: Text(successCache['message'].toString()),
//        backgroundColor: Colors.white,
//      );
//      widget.scaffoldKey.currentState.showSnackBar(m);
//      return;
//    }
  }

  Widget _buildTextField(
      {@required TextEditingController? controller,
      @required String? hint,
      @required String? label,
      @required Icon? icon,
      @required String? Function(String?)? validator,
      @required TextInputAction? textInputAction,
      int line = 1}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: TextFormField(
        style: const TextStyle(fontSize: 12),
        textAlignVertical: TextAlignVertical.center,
        textDirection: TextDirection.rtl,
        controller: controller,
        textAlign: TextAlign.right,
        textInputAction: textInputAction,
        maxLines: line,
        decoration: InputDecoration(
            hintText: hint,
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            prefixIcon: icon,
            labelText: label),
        validator: validator,
        onSaved: (newValue) {},
      ),
    );
  }

  @override
  void initState() {
    address = TextEditingController(text: widget.address["postal_address"]);
    city = TextEditingController(text: widget.address["city"]);
    pelak = TextEditingController();
    provence = TextEditingController(text: widget.address["province"]);
    postalCode = TextEditingController();
    userName = TextEditingController(text: widget.address["userName"]);

    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    address.dispose();
    city.dispose();
    pelak.dispose();
    provence.dispose();
    postalCode.dispose();
    userName.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.8,
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              iconTheme: const IconThemeData(color: Colors.black),
              title: const Text("جزئیات آدرس",
                  style: TextStyle(color: Colors.black)),
            ),
            body: SingleChildScrollView(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
              physics: const BouncingScrollPhysics(),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    _buildTextField(
                        icon: const Icon(FontAwesomeIcons.mapPin),
                        controller: provence,
                        hint: 'کردستان',
                        label: 'استان',
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'لطفا یک استان را وارد کنید';
                          }
                        },
                        textInputAction: TextInputAction.next),
                    _buildTextField(
                        icon: const Icon(FontAwesomeIcons.mapPin),
                        controller: city,
                        hint: 'مریوان',
                        label: 'شهر',
                        validator: (value) {
                          if (value!.isEmpty) return 'لطفا یک شهر را وارد کنید';
                        },
                        textInputAction: TextInputAction.next),
                    _buildTextField(
                        icon: const Icon(FontAwesomeIcons.mapMarked),
                        controller: address,
                        hint: 'مریوان جمهوری کوچه 1',
                        label: 'آدرس',
                        line: 2,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'لطفا یک آدرس صحیح وارد کنید';
                          }
                        },
                        textInputAction: TextInputAction.next),
                    _buildTextField(
                        icon: const Icon(FontAwesomeIcons.mapSigns),
                        controller: pelak,
                        hint: '123',
                        label: 'پلاک',
                        validator: (value) {
                          if (value!.isEmpty){
                            return 'لطفا یک پلاک را وارد کنید';}
                        },
                        textInputAction: TextInputAction.next),
                    _buildTextField(
                        icon: const Icon(FontAwesomeIcons.addressBook),
                        controller: postalCode,
                        hint: '1234567890',
                        label: 'گد پستی',
                        validator: (value) {
                          if (value!.isEmpty && value.length != 10) {
                            return 'لطفا یک کد پستی را وارد کنید';
                          }
                        },
                        textInputAction: TextInputAction.next),
                    _buildTextField(
                        icon: const Icon(FontAwesomeIcons.user),
                        controller: userName,
                        hint: 'محمد محمدی',
                        label: 'نام و نام خانوادگی',
                        validator: (value) {
                          if (value!.isEmpty) return 'وارد کردن نام الزامی است';
                        },
                        textInputAction: TextInputAction.done),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 6),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          primary: Theme.of(context).colorScheme.secondary,
                          padding:const EdgeInsets.symmetric(vertical: 12),
                          side: BorderSide(color: Theme.of(context).colorScheme.secondary),

                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        onPressed: _submitForm,
                        child: const Text("افزودن آدرس"),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

extension Utility on BuildContext {
  void nextEditableTextFocus() {
    do {
      FocusScope.of(this).nextFocus();
    } while (
        FocusScope.of(this).focusedChild!.context!.widget is! EditableText);
  }
}
