import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:poopak_app/view/map_location/view/search/search_vmodel.dart';
import 'package:provider/provider.dart';
import 'package:latlong2/latlong.dart';

class BigModeSearch extends StatefulWidget {
  const BigModeSearch(this.action, this.controller, {Key? key})
      : super(key: key);

  final MapController controller;
  final Function action;

  @override
  _BigModeSearchState createState() => _BigModeSearchState();
}

class _BigModeSearchState extends State<BigModeSearch> {
  final _editingController = TextEditingController();

  @override
  void didChangeDependencies() {
    var valuable = Provider.of<LocationSearchVModel>(context, listen: false);

    _editingController.addListener(() {
      _editingController.text.isEmpty
          ? valuable.closeClear = CloseClear.close
          : valuable.closeClear = CloseClear.clear;
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<LocationSearchVModel>(context, listen: false);

    return Directionality(
      textDirection: TextDirection.rtl,
      child: Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.025,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.symmetric(horizontal: 22),
                child: Row(
                  children: [
                    Selector<LocationSearchVModel, CloseClear>(
                      builder: (context, value, child) {
                        return valuable.closeClear == CloseClear.close
                            ? IconButton(
                                onPressed: () => widget.action.call(),
                                icon: const Icon(
                                  FontAwesomeIcons.chevronRight,
                                  size: 30,
                                ),
                              )
                            : IconButton(
                                onPressed: () => _editingController.clear(),
                                icon: const Icon(
                                  FontAwesomeIcons.times,
                                  size: 30,
                                ),
                              );
                      },
                      selector: (_, cNotifier) => cNotifier.closeClear,
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    Expanded(
                      child: TextField(
                        onChanged: (value) async {
                          valuable.error = await valuable.getData(
                              _editingController.text,
                              widget.controller.center);
                        },
                        controller: _editingController,
                        decoration: const InputDecoration(
                            hintText: "جستجو", border: InputBorder.none),
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(
                height: 10,
              ),
              Consumer<LocationSearchVModel>(
                builder: (_, __, ___) => valuable.error == true
                    ? SizedBox(
                        height: MediaQuery.of(context).size.height * 0.8,
                        width: MediaQuery.of(context).size.width,
                        child: const Center(
                          child: Text("خطا در اتصال"),
                        ),
                      )
                    : valuable.data["value"] == null
                        ? SizedBox(
                            height: MediaQuery.of(context).size.height * 0.5,
                            width: MediaQuery.of(context).size.width,
                            child: Center(
                              child: Icon(
                                FontAwesomeIcons.searchLocation,
                                size: 200,
                                color: Colors.black.withOpacity(0.1),
                              ),
                            ),
                          )
                        : SizedBox(
                            height: MediaQuery.of(context).size.height * 0.8,
                            width: MediaQuery.of(context).size.width,
                            child: ListView.builder(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 5),
                              itemCount: valuable.data["odata.count"],
                              itemBuilder: (context, index) {
                                return Card(
                                  elevation: 2,
                                  child: ListTile(
                                      onTap: () {
                                        widget.controller.move(
                                            LatLng(
                                                valuable.data["value"][index]
                                                    ["geom"]["coordinates"][1],
                                                valuable.data["value"][index]
                                                    ["geom"]["coordinates"][0]),
                                            15);
                                        widget.action.call();
                                      },
                                      subtitle: Text(valuable.data["value"]
                                          [index]["address"]),
                                      leading: const Icon(
                                        FontAwesomeIcons.searchLocation,
                                        size: 24,
                                      ),
                                      title: Text(
                                        valuable.data["value"][index]
                                                ["title"] ??
                                            valuable.data["value"][index]
                                                ["city"],
                                      )),
                                );
                              },
                            ),
                          ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
