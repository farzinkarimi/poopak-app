import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LittleModeSearch extends StatelessWidget {
  const LittleModeSearch({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[50],
      padding: const EdgeInsets.symmetric(horizontal: 20),
      height: 64,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Icon(FontAwesomeIcons.search),
          const Text("جستجو مکان"),
          GestureDetector(
            onTap: () => Navigator.pop(context),
            child: const Icon(FontAwesomeIcons.arrowRight),
          )
        ],
      ),
    );
  }
}