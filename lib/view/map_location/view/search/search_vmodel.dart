import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:latlong2/latlong.dart';
enum CloseClear{close,clear}

class LocationSearchVModel extends ChangeNotifier {
  Map<String, dynamic> _data = {};
  bool _error = false;
  CloseClear _closeClear=CloseClear.close;

  CloseClear get closeClear=>_closeClear;

  Map<String, dynamic> get data => _data;

  bool get error => _error;


  set closeClear(CloseClear c){
    _closeClear=c;
    notifyListeners();
  }

  set error(e) {
    _error = e;
    notifyListeners();
  }

  set data(data) {
    _data = data;
    notifyListeners();
  }



  Future<bool> getData(String s,LatLng center) async {
    http.Response response;
    String api =
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk2ZDQyNTk4ZjI1YjllYzcwZjNhZjU0MmUxMWQ1MDVjZDNjYjMxY2ZiNDUzOTM2MDIyMzZmMTM2NzhiMTY1NmNkOWE1ZmMyN2ExOTgyZDYxIn0.eyJhdWQiOiIxMTcyNiIsImp0aSI6Ijk2ZDQyNTk4ZjI1YjllYzcwZjNhZjU0MmUxMWQ1MDVjZDNjYjMxY2ZiNDUzOTM2MDIyMzZmMTM2NzhiMTY1NmNkOWE1ZmMyN2ExOTgyZDYxIiwiaWF0IjoxNjEzODI0ODYyLCJuYmYiOjE2MTM4MjQ4NjIsImV4cCI6MTYxNDk0ODA2Miwic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.ZoWkYkwAt4O_x3uRFAiR1Zzf2NH9B-i7pacHjrJRTd1w7v55ZWjTQ9uc7V5KAj1eajEopS3mNn4qm8O9uolIfyo-B32h3WJ0qeW6-MlqOIQBj18rtzPYTxlL8DaEwBmZx26Vc8dxNtfJwX9k2UwlehCV2BiHrbW1LqO23STTouS5kiTZMFSTAHGfu7C2U5uRrOE1xp2qouUs8D5KivLI9Aa62eD8rMJ4upIaPN65VnD1WeAZh1woXwsuUQp797tIndLNMMGvpMY3G5otsSMNX7bW3BPTVr9RSiT-G9W_NZ3Tx8sJFthBMhmrL69MxWwkbdA5axtoIKeNQinu4oUh6w";
    final body = json.encode({
      "text": s,
      "\$filter": "city eq مریوان",
      "lat": center.latitude,
      "lon": center.longitude
    });
    try {
      response = await http
          .post(Uri.parse("https://map.ir/search/v2/"),
          headers: {
            "x-api-Key": api,
            "content-type": "application/json"
          },
          body: body)
          .timeout(const Duration(seconds: 5));

    } catch (e) {
      debugPrint(e.toString());
      return true;
    }
    data = json.decode(response.body);
    return false;
  }
}
