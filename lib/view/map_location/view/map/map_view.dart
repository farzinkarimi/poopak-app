import 'package:flutter/material.dart';
import 'package:poopak_app/services/location_service/API/get_address_apis.dart';
import 'package:poopak_app/services/location_service/location_service.dart';


class MapVModel extends ChangeNotifier {
  bool _gettingLocation = false;
  bool _verifyingLocation=false;
  late LocationService _locationService;

  MapVModel(){
   _locationService=LocationService(GetAddressWorkAPIs());
  }

  bool get verifyingLocation=>_verifyingLocation;
  bool get gettingLocation=>_gettingLocation;


  set gettingLocation(bool b) {
    _gettingLocation = b;
    notifyListeners();
  }
  set verifyingLocation(bool b) {
    _verifyingLocation = b;
    notifyListeners();
  }
  Future<bool> mapVerifyLocation(double lat,double lon,int currentCity)async{
    final bool=_locationService.addressAPI.verifyLocation(lat, lon, currentCity);
    notifyListeners();
    return bool;
  }
  Future<Map<String,dynamic>> positionToAddressGet(double lat, double long)async{
    final address =await _locationService.addressAPI.positionToAddress(lat, long);
    return address;
  }
}
