import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:location/location.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:latlong2/latlong.dart';
import 'package:poopak_app/services/location_service/location.dart';
import 'package:poopak_app/view/map_location/address_vmodel.dart';
import 'package:poopak_app/view/map_location/view/complete_address/complete_page.dart';
import 'package:poopak_app/view/map_location/view/map/map_view.dart';
import 'package:poopak_app/view/map_location/view/search/big_search.dart';
import 'package:poopak_app/view/map_location/view/search/little_search.dart';
import 'package:poopak_app/view/map_location/view/search/search_vmodel.dart';

import 'package:provider/provider.dart';

class MapPage extends StatelessWidget {
  MapPage({Key? key,required this.selectedCityCode,required this.userName}) : super(key: key);

  final controller = MapController();
  final String userName;
  final int selectedCityCode;

  @override
  Widget build(BuildContext context) {
    final addressProvider = Provider.of<AddressVModel>(context, listen: false);
    final valuable = Provider.of<MapVModel>(context, listen: false);

    const spinKit =  SpinKitFadingCircle(
      color: Colors.white,
      size: 30.0,
    );
    const spinKit2 = SpinKitThreeBounce(
      color: Colors.white,
      size: 30.0,
    );
    return SafeArea(
      child: Scaffold(

        backgroundColor: Theme.of(context).backgroundColor,
        resizeToAvoidBottomInset: false,
        floatingActionButton: Selector<MapVModel, bool>(
          selector: (_, getting) => getting.gettingLocation,
          builder: (context, value, child) => Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Align(
                alignment: Alignment.bottomRight,
                child: FloatingActionButton(
                  onPressed: () async {
                    valuable.gettingLocation = true;
                    LocationData? cache = await getLoc();
                    valuable.gettingLocation = false;
                    if (cache == null) return;
                    controller.move(
                        LatLng(cache.latitude!, cache.longitude!), 10);
                  },
                  child: value ? spinKit : const Icon(Icons.my_location),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                padding:const EdgeInsets.symmetric(vertical: 6),
                width: MediaQuery.of(context).size.width*0.9,
                  decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondary,
                      border: Border.all(color: Colors.white),
                      borderRadius: BorderRadius.circular(10)),
                  child: Selector<MapVModel, bool>(
                    selector: (_, verify) => verify.verifyingLocation,
                    builder: (context, value, child) => TextButton(
                      onPressed: () async {
                          valuable.verifyingLocation = true;

                          bool cache = await valuable.mapVerifyLocation(
                              controller.center.latitude,
                              controller.center.longitude,
                              selectedCityCode);
                          valuable.verifyingLocation = false;

                          if (cache == true) {
                            var address = await valuable.positionToAddressGet(
                                controller.center.latitude,
                                controller.center.longitude);
                            address["userName"] = userName;
                            showCupertinoModalBottomSheet(
                              expand: false,
                              bounce: true,
                              context: context,
                              builder: (_) =>
                                  ChangeNotifierProvider.value(
                                      value: addressProvider,
                                      child: UserNewLocationModal(address)),
                            );
                          } else{
                            Fluttertoast.showToast(
                                msg: "مکان انتخاب شده با شهر شما مغایرت دارد",
                                backgroundColor: Colors.red);}

                      },
                      child: value == true
                          ? spinKit2
                          : const Text(
                              'تایید مکان',
                              style: TextStyle(color: Colors.white),
                            ),
                    ),
                  )),
            ],
          ),
        ),
        body: Stack(
          children: [
            FlutterMap(
              mapController: controller,
              options: MapOptions(center: LatLng(35.729757025, 51.41730308)),
              layers: [
                TileLayerOptions(
                    urlTemplate:
                        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    subdomains: ['a', 'b', 'c']),
              ],
            ),
            const Center(
              child: Icon(
                FontAwesomeIcons.mapPin,
                size: 50,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 32, left: 10, right: 10),
              child: OpenContainer(
                transitionType: ContainerTransitionType.fadeThrough,
                transitionDuration: const Duration(milliseconds: 500),
                closedBuilder: (context, action) {
                  return const LittleModeSearch();
                },
                openBuilder: (context, action) {
                  return SafeArea(
                    child: ChangeNotifierProvider(
                        create: (context) => LocationSearchVModel(),
                        child: BigModeSearch(action, controller)),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
