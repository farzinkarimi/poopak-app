import 'package:flutter/material.dart';
import 'package:poopak_app/model/address.dart';

class AddressBottomSheetVModel extends ChangeNotifier {
  UserAddress? _address;

  UserAddress? get address => _address;

  set address(UserAddress? address) {
    _address = address;
    notifyListeners();
  }
}
