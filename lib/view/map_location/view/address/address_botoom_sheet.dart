import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/model/address.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import '../../../../main.dart';
import 'package:poopak_app/view/map_location/address_vmodel.dart';
import 'package:poopak_app/view/map_location/view/address/address_bottom_sheet_vmodel.dart';
import 'package:poopak_app/view/map_location/view/map/map_page.dart';
import 'package:poopak_app/view/map_location/view/map/map_view.dart';

import 'package:provider/provider.dart';

class AddressBottomSheet extends StatefulWidget {
  const AddressBottomSheet({Key? key, required this.userName})
      : super(key: key);
  final String userName;

  @override
  State<AddressBottomSheet> createState() => _AddressBottomSheetState();
}

class _AddressBottomSheetState extends State<AddressBottomSheet> {

  @override
  Widget build(BuildContext context) {
    var userAddress = Provider.of<AddressVModel>(context, listen: false);
    var selectedAddress =
        Provider.of<AddressBottomSheetVModel>(context, listen: false);
    return Container(
      color: Theme.of(context).backgroundColor,
      height: MediaQuery.of(context).size.height * 0.5,
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          bottomNavigationBar: CustomRaisedButton(
              onPress: () {
            if (selectedAddress.address == null) {
              Fluttertoast.showToast(
                  msg: "آدرس باید انتخاب شود",
                  textColor: Colors.white,
                  backgroundColor: Colors.red);
              return;
            } else {
              final buyVModel =
                  Provider.of<BuyVModel>(context, listen: false);
              buyVModel.addSession("address", selectedAddress.address!.toJson());
              buyVModel.validator.remove("address");
              Navigator.pop(context);
            }
          },
          textColor: Colors.white,
            text: "تایید",
            color: Theme.of(context).colorScheme.secondary,
          ),

//          bottomNavigationBar: CustomRaisedButton(
//              color: Theme.of(context).Color,
//              text: "تائید آدرس",
//              textColor: Colors.white,
//              onPress: () {
//                if (selectedAddress.address == null) {
//                  Fluttertoast.showToast(
//                      msg: "آدرس باید انتخاب شود",
//                      textColor: Colors.white,
//                      backgroundColor: Colors.red);
//                  return;
//                } else {
//                  final session =
//                  Provider.of<BuyValuableNotifier>(context, listen: false);
//                  session.addSession(
//                      "address", selectedAddress.address!.toJson());
//                  print(session.session["address"]["name"]);
//                  Navigator.pop(context);
//                }
//
//
//
//              }),
          backgroundColor: Theme.of(context).backgroundColor,
          resizeToAvoidBottomInset: false,
          body: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(5)),
                    height: 8,
                    width: 56,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.symmetric(
                        vertical: 16, horizontal: 12),
                    child: OutlineButton.icon(
                      color: Theme.of(context).colorScheme.secondary,
                      highlightedBorderColor: Theme.of(context).colorScheme.secondary,
                      textColor: Colors.black,
                      padding: const EdgeInsets.symmetric(
                          vertical: 12.0, horizontal: 16),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      borderSide:
                          BorderSide(color: Theme.of(context).colorScheme.secondary),
                      icon: Icon(Icons.add_circle,
                          color: Theme.of(context).colorScheme.secondary),
                      label: Text(
                        "افزودن ادرس جدید",
                        style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                      ),
                      onPressed: () {
                        Navigator.push(context, PageTransition(child: MultiProvider(
                          providers: [
                            ChangeNotifierProvider(
                              create: (context) => MapVModel(),
                            ),
                            ChangeNotifierProvider.value(value: userAddress),
                          ],
                          child: MapPage(
                              selectedCityCode: 8623, userName: "userName"),
                        ), type: PageTransitionType.fade));

                      },
                    ),
                  ),
                  Consumer<AddressBottomSheetVModel>(
                    builder: (__, value, _) {
                      return ListView.builder(
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (context, index) {
                          bool selected;
                          if (value.address != null)
                            selected =
                                value.address!.latLng == userAddress.address[index].latLng;
                          else
                            selected = false;

                          print(selected);

                          return Container(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: ListTile(
                              tileColor: selected
                                  ? Theme.of(context)
                                      .colorScheme.secondary
                                      .withOpacity(0.1)
                                  : Colors.transparent,
                              trailing: selected
                                  ? Icon(
                                      Icons.check_circle,
                                      color: Theme.of(context).colorScheme.secondary,
                                    )
                                  : Icon(Icons.radio_button_unchecked),
                              contentPadding: const EdgeInsets.only(
                                  bottom: 12, right: 12, left: 12, top: 8),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                  side: BorderSide(
                                      color: selected
                                          ? Theme.of(context).colorScheme.secondary
                                          : Colors.blueGrey.shade200)),
                              title: FittedBox(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          top: 4, left: 8.0),
                                      child: Icon(Icons.location_on,
                                          color: selected
                                              ? Theme.of(context).colorScheme.secondary
                                              : Colors.black87),
                                    ),
                                    Text(
                                      userAddress.address[index].addressEntered,
                                      style: TextStyle(
                                          color: selected
                                              ? Theme.of(context).colorScheme.secondary
                                              : Colors.black87),
                                    )
                                  ],
                                ),
                              ),
                              subtitle: Container(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(userAddress
                                        .address[index].provinceCity),
                                    Text(userAddress.address[index].name)
                                  ],
                                ),
                              ),
                              onTap: () =>
                                  value.address = userAddress.address[index],
                            ),
                          );
                        },
                        itemCount: userAddress.address.length,
                      );
                    },
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
