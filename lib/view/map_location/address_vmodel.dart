
import 'package:flutter/cupertino.dart';
import 'package:poopak_app/model/address.dart';
import 'package:poopak_app/services/location_service/API/get_address_apis.dart';
import 'package:poopak_app/services/location_service/location_service.dart';

class AddressVModel extends ChangeNotifier {
  List<UserAddress> _address = [];
  late LocationService _locationService;

  AddressVModel() {
    _locationService = LocationService(GetAddressWorkAPIs());
  }

  List<UserAddress> get address => _address;

  set address(List<UserAddress> s) {
    _address = s;
    notifyListeners();
  }

  void addNewAddress(UserAddress address) {
    _address.add(address);
    notifyListeners();
  }

  void deleteAddress(UserAddress address) {
    _address.remove(address);
    notifyListeners();
  }

  void deleteAddressIndex(int index) {
    _address.removeAt(index);
    notifyListeners();
  }

  UserAddress? getSingleAddress(int index) {
    try {
      return _address[index];
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  Future<bool> getUserAddress() async {
    final addresses;
    try {
       addresses = await _locationService.addressAPI.getUserAddresses();
    }
    catch(e) {
      print(e);
      return false;
    }
    bool b;
    if (addresses == null) {
      b = false;
    } else {
      _address = addresses;
      b = true;
    }
    notifyListeners();
    return b;
  }
}
