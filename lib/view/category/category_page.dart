import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:poopak_app/model/CategoryItem.dart';
import 'package:poopak_app/model/Products.dart';
import 'package:poopak_app/routes/setting_route.gr.dart';
import 'package:styled_widget/styled_widget.dart';
import '../../view_model/product_vmodel.dart';
import 'package:provider/provider.dart';

class CategoryPage extends StatelessWidget {
  final CategoryItem _category;

  CategoryPage(this._category);

  @override
  Widget build(BuildContext context) {
    var products = Provider.of<ProductsVModel>(context, listen: false);
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text(_category.title).fontWeight(FontWeight.bold),
          centerTitle: true,
        ),
        body: ListView.builder(
          physics:
              AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),
          itemCount: _category.products.length,
          itemBuilder: (context, index) {
            ProductItem temp = products.products.firstWhere(
                (element) => element.key == _category.products[index]);
            return GestureDetector(
              onTap: () =>
                  AutoRouter.of(context).push(ProductRouter(product: temp)),
              child: Card(
                elevation: 5,
                child: Row(
                  children: [
                    Expanded(
                        flex: 3,
                        child: CachedNetworkImage(imageUrl: temp.image,fit: BoxFit.cover,height: double.maxFinite,).padding(left: 12)),
                    Expanded(
                        flex: 4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(temp.title).bold().fontSize(16),
                            FittedBox(child: Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(temp.subtitle),
                            )),
                          ],
                        )),
                  ],
                ).backgroundColor(Colors.white).clipRRect(all: 15),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
              ).height(100).padding(horizontal: 12).clipRRect(all: 15),
            );
          },
        ),
      ),
    );
  }
}
