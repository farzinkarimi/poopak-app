import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:poopak_app/model/Time.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import 'package:poopak_app/view/date_and_time/time/time_bottom_sheet.dart';
import 'package:poopak_app/view/date_and_time/time/time_vmodel.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

class TimeListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        showCupertinoModalBottomSheet(
          context: context,
          builder: (_) => MultiProvider(providers: [
            ChangeNotifierProvider.value(
              value: Provider.of<TimeVModel>(context, listen: false),
            ),
            ChangeNotifierProvider.value(
              value: Provider.of<BuyVModel>(context, listen: false),
            )
          ], child: TimeWidget()),
          topRadius: Radius.circular(20),
          barrierColor: Colors.black12,
        );
      },
      title: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "ساعت در دسترس سرویس",
          style: TextStyle(color: Colors.grey, fontSize: 14),
        ),
      ),
      subtitle: Row(
        children: [
          Icon(
            FontAwesomeIcons.clock,
            color: Colors.black54,
          ),
          SizedBox(
            width: 12,
          ),
          Selector3<BuyVModel, BuyVModel,
              BuyVModel, Tuple3<int, int, int>>(
            selector: (p0, p1, p2, p3) => Tuple3(p1.session["clockHour"],
                p2.session["clockMinutes"], p3.session["timeTakes"]),
            builder: (_, value, __) {
              Clock temp = Clock(hour: value.item1, minute: value.item2);
              return Text(
                "${temp.showClock()} تا ${temp.showClock(hour: value.item3)}",
                style: TextStyle(color: Colors.black),
              );
            },
          )
        ],
      ),
      trailing: Icon(Icons.keyboard_arrow_down),
    );
  }
}
//shouldRebuild: (previous, next) {
//if (previous.hour != next.hour ||
//previous.minute != next.minute)
//return true;
//else
//return false;
//},
