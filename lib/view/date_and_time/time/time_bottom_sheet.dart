import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import 'package:poopak_app/view/date_and_time/time/time_vmodel.dart';
import 'select_time.dart';
import 'package:poopak_app/helpers/const.dart';
import '../../../main.dart';
import 'package:provider/provider.dart';
import 'package:styled_widget/styled_widget.dart';

class TimeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var timeVModel = Provider.of<TimeVModel>(context, listen: false);
    var valuable = Provider.of<BuyVModel>(context, listen: false);

    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          bottomNavigationBar: Container(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: CustomRaisedButton(
                color: Theme.of(context).colorScheme.secondary,
                text: "تائید ساعت",
                textColor: Colors.white,
                onPress: () {
                  if (timeVModel.time.hour + timeVModel.selectedHourTime > 21) {
                    Fluttertoast.showToast(
                        msg: "ساعت پایانی کار قبل از 21 باشد",
                        textColor: Colors.white,
                        backgroundColor: Colors.red);
                    return;
                  } else{
                    valuable.addSession("clockHour",
                        timeVModel.time.hour);
                    valuable.addSession("clockMinutes",
                        timeVModel.time.minute);
                    valuable.addSession("timeTakes",
                        timeVModel.selectedHourTime);
                    Navigator.pop(context);}
                }),
          ),
          body: Column(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.access_time,
                    color: Colors.black54,
                  ),
                  SizedBox(
                    height: 40,
                    width: 8,
                  ),
                  Text(
                    "ساعت",
                    style: TextStyle(color: Colors.black54),
                  ),
                ],
              ).padding(horizontal: 12),
              FittedBox(
                child: TimePickerWidget(
                  initial: timeVModel.time.showClock(),
                  onChange: (s) {
                    print(s);
                    timeVModel.time = s;
                  },
                ),
              ),
              Container(
                alignment: Alignment.topRight,
                padding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
                child: Text(
                  'کار چند ساعت طول میکشد؟',
                  style: TextStyle(fontSize: 12, color: Colors.black54),
                ),
              ),
              Container(
                height: 64,
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: List.generate(
                      9,
                      (index) => Selector<TimeVModel, int>(
                            selector: (_, changeNotifier) =>
                                changeNotifier.selectedHourTime,
                            shouldRebuild: (previous, next) => next != previous,
                            builder: (context, value, child) {
                              return ListOfNumberGenerator(
                                  value: value,
                                  index: index + 1,
                                  set: (__,_) =>
                                      timeVModel.selectedHourTime = index + 1);
                            },
                          )),
                ),
              ),
            ],
          ).scrollable(),
        ),
      ),
    );
  }
}

String _format(DateTime t) {
  return "${t.hour}:${t.minute}";
}
