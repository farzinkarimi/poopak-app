import 'package:flutter/material.dart';
import 'package:poopak_app/model/Time.dart';


class TimeVModel extends ChangeNotifier
{
  int _selectedHourTime=1;
  Clock _time = Clock(hour: 8, minute: 0);

  Clock get time => _time;

  int get selectedHourTime => _selectedHourTime;

  set selectedHourTime(int time) {
    _selectedHourTime = time;
    notifyListeners();
  }


  set time(Clock t) {
    _time = t;
    notifyListeners();
  }

}