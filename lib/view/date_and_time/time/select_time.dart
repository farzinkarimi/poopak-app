import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poopak_app/model/Time.dart';
import '../../../main.dart';

class TimePickerWidget extends StatefulWidget {
  final Function(Clock) onChange;
  final String initial;

  const TimePickerWidget({Key? key,required this.onChange,required this.initial})
      : super(key: key);

  @override
  _TimePickerWidgetState createState() => _TimePickerWidgetState();
}

class _TimePickerWidgetState extends State<TimePickerWidget> {
  Widget durationPicker(
      {bool inMinutes = false,required int initialHour,required int initialMin}) {
    return CupertinoPicker(
      diameterRatio: 1,
      scrollController: FixedExtentScrollController(
          initialItem: inMinutes ? initialMin : initialHour),
      magnification: 1.7,
      onSelectedItemChanged: (x) {
        if (inMinutes) {
          currentTimeInMin = (x * 30).toString() != ""
              ? (x * 30).toString()
              : initialMin.toString();
          currentTimeInHour = (initialHour + 8).toString();
        } else {
          currentTimeInHour = (x + 8).toString() != ""
              ? (x + 8).toString()
              : initialHour.toString();
          currentTimeInMin = (initialMin * 30).toString();
        }

        widget.onChange(
          Clock(hour: int.parse(currentTimeInHour), minute: int.parse(currentTimeInMin))
        );
      },
      children: List.generate(
          inMinutes ? 2 : 13,
          (index) => Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: FittedBox(
                  child: Column(
                    children: [
                      Text(inMinutes ? '${index * 30}' : '${index + 8}',
                          style: TextStyle(color: Theme.of(context).colorScheme.secondary)),
                    ],
                  ),
                ),
              )),
      itemExtent: 40,
    );
  }

  String currentTimeInHour = '';
  String currentTimeInMin = '';

  @override
  Widget build(BuildContext context) {
    int initialHour = widget.initial[1] != ":"
        ? int.parse(widget.initial.substring(0, 2)) - 8
        : int.parse(widget.initial.substring(0, 1)) - 8;
    int initialMin =
        widget.initial[widget.initial.length - 2] == 3.toString() ? 1 : 0;
    return DefaultTextStyle(
      style: TextStyle(color: Colors.white),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: MediaQuery.of(context).size.height * 0.15,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                            child: durationPicker(
                                inMinutes: true,
                                initialMin: initialMin,
                                initialHour: initialHour)),
                        Text(
                          ":",
                          style: TextStyle(
                              color: Theme.of(context).colorScheme.secondary,
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                        ),
                        Expanded(
                            child: durationPicker(
                                initialMin: initialMin,
                                initialHour: initialHour)),
                      ],
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
