import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:poopak_app/view/buy/buy_vmodel.dart';
import 'package:poopak_app/view/date_and_time/date/date_vmodel.dart';
import 'date_bottom_sheet.dart';
import 'package:provider/provider.dart';

class DateListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final buyVModel = Provider.of<BuyVModel>(context, listen: false);
    if (buyVModel.session["date"] == null) {
      buyVModel.session["date"] = [];
      buyVModel.validator.add("date");
    }
    return ListTile(
      onTap: () => showCupertinoModalBottomSheet(
        bounce: true,
        enableDrag: false,
        context: context,
        builder: (_) => MultiProvider(providers: [
          ChangeNotifierProvider.value(value: buyVModel),
          ChangeNotifierProvider.value(
              value: Provider.of<DateVModel>(context, listen: false))
        ], child: DateBottomSheet()),
        topRadius: Radius.circular(20),
        barrierColor: Colors.black12,
      ),
      title: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          "تاریخ سرویس",
          style: TextStyle(color: Colors.grey, fontSize: 14),
        ),
      ),
      subtitle: Selector<BuyVModel, List<String>>(
        selector: (p0, p1) => p1.session["date"].cast<String>(),
        builder: (_, value, __) => value.isNotEmpty
            ? Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                runSpacing: 8,
                spacing: 12,
                children: List.generate(
                  value.length,
                  (index) {
                    return value[index] != ""
                        ? Column(
                            children: <Widget>[
                              Chip(
                                label: Text(
                                  value[index],
                                  style: TextStyle(color: Colors.black),
                                ),
                              ),
                            ],
                          )
                        : Container();
                  },
                )..insert(
                    0,
                    Icon(
                      FontAwesomeIcons.calendar,
                      color: Colors.black54,
                    ),
                  ),
              )
            : Text(
                "تاریخ مورد نظر را انتخاب کنید",
                style: TextStyle(color: Colors.black),
              ),
      ),
      trailing: Icon(Icons.keyboard_arrow_down),
    );
  }
}
