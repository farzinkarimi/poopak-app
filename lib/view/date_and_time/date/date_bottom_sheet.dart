import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:poopak_app/view/date_and_time/date/date_vmodel.dart';
import 'select_date.dart';
import 'package:poopak_app/helpers/const.dart';
import '../../../main.dart';
import '../../buy/buy_vmodel.dart';
import 'package:provider/provider.dart';

class DateBottomSheet extends StatefulWidget {
  @override
  _DateBottomSheetState createState() => _DateBottomSheetState();
}

List<Widget> getDates(BuildContext context) {
  var value = Provider.of<DateVModel>(context, listen: true);
  List<Widget> datesTextFields = [];

  for (int i = 0; i < value.date.length; i++) {
    datesTextFields.add(Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: SelectDate(i)),
    );
  }
  return datesTextFields;
}

class _DateBottomSheetState extends State<DateBottomSheet> {
  @override
  Widget build(BuildContext context) {
    var dateVModel = Provider.of<DateVModel>(context, listen: false);
    var buyVModel = Provider.of<BuyVModel>(context, listen: false);
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          bottomNavigationBar: Container(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: CustomRaisedButton(
                color: Theme.of(context).colorScheme.secondary,
                text: "تائید تاریخ",
                textColor: Theme.of(context).colorScheme.surface,
                onPress: () {
                  if ((dateVModel.date.isNotEmpty) &&
                      (!dateVModel.date.any((element) => element == ""))) {
                    buyVModel.addSession("date",
                        [...dateVModel.date]);
                    buyVModel.validator.remove("date");
                    Navigator.pop(context);
                  } else
                    Fluttertoast.showToast(
                        msg: "تاریخ باید انتخاب شود",
                        textColor: Theme.of(context).colorScheme.surface,
                        backgroundColor: Theme.of(context).colorScheme.error);
                }),
          ),
          backgroundColor: Theme.of(context).backgroundColor,
          resizeToAvoidBottomInset: false,
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Column(
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.date_range,
                        color: Colors.black54,
                      ),
                      SizedBox(
                        height: 40,
                        width: 8,
                      ),
                      Text(
                        "تاریخ",
                        style: TextStyle(color: Colors.black54),
                      ),
                    ],
                  ),
                  ...getDates(context),
                  Column(
                    children: [
                      Text("افزودن تاریخ جدید"),
                      SizedBox(
                        height: 8,
                      ),
                      Card(
                        color: Colors.white,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: DottedBorder(
                          color: Colors.grey,
                          radius: Radius.circular(10),
                          borderType: BorderType.RRect,
                          strokeWidth: 2,
                          child: InkWell(
                            onTap: () {
                              bool flag = true;
                              for (int i = 0;
                                  i < dateVModel.date.length;
                                  i++) {
                                if (dateVModel.date[i] == "") {
                                  flag = false;
                                  break;
                                }
                              }
                              if (flag) dateVModel.addDateTime("");
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 24, vertical: 4),
                              decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                              ),
                              child: FittedBox(
                                child: Row(
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.symmetric(
                                          horizontal: 2),
                                      child: DottedBorder(
                                        color: Colors.grey,
                                        borderType: BorderType.RRect,
                                        radius: Radius.circular(10),
                                        child: Text(
                                          "      +      ",
                                          style: TextStyle(color: Colors.grey),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 8),
                                      child: DottedBorder(
                                        color: Colors.grey,
                                        radius: Radius.circular(10),
                                        borderType: BorderType.RRect,
                                        child: Text(
                                          "      +      ",
                                          style: TextStyle(color: Colors.grey),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 8),
                                      child: DottedBorder(
                                        color: Colors.grey,
                                        radius: Radius.circular(10),
                                        borderType: BorderType.RRect,
                                        child: Text(
                                          "               +                ",
                                          style: TextStyle(color: Colors.grey),
                                        ),
                                      ),
                                    ),
                                    IgnorePointer(
                                      child: IconButton(
                                        onPressed: () {},
                                        icon: Icon(
                                          Icons.add_circle,
                                          color: Colors.green,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
