import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:persian_datetime_picker/persian_datetime_picker.dart';
import 'package:poopak_app/helpers/DateLine.dart';
import 'package:poopak_app/helpers/const.dart';
import 'package:poopak_app/helpers/format_date.dart';
import 'package:poopak_app/view/date_and_time/date/date_vmodel.dart';
import '../../buy/buy_vmodel.dart';
import 'package:provider/provider.dart';

class SelectDate extends StatefulWidget {
  final int index;

  SelectDate(this.index);

  @override
  _SelectDateState createState() => _SelectDateState();
}

class _SelectDateState extends State<SelectDate> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<DateVModel>(context, listen: false);
    bool hasDate = valuable.getSingleDateTime(widget.index) == "";
    bool selectedTomorrow = hasDate
        ? false
        : valuable.getSingleDateTime(widget.index) ==
            format(Jalali.now().addDays(1));
    bool selectedToday = hasDate
        ? false
        : valuable.getSingleDateTime(widget.index) == format(Jalali.now());
    bool selectedMonth = selectedToday
        ? false
        : selectedTomorrow
            ? false
            : valuable.getSingleDateTime(widget.index) == ""
                ? false
                : true;
    return Column(
      children: [
        Selector<DateVModel, String>(
          selector: (_, p1) => p1.date[widget.index],
          builder: (_, value, __) =>
              hasDate ? Text("انتخاب تاریخ") : Text(value),
        ),
        FittedBox(
          child: Row(
            children: [
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: dateSelectButton(
                    color: Theme.of(context).colorScheme.secondary,
                    text: "امروز",
                    isSelected: selectedToday,
                    onsSelect: () {
                      if (!valuable.date.any(
                        (element) =>
                            element ==
                            format(
                              Jalali.now(),
                            ),
                      )) {
                        !selectedToday
                            ? valuable.chooseDateTime(
                                format(Jalali.now()), widget.index)
                            : valuable.deleteDateTime(format(Jalali.now()));
                      }
                    },
                  )),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: dateSelectButton(
                      color: Theme.of(context).colorScheme.secondary,
                      text: "فردا",
                      isSelected: selectedTomorrow,
                      onsSelect: () {
                        if (!valuable.date.any(
                          (element) =>
                              element ==
                              format(
                                Jalali.now().addDays(1),
                              ),
                        )) {
                          !selectedTomorrow
                              ? valuable.chooseDateTime(
                                  format(Jalali.now().addDays(1)), widget.index)
                              : valuable.deleteDateTime(
                                  format(Jalali.now().addDays(1)));
                        }
                      })),
              Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FittedBox(
                    child: dateSelectButton(
                      color: Theme.of(context).colorScheme.secondary,
                      text: "انتخاب تاریخ",
                      isSelected: selectedMonth,
                      icon: Icons.arrow_drop_down,
                      onsSelect: () {
                        showCupertinoModalBottomSheet(
                          expand: false,
                          context: context,
                          builder: (context) => Container(
                            height: 150,
                            child: PCupertinoDatePicker(
                              initialDateTime: Jalali.now().add(days: 2),
                              mode: PCupertinoDatePickerMode.date,
                              maximumDate: Jalali.now().add(months: 1),
                              backgroundColor:
                                  Theme.of(context).backgroundColor,
                              minimumDate: Jalali.now().add(days: 2),

                              onDateTimeChanged: (value) {
                                valuable.chooseDateTime(
                                    format(value), widget.index);
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  )),
              IconButton(
                  onPressed: valuable.date.length == 1
                      ? null
                      : widget.index == valuable.date.length - 1
                          ? () {
                              valuable.deleteDateTimeRemove(widget.index);
                            }
                          : null,
                  icon: valuable.date.length == 1
                      ? Icon(
                          Icons.check_circle,
                          color: Theme.of(context).colorScheme.secondary,
                        )
                      : widget.index == valuable.date.length - 1
                          ? Icon(
                              Icons.remove_circle,
                              color: Colors.red,
                            )
                          : Icon(
                              Icons.check_circle,
                              color: Theme.of(context).colorScheme.secondary,
                            )),
            ],
          ),
        ),
      ],
    );
  }
}
