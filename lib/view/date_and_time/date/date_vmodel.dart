


import 'package:flutter/cupertino.dart';

class DateVModel extends ChangeNotifier{


  List<String> _date=[].cast<String>();
  List<String> get date => _date;

  String getSingleDateTime(int index) {
    if (_date.length <= index)
      return "";
    else
      return _date[index];
  }
  set date(List<String> dateTime) {
    _date = dateTime;
    notifyListeners();
  }

  void chooseDateTime(String date, int index) {
    _date.length == 0
        ? _date = [..._date, date]
        : index == _date.length
        ? _date = [..._date, date]
        : _date[index] = date;
    notifyListeners();
  }

  void addDateTime(String date) {
    _date.add(date);
    notifyListeners();
  }

  void deleteDateTime(String date) {
    _date[_date.indexOf(date)] = "";
    notifyListeners();
  }

  void deleteDateTimeRemove(int index) {
    _date.removeAt(index);
    notifyListeners();
  }


}