import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:poopak_app/view/date_and_time/date/date_list_tile.dart';
import 'package:poopak_app/view/date_and_time/time/time_list_tile.dart';

import '../buy/buy_vmodel.dart';
import 'package:provider/provider.dart';

class DateAndTimeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var valuable = Provider.of<BuyVModel>(context, listen: false);

    valuable.session["clockHour"] = 8;
    valuable.session["clockMinutes"] = 0;
    valuable.session["timeTakes"] = 1;
    return Padding(
      padding: const EdgeInsets.only(left: 12),
      child: Column(
        children: [DateListTile(), Divider(), TimeListTile()],
      ),
    );
  }
}
