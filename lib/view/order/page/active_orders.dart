import 'package:flutter/material.dart';
import 'order_detail.dart';
import '../order_vmodel.dart';
import 'package:provider/provider.dart';
import 'package:styled_widget/styled_widget.dart';

class ActiveOrder extends StatefulWidget {
  @override
  _ActiveOrderState createState() => _ActiveOrderState();
}

class _ActiveOrderState extends State<ActiveOrder> {
  @override
  Widget build(BuildContext context) {
    var _ordersProvider = Provider.of<OrderVModel>(context, listen: false);

    return FutureBuilder<bool>(
        future: _ordersProvider.loadOrders(),
        builder: (context, snapshot) => snapshot.connectionState ==
                ConnectionState.waiting
            ? _WaitingWidget()
            : snapshot.data == true
                ? Container(
                    child: ListView(
                      physics: AlwaysScrollableScrollPhysics(parent: BouncingScrollPhysics()),

                      children: List.generate(
                        _ordersProvider.orders.length,
                        (index) {
                          final tempOrder = _ordersProvider.indexOfOrder(index);
                          if (tempOrder != null)
                            return Card(
                              elevation: 5,
                              margin: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 10),
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary
                                          .withOpacity(0.2)),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15))),
                              child: ListTile(
                                  onTap: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(
                                      builder: (context) =>
                                          OrderDetail(tempOrder),
                                    ));
                                  },
                                  subtitle: Text(tempOrder.status),
                                  trailing: Text(
                                      "${tempOrder.price.toString()} هزار تومان "),
                                  title: Text(tempOrder.typeOfOrder)),
                            );
                          else {
                            return Container();
                          }
                        },
                      ),
                    ),
                  )
                : Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: Opacity(
                  opacity: 0.7,
                  child: Image.asset(
                    'assets/images/OrderPageImage.png',
                    scale: 2,
                  )),
            ),
            Text(' بدون سفارش  '),
            Text('برای مشاهده ی سفارش های خود وارد شوید')
          ],
        ),);
  }
}

class _WaitingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: [
        ...List.generate(10, (index) =>Card(
          elevation: 5,

          child: Container(

            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  color: Colors.grey.shade300,
                  height: 48,
                  width: 48,
                ).clipRRect(all: 15).padding(right: 12)
                ,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      color: Colors.grey.shade300,
                      height: 18,
                      width: 200,
                      alignment: Alignment.topLeft,
                    ).clipRRect(all: 15),
                    Container(
                      color: Colors.grey.shade300,
                      height: 10,
                      width: 150,
                      alignment: Alignment.topLeft,
                    ).clipRRect(all: 15),
                  ],
                )
              ],
            ),
            height: 72,
            width: MediaQuery.of(context).size.width,
          )
              .decorated(
            color: Colors.grey.shade50,
          )
              .clipRRect(all: 15),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15)),
        ).padding(top: 12, horizontal: 12), )
      ],
    );
  }
}
