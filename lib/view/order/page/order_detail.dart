import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:poopak_app/model/order.dart';
import 'package:poopak_app/helpers/getAddress.dart';
import 'package:latlong2/latlong.dart';
import '../../../main.dart';
import 'package:styled_widget/styled_widget.dart';

class OrderDetail extends StatefulWidget {
  OrderDetail(this._order);

  final Order _order;

  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
  MapController controller = MapController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          bottom: PreferredSize(child: Align(
              alignment: Alignment.centerRight,
              child: Text(
                widget._order.typeOfOrder,
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              )).padding(right: 16), preferredSize: Size.fromHeight(52)),
          elevation: 0,
          backgroundColor: Colors.transparent,
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          child: SingleChildScrollView(
            child: Column(
              textDirection: TextDirection.rtl,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    widget._order.status == "در حال انجام"
                        ? Container(
                            height: 20,
                            width: 20,
                            child: SpinKitDoubleBounce(
                              color: Colors.blue,
                            ),
                          )
                        : widget._order.status == "لغو شده"
                            ? Container(
                                height: 20,
                                width: 20,
                                child: Icon(
                                  Icons.circle,
                                  color: Colors.red,
                                  size: 16,
                                ))
                            : Container(
                                height: 20,
                                width: 20,
                                child: Icon(
                                  Icons.check_box,
                                  color: Colors.green,
                                  size: 24,
                                )),
                    SizedBox(
                      width: 6,
                    ),
                    Text(widget._order.status),
                  ],
                ),
SizedBox(height: 12,),
                Container(
                  padding: EdgeInsets.all(24),
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.secondary,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("قیمت : ").bold().textColor(Colors.white),
                      Text(" تومان ${widget._order.price} ~  ").bold().textColor(Colors.white)
                    ],
                  ),
                ),
                SizedBox(height: 16,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("نحوه پرداخت : "),
                    Text("${widget._order.payType}")
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text("تاریخ ارسال :  "), Text(widget._order.date)],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("آدرس : "),
                    FutureBuilder<Map<String, dynamic>>(
                        future: getAddress(
                            long: widget._order.location.longitude,
                            lat: widget._order.location.latitude),
                        builder: (context, snapshot) =>
                           snapshot.connectionState == ConnectionState.waiting
                                ? CupertinoActivityIndicator()
                                : Text(snapshot.data!["postal_address"])
                        ),
                  ],
                ),
                IgnorePointer(
                  child: Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    child: FlutterMap(
                      mapController: controller,
                      options: MapOptions(
                          center: LatLng(widget._order.location.latitude!,
                              widget._order.location.longitude!)),
                      layers: [
                        TileLayerOptions(
                            urlTemplate:
                                'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            subdomains: ['a', 'b', 'c']),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
