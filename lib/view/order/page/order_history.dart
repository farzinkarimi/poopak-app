import 'package:flutter/material.dart';


class HistoryTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Opacity(
              opacity: 0.7,
              child: Image.asset(
                'assets/images/OrderPageImage.png',
                scale: 2,
              )),
        ),
        Text('بدون سفارش فعال'),
        Text('برای مشاهده ی سفارش های خود وارد شوید')
      ],
    );
  }
}