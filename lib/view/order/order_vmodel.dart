import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:poopak_app/model/order.dart';
import 'package:http/http.dart' as http;

class OrderVModel extends ChangeNotifier {
  List<Order> _orders = [];

  List<Order> get orders => _orders;

  set orders(List<Order> order) {
    _orders = orders;
    notifyListeners();
  }

  Order? indexOfOrder(int index) {
    try {
      return _orders[index];
    } catch (e) {
      print("bad index $e");
      return null;
    }
  }

  bool setNewOrder(Order order) {
    try {
      _orders.add(order);
      notifyListeners();
      return true;
    } catch (e) {
      print(e);

      return false;
    }
  }

  Future<bool> loadOrders() async {
    print("load called");
    http.Response response;
    try {
      response = await http.get(
          Uri.parse(
              "https://api.json-generator.com/templates/TzjoxS3jrdFg/data"),
          headers: {
            "Authorization": "Bearer 71f3ii0o09eshcs49425vr8jpiz4wdlupqj9yls8"
          }).timeout(Duration(seconds: 10));
    } catch (e) {
      print(e);
      return false;
    }

    if (response.statusCode == 200 || response.statusCode == 201) {

      final List<dynamic> x = jsonDecode(response.body);
      _orders=x.map((e) => Order.fromJson(e as Map<String, dynamic>)).toList();
      return true;
    } else {
      return false;
    }
  }
}
