import 'package:flutter/material.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import 'page/active_orders.dart';
import 'order_vmodel.dart';
import '../auth/auth_vmodel.dart';
import 'page/order_history.dart';
import 'package:provider/provider.dart';

class OrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userService = Provider.of<UserVModel>(context, listen: false);

    return SafeArea(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Theme.of(context).backgroundColor,
            resizeToAvoidBottomInset: false,
          appBar: TabBar(labelColor: Theme.of(context).colorScheme.onPrimary, tabs: [
            Tab(
              text: 'سفارش های فعال',
            ),
            Tab(
              text: 'سفارش های گذشته',
            )

          ],indicatorColor: Theme.of(context).colorScheme.onPrimary,automaticIndicatorColorAdjustment: true,),
          body: TabBarView(physics: BouncingScrollPhysics(), children: [
            ChangeNotifierProvider(
                create: (context) => OrderVModel(), child: ActiveOrder()),
            HistoryTab()
          ]),
        ),
      ),
    );
  }
}




