
import 'package:flutter/material.dart';
import 'package:poopak_app/model/drawer_item_data.dart';


class DrawerVModel with ChangeNotifier {
  double _offset=0;
  DrawerIndex _page=DrawerIndex.HOME;


  void updateOffset(double newOffset){
      _offset = newOffset;
      notifyListeners();
    }
  void updatePage(DrawerIndex newPage){
    _page=newPage;
    notifyListeners();
  }

  double get offset => _offset;
  DrawerIndex get page=>_page;
}