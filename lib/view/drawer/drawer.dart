import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:poopak_app/view/drawer/design/drawer_design.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import 'drawer_vmodel.dart';

import '../home/home_scroll_controller_and_drawer_key.dart';
import 'package:provider/provider.dart';

class CustomDrawer extends StatelessWidget {
  CustomDrawer(
      {required this.scaffold,

      required this.changeDrawerCallBack});

  final Function changeDrawerCallBack;
  final Widget scaffold;

  @override
  Widget build(BuildContext context) {
    var userService = Provider.of<UserVModel>(context, listen: false);

    var drawerOffset =
        Provider.of<DrawerVModel>(context, listen: false);
    return InnerDrawer(
      rightAnimationType: InnerDrawerAnimation.quadratic,
      key: HomeScrollAndDrawerKey.innerDrawerKey,
      swipeChild: true,
      offset: IDOffset.only(right: 0.2),
      onDragUpdate: (double offset, index) {
        drawerOffset.updateOffset(offset);
      },
      backgroundDecoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
      ),
      colorTransitionScaffold: Colors.transparent,
      colorTransitionChild: Theme.of(context).colorScheme.onBackground,
      boxShadow: [
        BoxShadow(
            color: Theme.of(context).colorScheme.onBackground, offset: Offset(1, 5), blurRadius: 10)
      ],
      scale: IDOffset.horizontal(0.95),
      rightChild: Directionality(
        textDirection: TextDirection.rtl,

        child: ChangeNotifierProvider.value(
            value: userService, child: DrawerDesign(changeDrawerCallBack)),
      ),
      onTapClose: true,
      borderRadius: 35,
      scaffold: scaffold,
    );
  }
}




