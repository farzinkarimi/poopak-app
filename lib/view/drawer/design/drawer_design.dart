import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:poopak_app/model/drawer_item_data.dart';
import '../../auth/auth_vmodel.dart';
import 'package:poopak_app/view/auth/CircleAvatarDrawer.dart';
import 'package:poopak_app/view/drawer/design/drawer_item.dart';
import 'package:poopak_app/view/drawer/drawer.dart';
import '../../../main.dart';
import 'package:poopak_app/view_model/user_vmodel.dart';
import 'package:provider/provider.dart';

class DrawerDesign extends StatelessWidget {
  DrawerDesign(this.changeDrawerCallBack);

  final changeDrawerCallBack;
  final List<DrawerItemData> drawerList = <DrawerItemData>[
    DrawerItemData(

      index: DrawerIndex.HOME,
      labelName: 'صفحه ی اصلی',
      icon: Icon(Icons.home),
    ),
    DrawerItemData(
      index: DrawerIndex.Pay,
      labelName: 'صفحه ی پرداخت',
      icon: Icon(EvaIcons.creditCard),
    ),
    DrawerItemData(
      index: DrawerIndex.Share,
      labelName: 'دعوت از دوستان',
      icon: Icon(EvaIcons.peopleOutline),
    ),
    DrawerItemData(
      index: DrawerIndex.ContactUs,
      labelName: 'ارتباط با ما',
      icon: Icon(Icons.headset_mic),
    ),
    DrawerItemData(
      index: DrawerIndex.About,
      labelName: 'درباره ی ما',
      icon: Icon(EvaIcons.info),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
            padding: EdgeInsets.only(top: 10),
            height: MediaQuery.of(context).size.height - 50,
            child: ListView(
              children: [
                Consumer<UserVModel>(
                  builder: (_, value, __) => AnimatedSwitcher(
                      duration: Duration(
                        milliseconds: 500,
                      ),
                      transitionBuilder: (child, animation) => FadeTransition(
                        opacity: animation,
                        child: child,
                      ),

                      child: value.user!=null
                          ? AuthedDrawer()
                          : AuthDrawer()),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                    height: 400,
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: drawerList.length,
                      itemBuilder: (context, index) {
                        return DrawerItem(
                            drawerList[index],changeDrawerCallBack);
                      },
                    )),
                ListTile(
                  onTap: (){
                    Provider.of<UserVModel>(context,listen: false).logout();
                  },
                  leading: Icon(
                    EvaIcons.logOutOutline,
                    color: Colors.red[700],
                  ),
                  title: Text('خروج از حساب کاربری',style: Theme.of(context).textTheme.headline1,),
                ),
                ListTile(
                  leading: Icon(
                    EvaIcons.powerOutline,
                    color: Colors.red[700],
                  ),
                  title: Text('خروج از برنامه',style: Theme.of(context).textTheme.headline1,),
                ),
              ],
            )),
      ),
    );
  }
}