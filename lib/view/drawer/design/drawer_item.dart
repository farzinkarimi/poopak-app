import 'package:flutter/material.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';
import 'package:poopak_app/model/drawer_item_data.dart';
import 'package:poopak_app/view/drawer/drawer_vmodel.dart';
import 'package:poopak_app/view/home/home_scroll_controller_and_drawer_key.dart';
import '../../../main.dart';
import 'package:provider/provider.dart';


class DrawerItem extends StatelessWidget {
  DrawerItem(this.list,this.callback);
  final DrawerItemData list;
  final Function callback;
  @override
  Widget build(BuildContext context) {

    var drawerOffset = Provider.of<DrawerVModel>(context, listen: false);

    return Container(
        height: 65,
        width: 180,
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            splashColor: Colors.grey.withOpacity(0.1),
            highlightColor: Colors.transparent,
            onTap: () {
              callback(list.index);
              HomeScrollAndDrawerKey.innerDrawerKey.currentState!
                  .toggle(direction: InnerDrawerDirection.end);
            },
            child: Stack(
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: _DrawerIconWidget(list)),
                Consumer<DrawerVModel>(
                    builder: (_, notifier, child) {
                      return drawerOffset.page == list.index
                          ? Transform(
                        transform: Matrix4.translationValues(
                            (MediaQuery.of(context).size.width * 0.55) -
                                (notifier.offset *
                                    MediaQuery.of(context).size.width /
                                    2),
                            0.0,
                            0.0),
                        child: child,
                      )
                          : const SizedBox();
                    },
                    child: _BlueBoxAroundSelected())
              ],
            ),
          ),
        ),
      );
  }
}
class _BlueBoxAroundSelected extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8, bottom: 8),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.5,
        height: 46,
        decoration: BoxDecoration(
          color: Colors.blue.withOpacity(0.2),
          borderRadius: new BorderRadius.only(
            topLeft: Radius.circular(28),
            topRight: Radius.circular(0),
            bottomLeft: Radius.circular(28),
            bottomRight: Radius.circular(0),
          ),
        ),
      ),
    );
  }
}

class _DrawerIconWidget extends StatelessWidget {
  _DrawerIconWidget(this.itemData);
  final DrawerItemData itemData;
  @override
  Widget build(BuildContext context) {
    var drawerOffset = Provider.of<DrawerVModel>(context, listen: false);

    return Row(
      children: <Widget>[
        Container(
          width: 6.0,
          height: 46.0,
        ),
        const Padding(
          padding: EdgeInsets.all(4.0),
        ),
        const SizedBox(height: 8),
        Consumer<DrawerVModel>(
          builder: (_, __, ___) => itemData.isAssetsImage
              ? Container(
            width: 24,
            height: 24,
            child: Image.asset(itemData.imageName,
                color: drawerOffset.page == itemData.index
                    ? Colors.blue
                    : Theme.of(context).colorScheme.secondary),
          )
              : Icon(itemData.icon.icon,
              color: drawerOffset.page == itemData.index
                  ? Colors.blue
                  : Theme.of(context).colorScheme.secondary),
        ),
        const SizedBox(
          width: 2,
        ),
        Consumer<DrawerVModel>(
          builder: (_, __, ___) => Text(
            itemData.labelName,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 12,
              color: drawerOffset.page == itemData.index
                  ? Colors.blue
                  : Theme.of(context).colorScheme.secondary,
            ),
            textAlign: TextAlign.left,
          ),
        ),
      ],
    );
  }
}

