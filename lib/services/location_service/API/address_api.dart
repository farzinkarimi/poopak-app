


import 'package:poopak_app/model/address.dart';

abstract class AddressAPI{
  Future<List<UserAddress>?> getUserAddresses();
  Future<bool> verifyLocation(double lat,double lon,int currentCity);
  Future<Map<String,dynamic>> positionToAddress(double lat, double long);
}