
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:poopak_app/model/address.dart';
import 'package:poopak_app/services/location_service/API/address_api.dart';
import 'package:latlong2/latlong.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'dart:convert' as convert;


class GetAddressWorkAPIs extends AddressAPI{

  @override
  Future<List<UserAddress>?> getUserAddresses() async {
    http.Response response;
    try {
      response = await http
          .get(
          Uri.parse(
              "https://api.json-generator.com/templates/rasrOAKsrU4E/data"),
          headers: {
            "Authorization": "Bearer 71f3ii0o09eshcs49425vr8jpiz4wdlupqj9yls8"
          }
      )
          .timeout(const Duration(seconds: 5));
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
    if (response.statusCode == 200 || response.statusCode == 201) {

      final List<dynamic> tempAddresses = jsonDecode(response.body) as List<dynamic>;
    List<UserAddress> buffer=[];
      for (Map<String, dynamic> singleAddresses in tempAddresses) {
        buffer.add(
          UserAddress(
            provinceCity: singleAddresses["provinceCity"],
            addressEntered: singleAddresses["address"],
            name: singleAddresses["name"],
            latLng: LatLng(singleAddresses["lat"], singleAddresses["lng"]),
          ),
        );
      }
      return buffer;
    } else {
      return null;
    }
  }

  @override
  Future<bool> verifyLocation(double lat,double lon,int currentCity) async {
    http.Response response;
    String api =
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk2ZDQyNTk4ZjI1YjllYzcwZjNhZjU0MmUxMWQ1MDVjZDNjYjMxY2ZiNDUzOTM2MDIyMzZmMTM2NzhiMTY1NmNkOWE1ZmMyN2ExOTgyZDYxIn0.eyJhdWQiOiIxMTcyNiIsImp0aSI6Ijk2ZDQyNTk4ZjI1YjllYzcwZjNhZjU0MmUxMWQ1MDVjZDNjYjMxY2ZiNDUzOTM2MDIyMzZmMTM2NzhiMTY1NmNkOWE1ZmMyN2ExOTgyZDYxIiwiaWF0IjoxNjEzODI0ODYyLCJuYmYiOjE2MTM4MjQ4NjIsImV4cCI6MTYxNDk0ODA2Miwic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.ZoWkYkwAt4O_x3uRFAiR1Zzf2NH9B-i7pacHjrJRTd1w7v55ZWjTQ9uc7V5KAj1eajEopS3mNn4qm8O9uolIfyo-B32h3WJ0qeW6-MlqOIQBj18rtzPYTxlL8DaEwBmZx26Vc8dxNtfJwX9k2UwlehCV2BiHrbW1LqO23STTouS5kiTZMFSTAHGfu7C2U5uRrOE1xp2qouUs8D5KivLI9Aa62eD8rMJ4upIaPN65VnD1WeAZh1woXwsuUQp797tIndLNMMGvpMY3G5otsSMNX7bW3BPTVr9RSiT-G9W_NZ3Tx8sJFthBMhmrL69MxWwkbdA5axtoIKeNQinu4oUh6w";

    try {
      response = await http.get(
          Uri.parse('https://map.ir/geofence/boundaries?lat=$lat&lon=$lon'),
          headers: {
            "x-api-Key": api,
          }).timeout(const Duration(seconds: 5));
    } catch (e) {
      debugPrint(e.toString());
      return false;
    }
    if (response.statusCode ==200||response.statusCode==201) {
      Map<String,dynamic> value=convert.jsonDecode(response.body);
      if(value["value"][0]["id"]==currentCity){
        return true;
      }


    }

    return false;
  }
  @override
  Future<Map<String,dynamic>> positionToAddress(double lat, double long) async {
     http.Response response;
    String api =
        'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijk2ZDQyNTk4ZjI1YjllYzcwZjNhZjU0MmUxMWQ1MDVjZDNjYjMxY2ZiNDUzOTM2MDIyMzZmMTM2NzhiMTY1NmNkOWE1ZmMyN2ExOTgyZDYxIn0.eyJhdWQiOiIxMTcyNiIsImp0aSI6Ijk2ZDQyNTk4ZjI1YjllYzcwZjNhZjU0MmUxMWQ1MDVjZDNjYjMxY2ZiNDUzOTM2MDIyMzZmMTM2NzhiMTY1NmNkOWE1ZmMyN2ExOTgyZDYxIiwiaWF0IjoxNjEzODI0ODYyLCJuYmYiOjE2MTM4MjQ4NjIsImV4cCI6MTYxNDk0ODA2Miwic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.ZoWkYkwAt4O_x3uRFAiR1Zzf2NH9B-i7pacHjrJRTd1w7v55ZWjTQ9uc7V5KAj1eajEopS3mNn4qm8O9uolIfyo-B32h3WJ0qeW6-MlqOIQBj18rtzPYTxlL8DaEwBmZx26Vc8dxNtfJwX9k2UwlehCV2BiHrbW1LqO23STTouS5kiTZMFSTAHGfu7C2U5uRrOE1xp2qouUs8D5KivLI9Aa62eD8rMJ4upIaPN65VnD1WeAZh1woXwsuUQp797tIndLNMMGvpMY3G5otsSMNX7bW3BPTVr9RSiT-G9W_NZ3Tx8sJFthBMhmrL69MxWwkbdA5axtoIKeNQinu4oUh6w';
    try {
      response = await http.get(
        Uri.parse("https://map.ir/fast-reverse?lat=$lat&lon=$long"),
        headers: {
          "x-api-Key": api,
        },
      ).timeout(const Duration(seconds: 5));
    } catch (e) {
      debugPrint(e.toString());
      return {};
    }
    var x = json.decode(response.body);
    return x;
  }
}

