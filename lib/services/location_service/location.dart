import 'package:intl/intl.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';

/// Determine the current position of the device.
///
/// When the location services are not enabled or permissions
/// are denied the `Future` will return an error.

Future<LocationData?> getLoc() async {
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  final location = Location();
  _serviceEnabled = await location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await location.requestService();
    if (!_serviceEnabled) {
      return null;
    }
  }

  _permissionGranted = await location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return null;
    }
  }

  var _currentPosition = await location.getLocation();
  var _initialcameraposition =
  LatLng(_currentPosition.latitude!, _currentPosition.longitude!);
  location.onLocationChanged.listen((LocationData currentLocation) {

    _currentPosition = currentLocation;
    _initialcameraposition =
        LatLng(_currentPosition.latitude!, _currentPosition.longitude!);

    DateTime now = DateTime.now();
    var _dateTime = DateFormat('EEE d MMM kk:mm:ss ').format(now);
  });
  return _currentPosition;
}
