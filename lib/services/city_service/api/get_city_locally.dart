import 'package:poopak_app/services/city_service/api/city_local.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GetCityLocally extends CityLocal {
  @override
  Future<bool> saveCity(String city) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString("city", city);
  }

  @override
  Future<String?> getCity() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final city = prefs.getString("city");

    return city;
  }

  @override
  Future<bool> deleteCity() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.remove("city");
  }
}
