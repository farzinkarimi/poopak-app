import 'dart:convert' as convert;

import 'package:http/http.dart' as http;
import 'package:poopak_app/model/city.dart';
import 'package:poopak_app/services/city_service/api/city_api.dart';
class GetCityAPIs extends CityAPI {



  @override
  Future<List<City>?> getCities() async {
    http.Response response;

    try {
      response = await http.post(
          Uri.parse(
            "https://api.json-generator.com/templates/diLBNqJp2dNn/data",
          ),
          headers: {
            "Accept": "application/json",
            "Authorization": "Bearer 71f3ii0o09eshcs49425vr8jpiz4wdlupqj9yls8"
          }).timeout(Duration(seconds: 10));
    } catch (e) {
      print("$e from getCities");
      return null;
    }
    final List<dynamic> data;

    if (response.statusCode == 200 || response.statusCode == 201) {
      data = convert.jsonDecode(response.body) as List<dynamic>;
      return data.map<City>((json) {
        return City.fromJson(json as Map<String, dynamic>);
      }).toList();
    } else {
      return null;
    }
  }
}
