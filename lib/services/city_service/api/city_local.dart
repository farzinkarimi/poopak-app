
abstract class CityLocal{
  Future <bool>saveCity(String city);
  Future<String?> getCity();
  Future <bool>deleteCity();
}