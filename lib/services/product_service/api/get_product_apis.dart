import 'package:http/http.dart' as http;
import 'package:poopak_app/model/city.dart';
import 'package:poopak_app/services/product_service/api/product_api.dart';
import 'dart:convert' as convert;

class GetProductsAPIs extends ProductAPI {

  @override
  Future<List<dynamic>?> getProducts(City city) async {
    http.Response response;
    try {
      response = await http.get(
          Uri.parse(
              "https://api.json-generator.com/templates/blx_FV8Ubl__/data"),
          headers: {
            "Authorization": "Bearer 71f3ii0o09eshcs49425vr8jpiz4wdlupqj9yls8"
          }).timeout(Duration(seconds: 10));
    } catch (e) {
      print(e);
      return null;
    }
    if (response.statusCode == 200 || response.statusCode == 201) {
      return
      convert.jsonDecode(response.body);
    }
    else{return null;}
  }


}