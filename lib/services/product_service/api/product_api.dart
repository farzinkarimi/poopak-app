import 'package:poopak_app/model/city.dart';

abstract class ProductAPI {

  Future<List<dynamic>?> getProducts(City city);

}