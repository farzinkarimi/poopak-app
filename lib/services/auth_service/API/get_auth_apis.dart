import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:poopak_app/services/auth_service/API/auth_api.dart';

class GetAddressAPIs extends AuthAPI {
  @override
  Future<bool> sendPin(String mobile, int randomNumber) async {
    String message = 'کد 4 رقمی ارسال شده برای شما : ' +
        '$randomNumber' +
        '  شرکت خدماتی مروارید پوپک';
    http.Response response;
    try {
      response = await http
          .get(
            Uri.parse(
                'https://raygansms.com/SendMessageWithCode.ashx?Username=farzinkarimi&Password=1539574862&Mobile=$mobile&Message=$message/'),
          )
          .timeout(Duration(seconds: 5));
    } catch (e) {
      print("$e from sendPin");
      return false;
    }
    final int returnedMassageSendPin;
    if (response.statusCode == 200 || response.statusCode == 201) {
      returnedMassageSendPin = jsonDecode(response.body);

      if (returnedMassageSendPin >= 2000) return true;
      else return false;
    } else {
      return false;
    }
  }

  @override
  Future<Map<String, dynamic>?> checkEnteredMobile(String mobile) async {
    http.Response response;
    try {
      response = await http.get(
          Uri.parse(
            "https://api.json-generator.com/templates/Cp9tr29p2gKl/data",
          ),
          headers: {
            "Authorization": "Bearer 71f3ii0o09eshcs49425vr8jpiz4wdlupqj9yls8"
          }).timeout(Duration(seconds: 10));
    } catch (e) {
      print("$e from checkEnteredMobile");
      return null;
    }
    final Map<String, dynamic> massageData ;
    if (response.statusCode == 200 || response.statusCode == 201) {
      massageData = jsonDecode(response.body);
      return massageData;
    } else {
      return null;
    }
  }

  @override
  Future<String?> requestSignUp(Map<String, dynamic> user) async {
    http.Response response;
    try {
      response = await http.get(
          Uri.parse(
              "https://api.json-generator.com/templates/hdjD8EyVtqR8/data"),
          headers: {
            "Authorization": "Bearer 71f3ii0o09eshcs49425vr8jpiz4wdlupqj9yls8"
          }).timeout(Duration(seconds: 10));
    } catch (e) {
      print("$e from requestSignUp");
      return null;
    }
    final String? idToken ;
    if (response.statusCode == 200 || response.statusCode == 201) {
      idToken = jsonDecode(response.body)[0];
      return idToken;
    } else {
      return null;
    }
  }
}
