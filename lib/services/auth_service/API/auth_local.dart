


abstract class AuthLocal{
  Future<void> saveInStorage({required String key, required String value});
  Future<void> removeAuthData();
  Future<List<String>?> autoAuthenticate();

}