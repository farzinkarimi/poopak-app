import 'package:poopak_app/services/auth_service/API/auth_local.dart';
import 'package:poopak_app/services/city_service/api/city_local.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GetAuthLocally extends AuthLocal {
  @override
  Future<List<String>?> autoAuthenticate() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? token = prefs.getString('idToken');
    final String? mobile = prefs.getString('mobile');

    if (token != null && mobile != null) {
      return [token, mobile];
    } else {
      return null;
    }
  }

  @override
  Future<void> removeAuthData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    prefs.remove('mobile');
  }

  @override
  Future<void> saveInStorage(
      {required String key, required String value}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }
}
