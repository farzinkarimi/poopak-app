

abstract class AuthAPI{
  Future<bool> sendPin(String mobile,int randomNumber);
  Future<Map<String,dynamic>?> checkEnteredMobile(String mobile);
  Future<String?> requestSignUp(Map<String,dynamic> tempUser);
}